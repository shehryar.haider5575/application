
<script>
$(document).ready(function() {
	$('.fancybox').fancybox();
});
</script>
<style>
.formLabelContainer { padding: 0 10px 0 0 !important; width:50px !important; }
</style>
<?php
$path 				= '/'.$this->currentController.'/'.$this->currentAction;
$currentMonth		= 6;
$currentYear		= date('Y');
$month 				= ($month) 		? $month 	: ($this->input->post('month') 	? $this->input->post('month') 	: (($this->input->post('selectedMonth')) 	? $this->input->post('selectedMonth') 	: 6));
$year 				= ($year) 		? $year		: ($this->input->post('year') 	? $this->input->post('year') 	: (($this->input->post('selectedYear')) 	? $this->input->post('selectedYear') 	: (int)date('Y')));
$empCode 			= ($empCode) 	? $empCode 	: ($this->input->post('empCode')? $this->input->post('empCode') : (($this->input->post('empID')) 			? $this->input->post('empID') 			: $this->employeeID));

$txt0				= json_decode($arrTasks[0]['assessment_kpi']);
$txt1				= json_decode($arrTasks[0]['assessment_accomplishments']);
$txt2				= json_decode($arrTasks[0]['assessment_challenges']);
$txt3				= json_decode($arrTasks[0]['assessment_strengths']);
$txt4				= json_decode($arrTasks[0]['assessment_improvement']);
$txt5				= json_decode($arrTasks[0]['assessment_skills']);

$supervisorComments = isset($_POST['supervisorComments']) 		? $_POST['supervisorComments'] 	: $arrTasks[0]['assessment_remarks'];

if(isset($_POST['status']))
{ $status = $_POST['status']; }
else if(($arrMonthTasksStatus['pe_status'] > 2) && ($arrMonthTasksStatus['pe_status'] < 4))
{ $status = $arrMonthTasksStatus['pe_status']; }
else if(($arrMonthTasksStatus['pe_status'] > 5) && ($arrMonthTasksStatus['pe_status'] < 7))
{ $status = $arrMonthTasksStatus['pe_status']; }
else { $status = ''; }
?>

<form name="frmSearchTasks" id="frmSearchTasks" method="post" action="<?php echo $frmActionURL; ?>">
<div class="searchBoxMain">
    <div class="searchHeader">Search Criteria</div>
    <div class="searchcontentmain">
        <div class="searchCol">
        	<?php
			 {
			?>
			<div class="labelContainer">Employee:</div>
			<div class="textBoxContainer">
			  <select name="empCode" id="empCode" class="dropDown">
				  <option value="">Select Employee</option>
                  <?php
					if (count($arrEmployees)) {
						foreach($arrEmployees as $key => $arrEmployee) {
					?>
						<optgroup label="<?php echo $key; ?>">
							<?php for($i = 0; $i < count($arrEmployee); $i++) { ?>					
								<option value="<?php echo $arrEmployee[$i]['emp_id']; ?>"><?php echo $arrEmployee[$i]['emp_full_name']; ?></option>
							<?php } ?>
						</optgroup>
					<?php	}
					}
					?>
			  </select>
			</div>
			<?php
			}
			?>
        </div>
        <div class="searchCol">
        	<div class="labelContainer">Month:</div>
            <div class="textBoxContainer">
                <select class="dropDown" id="month" name="month" style="width:85px; margin-left:5px">
                    <option value="">Month</option>
                    <option value="6">June</option>
					<option value="12">December</option>
                </select>
            </div>
            <div class="labelContainer">Year:</div>
            <div class="textBoxContainer">
                <select id="year" name="year" class="dropDown" style="width:85px; margin-left:5px">
                    <option value="">Year</option>
                    <?php for($ind = $this->HRMYearStarted; $ind <= date('Y'); $ind++) { ?>
                    <option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="formButtonContainerWide">
            <input type="submit" class="searchButton" name="btnSearchTasks" id="btnSearchTasks" value="Search">
			<?php if(count($arrTasks)) { ?>
            <input type="button" class="searchButton" name="btnExport" id="btnExport" value="Export to PDF" onClick="window.location.href = '<?php echo base_url() . $this->currentController . '/my_tasks'; ?>/<?php echo $arrTasks[0]['assessment_id']; ?>'">
            <?php } ?>
        </div>
    </div>
    <script>
	$('#empID').val('<?php echo $empID; ?>');
	$('#empCode').val('<?php echo $empCode; ?>');
  	$('#month').val('<?php echo $month; ?>');
  	$('#year').val('<?php echo $year; ?>');
  </script>
</div>
</form>
</div>

	
    
	<div class="centerElementsContainer">
		<div class="recordCountContainer"><?php echo "Total Records Count: ".$totalRecordsCount; ?></div>
		<?php
        if($pageLinks) {
        ?>
            <div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
        <?php 	}	?>
	</div>

	<div class="listContentMain">
    <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
        <tr class="listHeader">
            <td class="listHeaderCol">Employee Name</th>
            <td class="listHeaderCol">Assesment KPIS</th>
            <td class="listHeaderCol">Self Appraisal Date</th>
            
            <?php if(($canWrite == 1) || (($canDelete == 1) && ($arrRecords[$ind]['assessment_id'] != STATUS_DELETED))) { ?>
            <td class="listHeaderColLast" style="width:100px">Action</th>
            <?php } ?>
        </tr>
        <?php
        for($ind = 0; $ind < count($arrRecords); $ind++) 
		{
			($arrRecords[$ind]['company_status'] == STATUS_ACTIVE) ? $classListingData = "listContent" : $classListingData = "listContentAlternate" ;
        ?>
        <tr class="<?php echo $classListingData; ?>">
            <td class="listContentCol"><?php echo $arrRecords[$ind]['emp_full_name']; ?></td>
            <td class="listContentCol"><?php echo $arrRecords[$ind]['assessment_kpi']; ?></td>
            <td class="listContentCol"><?php echo $arrRecords[$ind]['created_date']; ?></td>
             
            
           
            <?php if(($canWrite == 1) || (($canDelete == 1) && ($arrRecords[$ind]['company_status'] != STATUS_DELETED))) { ?>
			<td class="listContentColLast">
            	<div class="colButtonContainer" style="width:150px">
                <?php if($canWrite == 1) { ?>
                    
                <?php } ?>
                <?php if(($canDelete == YES)){ ?>
                    <input type="button" class="smallButton" value="Delete" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/<?php echo $arrRecords[$ind]['assessment_id']; ?>', '<?php echo $arrRecords[$ind]['assessment_id']; ?>');" />
                <?php } ?>
                
               
                
                </div>
            </td>
			<?php }	?>
        </tr>
        <?php
       }
       ?>
    </table>
    </div>
</div>