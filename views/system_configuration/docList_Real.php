<?php
$path = '/'.$this->currentController.'/'.$this->currentAction;
$companies 		= (isset($_POST['companies'])) ? $_POST['companies']			:	'';
?>

<form name="frmListCompanies" id="frmListCompanies" method="post" action="<?php echo $frmActionURL; ?>">
<div class="listPageMain">
	<div class="searchBoxMain">
    	<div class="searchHeader">Search Criteria</div>
        
        <div class="searchcontentmain">
            <div class="searchCol">
				<div class="labelContainer">Status:</div>
				<div class="textBoxContainer">
				 <select id="companies" name="companies" class="dropDown">
              <option value="">Select Companies</option>
              <?php
              if (count($getCompanies)) {
                  foreach($getCompanies as $getCompany) {
                      $selected = '';
                     
              ?>
                  <option value="<?php echo $getCompany['company_id']; ?>" ><?php echo $getCompany['company_name']; ?></option>
              <?php
                  }
              }
              ?>
          </select>
				</div>
			</div>
			
			
			<div class="formButtonContainerWide">
				<input type="submit" class="searchButton" name="btnSearchCompanies" id="btnSearchCompanies" value="Search">
			</div>
		</div>
	</div>
      
  <script>
  	$('#companies').val('<?php echo $companies; ?>');
  </script>
</form>

	<?php if($canWrite == 1) { ?>
	<div class="centerButtonContainer">
		<input class="addButton" type="button" value="Add New Document Companies" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/docList_Companies' ?>';" />
	</div>
	<?php }	?>
    
	<div class="centerElementsContainer">
		<div class="recordCountContainer"><?php echo "Total Records Count: ".$totalRecordsCount; ?></div>
		<?php
        if($pageLinks) {
        ?>
            <div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
        <?php 	}	?>
	</div>

	<div class="listContentMain">
 
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain dottedBorder">
	<tr class="listHeader">
    	<td class="listHeaderCol" width="50%">Company Name</th>
    	<td class="listHeaderCol" width="20%">Document Type</th>
    	<td class="listHeaderCol" width="20%">Document</th>
        <?php if($canDelete == YES) { ?>
    	<td class="listHeaderColLast">Action</th>
        <?php } ?>
    </tr>
    <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
	?>
    <tr class="listContent">
    	<td class="listContentCol"><?php
		
		
		$getCompanies = $this->model_system->getCompanies(array('company_id' => $arrRecords[$ind]['company_id']));
		
		 echo $getCompanies[0]['company_name']; ?></td>
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['doc_type']; ?></td>
    	<td class="listContentCol"><?php echo ($arrRecords[$ind]['doc_file'] != '') ? '<a href="' . $this->baseURL . '/' . $companyDocFolderShow . $arrRecords[$ind]['doc_file'] . '" style="color:#f00" target="_blank">View Document</a>' : ' - '; ?></td>
        <?php if($canDelete == YES) { ?>
    	<td class="listContentColLast">
        	<div class="empColButtonContainer">
            <input type="button" class="smallButton" value="Delete" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/<?php echo $arrRecords[$ind]['company_id']; ?>', '<?php echo $arrRecords[$ind]['doc_id']; ?>');" />
            <?php  { ?>
                    <input class="smallButton" type="button" value="View/Edit" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/docList_Companies/' . $arrRecords[$ind]['doc_id']; ?>';" />
                <?php } ?>
                   
              
			</div>
        </td>
        <?php } ?>
    </tr>
    <?php
	}
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="4" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
   
</table>
    </div>
</div>