<style>
    .selectize-control.dropDown.single {
        width: 250px !important;
    }
</style>
<?php $userRoleID = $this->session->userdata('user_role_id');

$headerTitle = "PCE Employee Dashboard - HRMS";
$url = $_SERVER['REQUEST_URI'];
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<!-- <div class="headerTitle">
    <a href="<?php echo base_url();?>home">
        <img src="<?php echo $this->imagePath; ?>/logo.png" alt=" HRM" title="ValuStrat HRM">
    </a>
</div>

<?php if($this->currentController != 'message') {
		if($this->userID != 0) { ?>
	<?php if($this->userRoleID == WEB_ADMIN_ROLE_ID || (int)$this->session->userdata('user_is_webadmin')) { ?>
<div class="logoutMain" style="padding-top:22px">
	<div style="float:right">    	
    	<form name="frmChangeUser" id="frmChangeUser" method="post">
            <div class="logoutText">Use As:&nbsp;
            <select name="changeUser" id="changeUser" onchange="this.form.submit()" class="dropDown" style="float:right">
                <option value="">Select Employee</option>
                <?php for($ind = 0; $ind < count($this->allEmployees); $ind++) { ?>
                <option value="<?php echo $this->allEmployees[$ind]['emp_id']; ?>"><?php echo $this->allEmployees[$ind]['emp_full_name']; ?> - <?php echo $this->allEmployees[$ind]['user_role_name']; ?></option>
                <?php } ?>
            </select></div>
            <script>$('#changeUser').val('<?php echo (int)$this->userEmpNum; ?>');</script>
        </form>
    </div>
    <?php } else { ?>
    <div class="logoutMain">
    <?php } ?>
	<div class="logoutText">
		Welcome - <?php echo $this->userWelcomeName; ?>
	</div> -->
<!-- 	
	<?php echo form_open(); ?>
	<div class="logoutButton">
		<input class="smallButton" name="logoffBtn" type="submit" value="Logout">
		<input type="hidden" name="logoff" value="1">
	</div>
	<?php echo form_close(); ?>
</div> -->
<div>
    <div id="page-container" class="sidebar-o sidebar-dark enable-page-overlay side-scroll page-header-fixed main-content-narrow">
		<nav class="cl" style="background-image: linear-gradient(45deg, #780206, #061161)" id="sidebar" aria-label="Main Navigation">
            <!-- Side Header (mini Sidebar mode) -->
            <div class="smini-visible-block">
                <div class="content-header">
                    <!-- Logo -->
                    <a class="font-w600 text-white tracking-wide" href="index.html">
                        D<span class="opacity-75">x</span>
                    </a>
                    <!-- END Logo -->
                </div>
            </div>
            <!-- END Side Header (mini Sidebar mode) -->
            <!-- Side Header (normal Sidebar mode) -->
            <div class="smini-hidden">
                <div class="content-header justify-content-lg-center">
                    <!-- Logo -->
                    <a class="font-w600 text-white tracking-wide" href="index.html">    
                    <!-- Dash<span class="opacity-75">mix</span>
                    <span class="font-w400">Modern</span> -->
                    <img src="https://amanahmall.com.pk/wp-content/uploads/2018/12/Pakistan-Currency-Exchange.png" alt="Pakistan Currency Exchange Logo" width="150" heigh="auto">
                    </a>
                    
                    <!-- END Logo -->
                    <!-- Options -->
                    <div class="d-lg-none">
                        <!-- Close Sidebar, Visible only on mobile screens -->
                        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                        <a class="text-white ml-2" data-toggle="layout" data-action="sidebar_close" href="javascript:void(0)">
                            <i class="fa fa-times-circle"></i>
                        </a>
                        <!-- END Close Sidebar -->
                    </div>
                    <!-- END Options -->
                </div>
            </div>
            <!-- END Side Header (normal Sidebar mode) -->

            <!-- Sidebar Scrolling -->
            <div class="js-sidebar-scroll">
                <!-- Side Actions -->
                <div class="content-side content-side-full bg-black-10 text-center">
                    <div class="smini-hide">
                        <button type="button" class="btn btn-sm btn-secondary">
                            <i class="fa fa-fw fa-user-circle"></i>
                        </button>
                        <button type="button" class="btn btn-sm btn-secondary">
                            <i class="fa fa-fw fa-pencil-alt"></i>
                        </button>
                        <button type="button" class="btn btn-sm btn-secondary">
                            <i class="fa fa-fw fa-file-alt"></i>
                        </button>
                        <button type="button" class="btn btn-sm btn-secondary">
                            <i class="fa fa-fw fa-envelope"></i>
                        </button>
                        <button type="button" class="btn btn-sm btn-secondary">
                            <i class="fa fa-fw fa-cog"></i>
                        </button>
                    </div>
                </div>
                <!-- END Side Actions -->
                <!-- UserRole Management Start -->
                <div style="display: flex">
            
    
                <form name="frmChangeUser" id="frmChangeUser" method="post">
                    <div class="useAs">Use As:&nbsp;
                        <select name="changeUser" id="changeUser" onchange="this.form.submit()" class="dropDown" style="float:right">
                            <option value="">Select Employee</option>
                            <?php for($ind = 0; $ind < count($this->allEmployees); $ind++) { ?>
                            <option value="<?php echo $this->allEmployees[$ind]['emp_id']; ?>"><?php echo $this->allEmployees[$ind]['emp_code'] . '---' . $this->allEmployees[$ind]['emp_full_name']; ?> - <?php echo $this->allEmployees[$ind]['user_role_name']; ?></option>
                            <!-- USER CHECKING FOR DEV PROCESS -->
                            <!-- <//?php echo $arrEmpSupervisors; ?> -->
                            <?php } ?>
                            <!-- <//?php echo $userRoleID;?> -->  
                            <?php echo print_r($arrEmpSupervisors); ?>
                        </select>
                    </div>
                    <script>$('#changeUser').val('<?php echo (int)$this->userEmpNum; ?>');</script>
                </form>
                <br><br>
                <?php echo form_open(); ?>
                    <div class="logoutButton" style="position: relative;right: 104px;top: 30px;">
                        <input class="logout" name="logoffBtn" type="submit" value="Logout">
                        <input type="hidden" name="logoff" value="1">
                    </div>
                <?php echo form_close(); ?>
            </div>
            <div class="content-side">
                <ul class="nav-main designation_nav">
                    <li class="nav-main-item">
                        <a class="nav-main-link active" href="/HRM/home">
                            <span class="nav-main-link-name">Log As </span><span class="nav-main-link-name"><?php echo $this->session->userdata('display_name');  
                            ?> </span>
                            <!-- <span class="nav-main-link-badge badge badge-pill badge-primary">3</span> -->
                        </a>
                    </li>
                </ul>
            </div>
            <?php if (!empty($this->BranchName)) { ?>
                <div class="content-side">
                    <ul class="nav-main designation_nav">
                        <li class="nav-main-item">
                            <a class="nav-main-link active" href="/HRM/home">
                                <span class="nav-main-link-name">Branch Manager : </span><span class="nav-main-link-name"><?php echo $this->BranchName;  
                                ?> </span>
                                <!-- <span class="nav-main-link-badge badge badge-pill badge-primary">3</span> -->
                            </a>
                        </li>
                    </ul>
                </div>
            <?php } ?>
            <!-- UserRole Management Ends -->
                    <!-- Side Navigation -->
                    <div class="content-side">
                        <hr>
                        <ul class="nav-main">
                            <li class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="<?php echo $this->baseURL; ?>" style="height:25px; padding-top:16px"
                                ><i class="nav-main-link-icon fa fa-home"></i>
                                <span class="nav-main-link-name">Home</span></a>
                            </li>
                            <?php
                            foreach ($this->modulesAllowedForMenu as $module)
                            {
                                if($module['module_name'] != "home")
                                {
                                    $moduleName = explode(' ', $module['display_name']);
                                    ($module['module_name'] == $this->currentController) ?	$mainMenuClass = "current" : $mainMenuClass = "";
                                    $allowedSubModulesList = $module['sub_menu']; //$this->privilege->getPrivilege($this->userRoleID, $module['module_id'], true);
                            ?>
                                <li class="<?php echo $mainMenuClass; ?> nav-main-item" >
                                    <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="<?php echo $this->baseURL . '/' . $module['module_name']; ?>">
                                    <i class="nav-main-link-icon fa <?php echo $module['icon']; ?>" aria-hidden="true"></i>
                                    <span class="nav-main-link-name"><?php echo ucwords($moduleName[0]); ?> <?php echo ucwords($moduleName[1]); ?></span> 
                                    </a>
                                        <?php if(count($allowedSubModulesList)) { ?>
                                            <ul class="nav-main-submenu">
                                            <?php
                                            for($ind = 0; $ind < count($allowedSubModulesList); $ind++) {
                                                ($allowedSubModulesList[$ind]['module_name'] == $this->currentAction) ?	$subMenuClass = "selected" : $subMenuClass = "";
                                            ?>
                                                <li class="<?php echo $subMenuClass; ?> nav-main-item">
                                                    <a class="nav-main-link" href="<?php echo $this->baseURL . '/' . $module['module_name']. '/' . $allowedSubModulesList[$ind]['module_name']; ?>" >
                                                    <i class="nav-main-link-icon fa fa-circle-o"></i>
                                                    <span class="nav-main-link-name"><?php echo $allowedSubModulesList[$ind]['display_name']; ?></span>
                                                    </a>
                                                </li>
                                            <?php
                                            }
                                            // print_r($allowedSubModulesList[8]);exit;
                                            ?>
                                            </ul>
                                    <?php } ?>
                                </li>
                            <?php 
                                }
                            }
                        ?>
                        </ul> 
                    
                    
                    </div>
                    <!-- END Side Navigation -->
                </div>
                <!-- END Sidebar Scrolling -->
            </nav>

	</div>
</div>
<?php }
} 
?>