<div class="popupNotificationMain" style="font-size:14px">
  <div class="popupNotificationBox">
    <div style="float:left;width:100%">
      <div style="border:5px solid #99DAEF;float:none;margin:0 auto;">
      	<table width="100%">
        	<tr>
            	<td>
        			<table border="0" align="center" cellspacing="0" cellpadding="0" class="dottedBorder" style="width:100%">
                      <tr>
                        <td class="weekendPopupColLast" colspan="2">
                        	<div class="weekendPopupHeading">Leaves Calculation</div>
                        </td>
                      </tr>
                      <tr>
                        <td class="weekendPopupCol">Upto 5 Lates</td>
                        <td class="weekendPopupColLast">0 Leave</td>
                      </tr>
                      <tr class="weekendPopupRowAlternate">
                        <td class="weekendPopupCol">6 Lates</td>
                        <td class="weekendPopupColLast">0.5 Leave</td>
                      </tr>
                      <tr>
                        <td class="weekendPopupCol">7 Lates</td>
                        <td class="weekendPopupColLast">1 Leave</td>
                      </tr>
                      <tr class="weekendPopupRowAlternate">
                        <td class="weekendPopupCol">X Lates (> 7)</td>
                        <td class="weekendPopupColLast">( X <span style="font-size:22px">-</span> 6 ) Leave(s)</td>
                      </tr>
                      <tr>
                        <td class="weekendPopupColLast">&nbsp;</td>
                        <td class="weekendPopupColLast">&nbsp;</td>
                      </tr>
                      <tr class="weekendPopupRowAlternate">
                        <td class="weekendPopupCol">Half Day</td>
                        <td class="weekendPopupColLast">0.5 Leave</td>
                      </tr>
                      <tr>
                        <td class="weekendPopupCol">Full Day</td>
                        <td class="weekendPopupColLast">1 Leave</td>
                      </tr>
                      <tr>
                        <td class="weekendPopupColLast">&nbsp;</td>
                        <td class="weekendPopupColLast">&nbsp;</td>
                      </tr>
                    </table>
        		</td>
                <td width="30px">
                	&nbsp;
                </td>
                <td>
                	<table border="0" align="center" cellspacing="0" cellpadding="0" class="dottedBorder" style="width:100%">
                      <tr>
                        <td class="weekendPopupColLast" colspan="3">
                        	<div class="weekendPopupHeading">My Details</div>
                        </td>
                      </tr>
                      <tr>
                        <td class="weekendPopupCol" width="120px">Type</td>
                        <td class="weekendPopupCol">Total</td>
                        <td class="weekendPopupColLast">Leave(s)</td>
                      </tr>
                      <tr class="weekendPopupRowAlternate">
                        <td class="weekendPopupCol">Late(s)</td>
                        <td class="weekendPopupCol"><?php echo $totalLates; ?></td>
                        <?php
						$latePU = 0;
                        if($totalLates > $this->maxLateAllowed && $totalLates < $this->fullOnLates) {
							$latePU	= ($totalLates - $this->maxLateAllowed) * 0.5;
						} else if($totalLates >= $this->fullOnLates) {
							$latePU	= (1 + ($totalLates - $this->halfOnLates));
						}
						?>
                        <td class="weekendPopupColLast"><?php echo $latePU; ?></td>
                      </tr>
                      <tr>
                        <td class="weekendPopupCol">Halfday(s)</td>
                        <td class="weekendPopupCol"><?php echo $totalHDs; ?></td>
                        <td class="weekendPopupColLast"><?php echo $totalHDs * 0.5; ?></td>
                      </tr>
                      <tr class="weekendPopupRowAlternate">
                        <td class="weekendPopupCol">Absent(s)</td>
                        <td class="weekendPopupCol"><?php echo $totalAbsents; ?></td>
                        <td class="weekendPopupColLast"><?php echo $totalAbsents; ?></td>
                      </tr>
                      <tr>
                        <td class="weekendPopupCol">&nbsp;</td>
                        <td class="weekendPopupCol">&nbsp;</td>
                        <td class="weekendPopupColLast">&nbsp;</td>
                      </tr>
                      <tr class="weekendPopupRowAlternate">
                        <td class="weekendPopupCol">&nbsp;</td>
                        <td class="weekendPopupCol">&nbsp;</td>
                        <td class="weekendPopupColLast">&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="weekendPopupCol">Total Leaves</td>
                        <td class="weekendPopupCol">&nbsp;</td>
                        <td class="weekendPopupColLast"><?php echo $totalPUL; ?></td>
                      </tr>
                      <tr>
                        <td class="weekendPopupCol">&nbsp;</td>
                        <td class="weekendPopupCol">&nbsp;</td>
                        <td class="weekendPopupColLast">&nbsp;</td>
                      </tr>
                    </table>
                </td>
        	</tr>
        </table>
      </div>
    </div>
  </div>
  <div style="text-align:center">
    <input  class="smallButton"  name="close" type="button" value="Close" onclick="window.close()">
  </div>
</div>
