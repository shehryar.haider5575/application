<style>
.applyBoxMain {
	float:left;
	width:1246px
}
.applyBoxHeader {
	background: url('http://www.sbtjapan.com/images/main_box_header_bg.jpg') repeat-x scroll 0 0 rgba(0, 0, 0, 0);
	color: #2D2D2D;
	float: left;
	font-size: 17px;
	height: 22px;
	padding: 4px 0 0 6px;
	text-align: left;
	width:1240px
}
.applyBoxBorder {
	float:left;
	width:1px
}
.applyBoxMiddle {
	color:#000000;
	float:left;
	font-family:Arial;
	font-size:13px;
	line-height:13px;
	padding:5px 25px;
	text-align:left;
	width:1194px
}
.applyBoxFooter {
	background:url('http://www.sbtjapan.com/images/main_box_footer_bg.jpg') repeat-x scroll 0 0 rgba(0, 0, 0, 0);
	float:left;
	height:15px;
	width:1246px
}
.applyTextBox {
	border:1px solid #A8A8A8;
	height:22px;
	margin:0 0 5px;
	padding:2px 5px;
	width:171px
}
.applyBoxMiddle h1 {
	color:#2777B3;
	font-size:18px;
	font-weight:normal;
	line-height:17px;
	margin:20px 0 10px
}
.applyBoxMiddle p {
	background:url('../../images/red-bullet.jpg') no-repeat scroll 0 4px;
	color:#FF0000;
	font-size:12px;
	line-height:17px;
	margin:0 0 10px;
	padding:0 0 0 15px
}
.applyformLabel {
	font-size:13px;
	line-height:15px;
	vertical-align:top;
	padding:6px 0 0
}
</style>

<form name="frmGetDetails" id="frmGetDetails" method="post">
<!--
<div id="recordDiv" class="listContentCareer">
<table class="listTableMain" cellspacing="0" border="0" style="border:solid 1px #e1e1e1; border-collapse:collapse;">
<tr class="listHeader">
	<td colspan="2" class="listHeaderCol" width="100%">Send Details</td>
</tr>
<tr class="listContent">
	<td class="listContentCol" width="50%">Account Email</td>
	<td class="listContentCol" width="50%"><?php echo $strEmail; ?><input type="hidden" name="candEmail" id="candEmail" value="<?php echo $strEmail; ?>" /></td>
</tr>
<tr class="listContent">
	<td class="listContentCol" width="50%"></td>
	<td class="listContentCol" width="50%"><input type="submit" class="smallButton" name="btnSend" id="btnSend" value="Send Me Details"></td>
</tr>
</div>
-->
<div class="listPageMain">
	<div class="formMain">
		<table border="0" cellspacing="0" cellpadding="0" style="width:400px">
			<tr>
				<td class="formHeaderRow" colspan="2">Send Details</td>
			</tr>
			<tr>
				<td class="formLabelContainer">Account Email</td>
				<td class="formTextBoxContainer">
					<div><?php echo $strEmail; ?></div>
					<input type="hidden" name="candEmail" id="candEmail" value="<?php echo $strEmail; ?>" />
				</td>
			</tr>
            <tr>
                <td class="formLabelContainer"></td>
                <td class="formTextBoxContainer">
					<input type="submit" class="smallButton" name="btnSend" id="btnSend" value="Send Me Details">
				</td>
			</tr>
		</table>
	</div>
</div>

<!--
  <div class="applyBoxMain">
    <div class="applyBoxHeader">Send Details</div>
    <div class="applyBoxBorder"> <img width="1px" height="200px" src="http://www.sbtjapan.com/images/main_box_side_border.jpg"> </div>
    <div class="applyBoxMiddle">
      <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
        <tr>
          <td><table border="0" cellspacing="0" cellpadding="0" style="width:550px">
              <tr>
                <td align="left" valign="top" style="width:100px" class="applyformLabel">Account Email</td>
                <td align="left" valign="top" style="width:175px">< ?php echo $strEmail; ?>
                	<input type="hidden" name="candEmail" id="candEmail" value="< ?php echo $strEmail; ?>" />
                </td>
              </tr>
              <tr>
                <td align="left" valign="top" style="width:100px" class="applyformLabel"></td>
                <td align="left" valign="top" style="width:175px"><input type="submit" class="smallButton" name="btnSend" id="btnSend" value="Send Me Details"></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <div class="applyBoxBorder"> <img width="1px" height="200px" src="http://www.sbtjapan.com/images/main_box_side_border.jpg"> </div>
      <div class="applyBoxFooter" style="margin-bottom:5px;"></div>
    </div>
  </div>
-->
</form>
