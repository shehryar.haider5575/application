<script>
$(document).ready(function () {
      $('.select-state').selectize({
          sortField: 'text'
      });
  });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />



<?php
$empID 		= isset($_POST['empID']) 		? $_POST['empID'] 		: $empID;
$selMonth 	= isset($_POST['selMonth']) 	? $_POST['selMonth'] 	: $selMonth;
$selYear 	= isset($_POST['selYear']) 		? $_POST['selYear'] 	: $selYear;
$employee_id = array();
$attendance_date = array();
$attendance_in = array();
$attendance_out = array();
if ($fetch_timing->num_rows() > 0) {
	foreach ($fetch_timing->result() as $row) {
		array_push($employee_id, $row->employee_id);
		array_push($att_date, $row->att_date);
		array_push($att_in, $row->att_in);
		array_push($att_out, $row->att_out);
	}
} else {
	echo "no data found";
}	
$data1 = [];
foreach ($fetch_timing->result() as $key => $row) {
	$data1[$key]['employee_id'] = $row->employee_id;
	$data1[$key]['attendance_date'] = $row->att_date;
	$data1[$key]['attendance_in'] = $row->att_in;
	$data1[$key]['attendance_out'] = $row->att_out;
}

$emp_key = array_search($empID, $employee_id);
$worktime_in;
$worktime_out;
?>
<style>
	.selectize-dropdown.single.dropDown{
		min-height: 100px !important;

	}
	.item{
		width: 100%;
		height: 0rem;
	}
</style>
<script>
	$(function() {
		$(".datePicker").datepicker({
			changeMonth: true,
			changeYear: true
		});
		$(".datePicker").datepicker("option", "dateFormat", "<?php echo $dateFormat; ?>");
		$(".datePicker").datepicker("option", "minDate", '<?php echo date('Y-m-d', strtotime('6 months ago')); ?>');
		$("#dateFrom").datepicker("setDate", "<?php echo $dateFrom; ?>");
		$("#dateTo").datepicker("setDate", "<?php echo $dateTo; ?>");
	});

	function saveAttIn(txtArea, empID, strDate) {
		var strNotes = $('#' + txtArea).val();
		alert(txtArea +" + "+ empID + " + "+ strNotes); 
		$.ajax({
			url: base_url + "/ajax/saveAttIn/",
			data: "emp_id=" + empID + "&att_date=" + strDate + "&att_in=" + strNotes,
			dataType: "text",
			type: "POST",
			success: function (msg) {
				alert(msg);
				// $("#frmAttDetail").submit();
				window.location.reload();
			},
			error: function (msg) {
				alert('Some Error Occurred, Please Try Again.');
			}
		})
	}
	
	function UpdateAttIn(txtArea, empID, strDate) {
		var strNotes = $('#' + txtArea).val();
		// alert(txtArea +" + "+ empID + " + "+ strNotes); 
		$.ajax({
			url: base_url + "/ajax/UpdateAttIn/",
			data: "emp_id=" + empID + "&att_date=" + strDate + "&att_in=" + strNotes,
			dataType: "text",
			type: "POST",
			success: function (msg) {
				alert(msg);
				// $("#frmAttDetail").submit();
				window.location.reload();
			},
			error: function (msg) {
				alert('Some Error Occurred, Please Try Again.');
			}
		})
	}
	
	function UpdateAttOut(txtArea, empID, strDate, att_in) {
		var strNotes = $('#' + txtArea).val();
		// alert(txtArea +" + "+ empID + " + "+ strNotes); 
		$.ajax({
			url: base_url + "/ajax/UpdateAttOut/",
			data: "emp_id=" + empID + "&att_date=" + strDate + "&att_out=" + strNotes + "&att_in=" + att_in,
			dataType: "text",
			type: "POST",
			success: function (msg) {
				alert(msg);
				window.location.reload();
			},
			error: function (msg) {
				alert('Some Error Occurred, Please Try Again.');
			}
		})
	}

	function saveAttOut(txtArea, empID, strDate, att_in) {
		var strNotes = $('#' + txtArea).val();
		// alert(txtArea +" + "+ empID + " + "+ strNotes); 
		$.ajax({
			url: base_url + "/ajax/saveAttOut/",
			data: "emp_id=" + empID + "&att_date=" + strDate + "&att_out=" + strNotes + "&att_in=" + att_in,
			dataType: "text",
			type: "POST",
			success: function (msg) {
				alert(msg);
				window.location.reload();
			},
			error: function (msg) {
				alert('Some Error Occurred, Please Try Again.');
			}
		})
	}
</script>
<?php
	if(count($arrAllDates) > 0)
	{
?>
<form name="frmAttDetail" id="frmAttDetail" method="post" action="<?php echo $frmActionURL; ?>">
	<div class="searchBoxMain">
		<div class="searchHeader">Search Criteria</div>
		<div class="searchcontentmain">
			<?php
			if (count($arrEmployees) > 0) {
			?>
				<div class="searchCol">
					<div class="labelContainer">Employee:</div>
					<div class="textBoxContainer">
						<select name="empID" id="empID" class="dropDown select-state">
							<option value="">Select Employee</option>
							<?php
							if (count($arrEmployees)) {
								foreach ($arrEmployees as $key => $arrEmp) {
							?>
									<optgroup label="<?php echo $key; ?>">
										<?php for ($i = 0; $i < count($arrEmp); $i++) { ?>
											<option value="<?php echo $arrEmp[$i]['emp_id']; ?>"><?php echo $arrEmp[$i]['emp_code'].' --- '.$arrEmp[$i]['emp_full_name']; ?></option>
										<?php } ?>
									</optgroup>
							<?php	}
							}
							?>
						</select>
					</div>
				</div>
			<?php
			}
			?>
			<div class="searchCol">
				<div class="labelContainer">Month:</div>
				<div class="textBoxContainer">
					<select id="selMonth" name="selMonth" class="dropDown select-state">
						<option value="">Month</option>
						<option value="01">Jan</option>
						<option value="02">Feb</option>
						<option value="03">Mar</option>
						<option value="04">Apr</option>
						<option value="05">May</option>
						<option value="06">Jun</option>
						<option value="07">Jul</option>
						<option value="08">Aug</option>
						<option value="09">Sep</option>
						<option value="10">Oct</option>
						<option value="11">Nov</option>
						<option value="12">Dec</option>
					</select>
				</div>
			</div>
			<div class="searchCol">
				<div class="labelContainer">Year:</div>
				<div class="textBoxContainer">
					<select id="selYear" name="selYear" class="dropDown select-state">
						<option value="">Year</option>
						<?php for ($ind = $this->HRMYearStarted; $ind <= date('Y'); $ind++) { ?>
							<option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="buttonContainer">
				<input type="hidden" name="txtExport" id="txtExport" value="0" />
				<input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search" onclick="$('#txtExport').val('0')">
				<!-- <input class="searchButton" name="btnExport" id="btnExport" type="submit" value="Export PDF" onclick="$('#txtExport').val('1')"> -->
				
			</div>
		</div>
	</div>
	<script>
		$('#empID').val('<?php echo $empID; ?>');
		$('#selMonth').val('<?php echo $selMonth; ?>');
		$('#selYear').val('<?php echo $selYear; ?>');		
	</script>
</form>
<?php if (in_array($this->userRoleID,array(WEB_ADMIN_ROLE_ID))){ ?>
	<form method="post" id="import_csv" enctype="multipart/form-data" action="">
		<input type="file" id="myFile" name="csv_file" id="csv_file" accept=".csv" />
		<button type="submit" name="import_csv" class="searchButton" id="import_csv_button">Import CSV</button>
	</form>
	<div id="imported_csv_data"></div>
	<?php } ?>
<script>


$('#import_csv').on('submit', function(event){
	event.preventDefault();
	$.ajax({
		url:"<?php echo base_url(); ?>attendance_management/importcsv",
		method:"POST",
		data:new FormData(this),
		contentType:false,
		cache:false,
		processData:false,
		beforeSend:function(){
			$('#import_csv_button').html('Importing...');
		},
		success:function(res)
		{
			console.log(res);
			$('#import_csv')[0].reset();
			$('#import_csv_button').attr('disabled', false);
			$('#import_csv_button').html('Import Done');
		}
	})
});

</script>
<div class="centerElementsContainer">
	<div class="recordCountContainer">
		<b>Legends:</b>&nbsp;&nbsp;&nbsp;<span style="background-color:#8FB05C;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span> <span class="mandatoryStar"> Off Day</span>
	</div>
	<?php
	if ($pageLinks) {
	?>
		<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
	<?php }	?>
</div>
<div class="listContentMain">
	
	<table cellspacing="0" cellpadding="0" class="listTableMain">
		<thead>
			<tr class="listHeader">
				<td class="listHeaderCol">Date</td>
				<!-- cast(checktime) -->
				<td class="listHeaderCol">Day</td>
				<!-- cast(checktime) -->
				<td class="listHeaderCol">In</td>
				<!-- min(checktime) -->
				<td class="listHeaderCol">Out</td>
				<!-- max(checktime) -->
				<td class="listHeaderCol">Working Hrs</td>
				<td class="listHeaderCol">Leave Applied</td>
				<td class="listHeaderCol">Notes</td>
				
			</tr>
		</thead>
		<tbody>
		<?php
		$Total_hours = [];

		$newdate 			= date("Y-m-", strtotime ( '+1 month', strtotime ( $arrAllDates[0] ) ));
		$newdate 			= $newdate.'26';
		$holidays			= 0;
		$leave				= 0;
		$absent				= 0;
		$workDays			= 0;
		$interval 			= new DateInterval('P1D');
		$start 				= new DateTime($arrAllDates[0]);
		$end_emp_month 		= new DateTime($newdate);
		$period 			= new DatePeriod($start, $interval, $end_emp_month);
		foreach ($period as $dt)
		{
			if ($dt->format('N') != 7)
			{
				$daily_attendance = $this->attandance->fetch_daily_attendance($empID, $dt->format('Y-m-d'));
				if(!empty(getIfLeaveApplied($empID, $dt->format('Y-m-d')))) 
				{
					$leave++;
				}elseif(!empty(getHolidayLeave($dt->format('Y-m-d')))) 
				{
					$holidays++;
				}elseif((empty($daily_attendance) || $daily_attendance['att_in'] == '00:00:00' && $daily_attendance['att_out'] == '00:00:00') && empty(getIfLeaveApplied($empID, $dt->format('Y-m-d'))) && empty(getHolidayLeave($dt->format('Y-m-d'))) )
				{
					$absent++;
				} else {
					$workDays++;
				}
			}else{
				$next = date('d', strtotime('+1 day', strtotime( $dt->format('Y-m-d') ) ) );
				$prev = date('d', strtotime('-1 day', strtotime( $dt->format('Y-m-d') ) ) );
				$next_attendance = $this->attandance->fetch_daily_attendance($empID, date('Y-m-d', strtotime('+1 day', strtotime( $dt->format('Y-m-d') ))) );
				$prev_attendance = $this->attandance->fetch_daily_attendance($empID, date('Y-m-d', strtotime(' -1 day', strtotime( $dt->format('Y-m-d')))) );
				if ($next != 26 || $prev != 24) {
					if (($next_attendance['att_in'] == '00:00:00' && $next_attendance['att_out'] == '00:00:00' ) && ($prev_attendance['att_in'] == '00:00:00' && $prev_attendance['att_out'] == '00:00:00') && empty(getIfLeaveApplied($empID, $dt->format('Y-m-d'))) && empty(getHolidayLeave($dt->format('Y-m-d')))) {
						$absent++;
					}
				}
			}
		}
		$branchTiming 	= $this->employee->headBranchHaveBranch($empID);
		$to_time 		= strtotime($branchTiming[0]['time_in']);
		$from_time 		= strtotime($branchTiming[0]['time_out']);
		$workHours 		= $workDays * (abs($to_time - $from_time) / 3600);

		for ($ind = 0; $ind < count($arrAllDates); $ind++) {
			$showClocking = true;
			$dateIndex = array_search($arrAllDates[$ind], array_column($arrRecords, 'DATE'));
			if ($dateIndex !== false) {
				$jnd = $dateIndex;
			} else {
				$jnd = -1;
				$showClocking = false;
			}
			$rowBGColor = '';
			$publicHoliday = '';
			
			$TW_date 	= readableDate($arrAllDates[$ind], 'd/m/Y');
			$TW_month 	= readableDate($arrAllDates[$ind], 'm');
			$TW_year 	= readableDate($arrAllDates[$ind], 'Y');
			$myTime 	= strtotime($TW_date); 
			$daysInMonth = cal_days_in_month(CAL_GREGORIAN, $TW_month, $TW_year);
			
			for ($i=1; $i <=$daysInMonth ; $i++) { 
				// $day = date("D", strtotime($TW_month.'/'.$i.'/'. $TW_year));
				// $dayFormat = date("Y-m-d", strtotime($TW_month.'/'.$i.'/'. $TW_year));
				// if($day != "Sun"){
				// 	$daily_attendance = $this->model_attendance_management->fetch_daily_attendance($empID, $arrAllDates[$ind]);
				// 	if(!empty(getHolidayLeave($dayFormat))) 
				// 	{
				// 		$holidays++;
				// 	}else if(empty($daily_attendance) && empty(getIfLeaveApplied($empID, $arrAllDates[$ind])) && empty(getIfHalfLeaveApplied($empID, $arrAllDates[$ind])) && empty(getHolidayLeave($dayFormat)) )
				// 	{
				// 		$absent++; 
				// 	}else{
				// 		$workDays++;
				// 	}
				// }
			}
			

			$holidayIndex = array_search($arrAllDates[$ind], array_column($arrHolidays, 'holiday_date'));
			// if (date('N', strtotime($arrAllDates[$ind])) == $arrEmployee['company_weekly_off_1'] || date('N', strtotime($arrAllDates[$ind])) == $arrEmployee['company_weekly_off_2'] || $holidayIndex !== false) {
			if (date('N', strtotime($arrAllDates[$ind])) == $arrEmployee['company_weekly_off_2'] || $holidayIndex !== false) {
			
				$rowBGColor = ' bgcolor="#8FB05C" style="color:#FFF"';
				$showClocking = false;
				if ($holidayIndex !== false) {
					$publicHoliday = $arrHolidays[$holidayIndex]['holiday_name'];
					$showClocking = false;
				}
			}

			if ($arrAllDates[$ind] == date('Y-m-d')) {
				$arrRecords[$jnd]['OUT'] = '';
			}
			?>
			<tr <?php echo $rowBGColor; ?> height="30px">
				<!-- Date -->
				<td class="listContentCol"><?php echo readableDate($arrAllDates[$ind], 'jS M, Y'); ?></td>
				<!-- days -->
				<td class="listContentCol"><?php echo date('l', strtotime($arrAllDates[$ind])); ?></td>
				<!-- check IN -->
				<td class="listContentCol">
					<?php 
					$dateupdate = readableDate($arrAllDates[$ind], 'Y-m-d');
					$record = $this->model_attendance_management->fetch_attendance_by_date($empID, $dateupdate);
					if ($this->userRoleID == WEB_ADMIN_ROLE_ID) {
						if (!empty($record['att_in']) ){
							?>
								<input type="time" name="att_in<?php echo $arrAllDates[$ind]; ?>" id="att_in<?php echo $arrAllDates[$ind]; ?>" class="textBox" value="<?php echo $record['att_in']; ?>">&nbsp;
								<input class="searchButton" name="btnSubmit" id="btnSubmit" type="button" value="Update" onclick="UpdateAttIn('att_in<?php echo $arrAllDates[$ind]; ?>', '<?php echo $empID; ?>', '<?php echo $arrAllDates[$ind]; ?>')">
							<?php
							}else{
								?>
								<input type="time" name="att_in<?php echo $arrAllDates[$ind]; ?>" id="att_in<?php echo $arrAllDates[$ind]; ?>" class="textBox" value="<?php echo $record['att_in']; ?>">&nbsp;
								<input class="searchButton" name="btnSubmit" id="btnSubmit" type="button" value="Save" onclick="saveAttIn('att_in<?php echo $arrAllDates[$ind]; ?>', '<?php echo $empID; ?>', '<?php echo $arrAllDates[$ind]; ?>')">
								<?php
							}
					}else if(!empty($record)){
						echo $record['att_in'];
					}
					?>
				</td>
				<!-- Check Out -->
				<td class="listContentCol">
					<?php 
					if ($this->userRoleID == WEB_ADMIN_ROLE_ID) {
						if ( !empty($record['att_in']) && !empty($record['att_out']) ){ ?>
							<input type="time" name="att_out<?php echo $arrAllDates[$ind]; ?>" id="att_out<?php echo $arrAllDates[$ind]; ?>" class="textBox" value="<?php echo $record['att_out']; ?>">&nbsp;
							<input class="searchButton" name="btnSubmit" id="btnSubmit" type="button" value="Update" onclick="UpdateAttOut('att_out<?php echo $arrAllDates[$ind]; ?>', '<?php echo $empID; ?>', '<?php echo $arrAllDates[$ind]; ?>', '<?php echo $record['att_in']; ?>')">
						<?php
					}else if( !empty($record['att_in']) && empty($record['att_out']) ){ ?>
							<input type="time" name="att_out<?php echo $arrAllDates[$ind]; ?>" id="att_out<?php echo $arrAllDates[$ind]; ?>" class="textBox" value="<?php echo $record['att_out']; ?>">&nbsp;
							<input class="searchButton" name="btnSubmit" id="btnSubmit" type="button" value="Save" onclick="saveAttOut('att_out<?php echo $arrAllDates[$ind]; ?>', '<?php echo $empID; ?>', '<?php echo $arrAllDates[$ind]; ?>', '<?php echo $record['att_in']; ?>' )">
					<?php 
					}
					}else if(!empty($record)){
						echo $record['att_out'];
					} ?>
				</td>
				

				<!-- working hours  -->
				<td class="listContentCol" width="100px">
					<?php
					if ($publicHoliday != '') {
						echo $publicHoliday;
					 } else {
						foreach ($data1 as $key => $value) { 
							if ($empID == $value['employee_id']  && $dateupdate == $value['attendance_date']){
								$worktime_in = $value['attendance_in'];
								$worktime_out = $value['attendance_out'];
								if (!empty($worktime_in) && !empty($worktime_out) ) {
									echo number_format(((((strtotime($worktime_out) - strtotime($worktime_in)) / 60) /60) ), 2);
								}
								$Total_hours[] = ((((strtotime($worktime_out) - strtotime($worktime_in)) / 60) /60) );
								break;
							}
						}
					}
					if ($arrRecords[$jnd]['IN'] != '' && $arrRecords[$jnd]['OUT'] != '') {
						$strTime1 = new DateTime($arrRecords[$jnd]['IN']);
						$strTime2 = new DateTime($arrRecords[$jnd]['OUT']);
						$intInterval = $strTime1->diff($strTime2);
						if ($intInterval->format('%h') > 0 || $intInterval->format('%i') > 0) {
							echo $intInterval->format('%h') . " hr " . $intInterval->format('%i') . " min";
						}
					}
					?>
				</td>

				<td class="listContentCol" width="100px">
					<!-- Leave -->
					<?php
					$strLeave = getIfLeaveApplied($empID, $arrAllDates[$ind]);
					echo ($strLeave != '') ? $strLeave : '-';
					?>
				</td>
				<td class="listContentCol" width="300px">
					<!-- Note -->
					<?php
					$strNotes = getAttendanceNotes($empID, $arrAllDates[$ind]);

					if ($canWrite == YES) {
					?>
						<input type="text" name="txtNote<?php echo $arrAllDates[$ind]; ?>" id="txtNote<?php echo $arrAllDates[$ind]; ?>" class="textBox" value="<?php echo $strNotes; ?>">&nbsp;
						<input class="searchButton" name="btnSubmit" id="btnSubmit" type="button" value="Save" onclick="saveNotes('txtNote<?php echo $arrAllDates[$ind]; ?>', '<?php echo $empID; ?>', '<?php echo $arrAllDates[$ind]; ?>')">
					<?php
					} else {
						echo ($strNotes != '') ? $strNotes : '-';
					}
					?>
				</td>
				<?php if (false) { ?>
					<td class="listContentCol" align="center"><?php if ($showClocking) { ?><img src="<?php echo $this->imagePath; ?>/clocking-icon.png" alt="Clocking" onclick="showPopup('<?php echo $this->baseURL . '/' . $this->currentController . '/clocking_detail/' . $empID . '/' . $arrAllDates[$ind]; ?>', 650, 500)" style="cursor:pointer" /><?php } ?></td>
				<?php } ?>
				</tr>
			<?php
		}
			?>
			<?php
			if (!$ind) {
			?>
				<tr class="listContentAlternate">
					<td colspan="7" align="center" class="listContentCol">No Record Found</td>
				</tr>
			<?php
			}
			?>
		</tbody>
	</table>
</div>

<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
	<tr class="listHeader">
		<td class="listHeaderCol">Total Working Hours</td>
		<td class="listHeaderCol">Total Hours Should Be</td>
	</tr>
	<tr>
	<td class="listContentCol"><?php echo number_format(array_sum($Total_hours), 2); ?></td>
		<td class="listContentCol"><?php echo $workHours; ?></td>
	</tr>
	<tr class="listHeader">
		<td class="listHeaderCol">Total Working Days</td>
		<td class="listHeaderCol">Total Absents</td>
		<td class="listHeaderCol">Total holidays</td>
	</tr>
	<tr>
	<td class="listContentCol"><?php echo $workDays; ?></td>
		<td class="listContentCol"><?php echo $absent; ?></td>
		<td class="listContentCol"><?php echo $holidays; ?></td>
	</tr>
		
</table>
<?php } ?>
