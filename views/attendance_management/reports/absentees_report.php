<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( ".datePicker" ).datepicker( "setDate", "<?php echo $selectedDate; ?>" );
	$( ".datePicker" ).datepicker( "option", "maxDate", "0" );
});
</script>
<form name="frmAttReport" id="frmAttReport" method="post" action="<?php echo $frmActionURL; ?>">
  <div class="searchBoxMain">
    <div class="searchHeader">Report - Search Criteria</div>
    <div class="searchcontentmain">
      <div class="searchCol">
        <div class="labelContainer">Date:</div>
        <div class="textBoxContainer">
        	<input type="text" class="textBox datePicker" id="selectedDate" name="selectedDate" />
        </div>
      </div>
      <div class="buttonContainer">
        <input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search">
        <input class="searchButton" name="btnBack" id="btnBack" type="button" value="Back" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController; ?>';">
      </div>
    </div>
  </div>
</form>
<div class="centerElementsContainer">
	<div class="recordCountContainer">
		<?php echo "Total Records Count: " . count($arrEmployees); ?>
    </div>
</div>
<div class="listContentMain">
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
  <tr class="listHeader">
    <td class="listHeaderCol">Employee Code</td>
    <td class="listHeaderCol">Employee Name</td>
    <td class="listHeaderCol center">Call</td>
  </tr>
  <?php	
    for($ind = 0; $ind < count($arrEmployees); $ind++) {
	?>
  <tr height="30px">
    <td class="listContentCol"><?php echo $arrEmployees[$ind]['emp_code']; ?></td>
    <td class="listContentCol"><?php echo $arrEmployees[$ind]['emp_full_name']; ?></td>
    <td class="listContentCol" align="center">	
	
        <?php if(trim($arrEmployees[$ind]['emp_mobile']) != '' && isAdmin($this->userRoleID)) { ?>
        <img title="Call" style="margin:-7px 0;cursor:pointer" width="25px" height="25px" src="<?php echo $this->imagePath . '/call.png';?>" onclick="showPopup('<?php echo $this->baseURL . '/employee_management/call_employee/' . $arrEmployees[$ind]['emp_id']; ?>', 650, 470);">
        <?php } ?>
    
    </td>  
  </tr>
  <?php } ?>
  <?php
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="3" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
  	?>
</table>
</div>
<div class="clear">&nbsp;</div>