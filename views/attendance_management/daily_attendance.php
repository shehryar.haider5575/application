

<?php
$empID 		= isset($_POST['empID']) 		? $_POST['empID'] 		: $empID;
$selMonth 	= isset($_POST['selMonth']) 	? $_POST['selMonth'] 	: $selMonth;
$selYear 	= isset($_POST['selYear']) 		? $_POST['selYear'] 	: $selYear;
$employee_id = array();
$attendance_date = array();
$attendance_in = array();
$attendance_out = array();
// if ($fetch_timing->num_rows() > 0) {
//     foreach ($fetch_timing->result() as $row) {
//         array_push($employee_id, $row->employee_id);
// 		array_push($att_date, $row->att_date);
// 		array_push($att_in, $row->att_in);
// 		array_push($att_out, $row->att_out);
// 	}
//     echo "fdsk";exit;
// } else {
//     echo "no data found";
// }	
// $data1 = [];
// foreach ($fetch_timing->result() as $key => $row) {
// 	$data1[$key]['employee_id'] = $row->employee_id;
// 	$data1[$key]['attendance_date'] = $row->att_date;
// 	$data1[$key]['attendance_in'] = $row->att_in;
// 	$data1[$key]['attendance_out'] = $row->att_out;
// }

$emp_key = array_search($empID, $employee_id);
$worktime_in;
$worktime_out;
?>
<style>
	.selectize-dropdown.single.dropDown{
		min-height: 100px !important;
	}
	.item{
		width: 100%;
		height: 0rem;
	}
	h1.header_title_page {
		text-align: center;
		font-size: 40px;
	}
</style>
<script>
	$(function() {
		$(".datePicker").datepicker({
			changeMonth: true,
			changeYear: true
		});
		$(".datePicker").datepicker("option", "dateFormat", "<?php echo $dateFormat; ?>");
		$(".datePicker").datepicker("option", "minDate", '<?php echo date('Y-m-d', strtotime('6 months ago')); ?>');
		$("#dateFrom").datepicker("setDate", "<?php echo $dateFrom; ?>");
		$("#dateTo").datepicker("setDate", "<?php echo $dateTo; ?>");
	});

	function saveAttIn(txtArea, empID, strDate) {
		var strNotes = $('#' + txtArea).val();
		// alert(txtArea +" + "+ empID + " + "+ strNotes); 
		$.ajax({
			url: base_url + "/ajax/saveAttIn/",
			data: "emp_id=" + empID + "&att_date=" + strDate + "&att_in=" + strNotes,
			dataType: "text",
			type: "POST",
			success: function (msg) {
				alert(msg);
				// $("#frmAttDetail").submit();
				window.location.reload();
			},
			error: function (msg) {
				alert('Some Error Occurred, Please Try Again.');
			}
		})
	}
	
	function UpdateAttIn(txtArea, empID, strDate) {
		var strNotes = $('#' + txtArea).val();
		// alert(txtArea +" + "+ empID + " + "+ strNotes); 
		$.ajax({
				url: base_url + "/ajax/UpdateAttIn/",
			data: "emp_id=" + empID + "&att_date=" + strDate + "&att_in=" + strNotes,
			dataType: "text",
			type: "POST",
			success: function (msg) {
				alert(msg);
				// $("#frmAttDetail").submit();
				window.location.reload();
			},
			error: function (msg) {
				alert('Some Error Occurred, Please Try Again.');
			}
		})
	}
	
	function UpdateAttOut(txtArea, empID, strDate, att_in) {
		var strNotes = $('#' + txtArea).val();
		// alert(txtArea +" + "+ empID + " + "+ strNotes); 
		$.ajax({
			url: base_url + "/ajax/UpdateAttOut/",
			data: "emp_id=" + empID + "&att_date=" + strDate + "&att_out=" + strNotes + "&att_in=" + att_in,
			dataType: "text",
			type: "POST",
			success: function (msg) {
				alert(msg);
				window.location.reload();
			},
			error: function (msg) {
				alert('Some Error Occurred, Please Try Again.');
			}
		})
	}

	function saveAttOut(txtArea, empID, strDate, att_in) {
		var strNotes = $('#' + txtArea).val();
		// alert(txtArea +" + "+ empID + " + "+ strNotes); 
		$.ajax({
			url: base_url + "/ajax/saveAttOut/",
			data: "emp_id=" + empID + "&att_date=" + strDate + "&att_out=" + strNotes + "&att_in=" + att_in,
			dataType: "text",
			type: "POST",
			success: function (msg) {
				alert(msg);
				window.location.reload();
			},
			error: function (msg) {
				alert('Some Error Occurred, Please Try Again.');
			}
		})
	}

	function saveNotes(txtArea, empID, strDate) {
		var strNotes = $('#' + txtArea).val();
		$.ajax({
			url: base_url + "/ajax/saveAttNotes/",
			data: "emp_id=" + empID + "&att_date=" + strDate + "&att_notes=" + strNotes,
			dataType: "text",
			type: "POST",
			success: function(msg) {
				alert('Note Saved!');
			},
			error: function(msg) {
				alert('Some Error Occurred, Please Try Again.');
			}
		})
	}

</script>
<?php
	if(count($arrEmployees) > 0)
	{
?>
<h1 class="header_title_page">Daily Attandance Report</h1>
<div class="centerElementsContainer">
	
</div>
<div class="listContentMain">
	<table cellspacing="0" cellpadding="0" class="listTableMain">
		<thead>
			<tr class="listHeader">
				<td class="listHeaderCol">Employee Name</td>
				<td class="listHeaderCol">Date</td>
				<td class="listHeaderCol">In</td>
				<td class="listHeaderCol">Out</td>			
				<td class="listHeaderCol">Note</td>			
			</tr>
		</thead>
		<tbody>
		<?php
		$Total_hours = [];
		
		for ($ind = 0; $ind < count($arrEmployees); $ind++) {
			// print_r($arrEmployees);exit;
			if ($arrAllDates[$ind] == date('Y-m-d')) {
				$arrRecords[$jnd]['OUT'] = '';
			}
			?>
			<tr <?php echo $rowBGColor; ?> height="30px">
				<!-- Date -->
				<td class="listContentCol"><?php echo $arrEmployees[$ind]['emp_full_name']; ?></td>
				<!-- days -->
				<td class="listContentCol"><?php echo readableDate($arrEmployees[$ind]['date'], 'jS M, Y'); ?></td>
				<!-- check IN -->
				<td class="listContentCol">
					<?php 
					$dateupdate = readableDate($arrEmployees[$ind]['date'], 'Y-m-d');
					// if (in_array($this->userRoleID,array(26))) {
						if (!empty($arrEmployees[$ind]['att_in']) ){
						?>
							<input type="time" name="att_in<?php echo $$arrEmployees[$ind]['emp_id']; ?>" id="att_in<?php echo $arrEmployees[$ind]['emp_id']; ?>" class="textBox" value="<?php echo $arrEmployees[$ind]['att_in']; ?>">&nbsp;
							<input class="searchButton" name="btnSubmit" id="btnSubmit" type="button" value="Update" onclick="UpdateAttIn('att_in<?php echo $arrEmployees[$ind]['emp_id']; ?>', '<?php echo $arrEmployees[$ind]['emp_id']; ?>', '<?php echo $arrEmployees[$ind]['date']; ?>')">
						<?php
						}else{
							?>
							<input type="time" name="att_in<?php echo $arrEmployees[$ind]['emp_id']; ?>" id="att_in<?php echo $arrEmployees[$ind]['emp_id']; ?>" class="textBox" value="<?php echo $arrEmployees[$ind]['att_in']; ?>">&nbsp;
							<input class="searchButton" name="btnSubmit" id="btnSubmit" type="button" value="Save" onclick="saveAttIn('att_in<?php echo $arrEmployees[$ind]['emp_id']; ?>', '<?php echo $arrEmployees[$ind]['emp_id']; ?>', '<?php echo $arrEmployees[$ind]['date']; ?>')">
							<?php
						}
					// }
					?>
				</td>
				<!-- Check Out -->
				<td class="listContentCol">
					<?php 
					// if (in_array($this->userRoleID,array(26))) {
						if ( !empty($arrEmployees[$ind]['att_in']) && !empty($arrEmployees[$ind]['att_out']) ){ ?>
								<input type="time" name="att_out<?php echo $arrEmployees[$ind]['emp_id']; ?>" id="att_out<?php echo $arrEmployees[$ind]['emp_id']; ?>" class="textBox" value="<?php echo $arrEmployees[$ind]['att_out']; ?>">&nbsp;
								<input class="searchButton" name="btnSubmit" id="btnSubmit" type="button" value="Update" onclick="UpdateAttOut('att_out<?php echo $arrEmployees[$ind]['emp_id']; ?>', '<?php echo $arrEmployees[$ind]['emp_id']; ?>', '<?php echo $arrEmployees[$ind]['date']; ?>', '<?php echo $arrEmployees[$ind]['att_in']; ?>')">
							<?php
						}else if( !empty($arrEmployees[$ind]['att_in']) && empty($arrEmployees[$ind]['att_out']) ){ ?>
								<input type="time" name="att_out<?php echo $arrEmployees[$ind]['emp_id']; ?>" id="att_out<?php echo $arrEmployees[$ind]['emp_id']; ?>" class="textBox" value="<?php echo $arrEmployees[$ind]['att_out']; ?>">&nbsp;
								<input class="searchButton" name="btnSubmit" id="btnSubmit" type="button" value="Save" onclick="saveAttOut('att_out<?php echo $arrEmployees[$ind]['emp_id']; ?>', '<?php echo $arrEmployees[$ind]['emp_id']; ?>', '<?php echo $arrEmployees[$ind]['date']; ?>', '<?php echo $arrEmployees[$ind]['att_in']; ?>' )">
						<?php 
						}
					// } ?>

				</td>
				<!-- Note -->
				<td class="listContentCol">
					<?php 
					// if (in_array($this->userRoleID,array(26))) {
						?>
						<input type="text" name="txtNote<?php echo $arrEmployees[$ind]['date'];  ?>" id="txtNote<?php echo $arrEmployees[$ind]['date'];  ?>" class="textBox" value="<?php echo $arrEmployees[$ind]['att_notes']; ?>">&nbsp;
						<input class="searchButton" name="btnSubmit" id="btnSubmit" type="button" value="Save" onclick="saveNotes('txtNote<?php echo $arrEmployees[$ind]['date'];  ?>', '<?php echo $arrEmployees[$ind]['emp_id'] ?>', '<?php echo $arrEmployees[$ind]['date'];  ?>')">
					<?php // } ?>
				</td>
			</tr>
		<?php
		} ?>
			<?php
			if (!$ind) {
			?>
				<tr class="listContentAlternate">
					<td colspan="7" align="center" class="listContentCol">No Record Found</td>
				</tr>
			<?php
			}
			?>
		</tbody>
	</table>
</div>

<?php } ?>
