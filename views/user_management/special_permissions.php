<?php
$txtSortField 	= (isset($_POST['sort_field'])) 	? $_POST['sort_field'] 		: 'l.created_date';
$txtSortOrder 	= (isset($_POST['sort_order'])) 	? $_POST['sort_order'] 		: 'DESC';
?>
<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( "#dateFrom" ).datepicker( "setDate", "<?php echo $dateFrom; ?>" );
	$( "#dateTo" ).datepicker( "setDate", "<?php echo $dateTo; ?>" );
});
</script>
<form name="frmSpecialPermissions" id="frmSpecialPermissions" method="post" action="<?php echo $frmActionURL; ?>">
  <div class="searchBoxMain">
    <div class="searchHeader">Search Criteria</div>
    <div class="searchcontentmain">
      <div class="searchCol">
        <div class="labelContainer">Employee:</div>
        <div class="textBoxContainer">
            <select id="empID" name="empID" class="dropDown">
                <option value="">All</option>
                <?php
				if (count($arrEmployees)) {
					foreach($arrEmployees as $key => $arrEmployee) {
				?>
                	<optgroup label="<?php echo $key; ?>">
						<?php for($i=0;$i<count($arrEmployee);$i++) { ?>					
                            <option value="<?php echo $arrEmployee[$i]['emp_id']; ?>"><?php echo $arrEmployee[$i]['emp_code']." - ".$arrEmployee[$i]['emp_full_name']; ?></option>
                        <?php } ?>
                    </optgroup>
				<?php	}
				}
				?>
            </select>
        </div>
        <div class="labelContainer">Module:</div>
        <div class="textBoxContainer">
            <select class="dropDown" id="moduleName" name="moduleName">
                <option value="">All</option>
                <?php
                if (count($arrModules)) {
                    foreach($arrModules as $arrModule) {
                ?>
                <option value="<?php echo $arrModule['module_name']; ?>"><?php echo $arrModule['display_name']; ?></option>
                <?php
                    }
                }
                ?>
            </select>
        </div>
      </div>
      <div class="searchCol">
        <div class="labelContainer">User Role:</div>
        <div class="textBoxContainer">
            <select class="dropDown" id="userRole" name="userRole">
                <option value="">All</option>
                <?php
                if (count($userRoles)) {
                    foreach($userRoles as $arrUserRole) {
                ?>
                    <option value="<?php echo $arrUserRole['user_role_id']; ?>"><?php echo $arrUserRole['user_role_name']; ?></option>
                <?php
                    }
                }
                ?>
            </select>
        </div>
      </div>
      <div class="buttonContainer">
        <input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search" onclick="$('#txtExport').val(0)">
		<input class="searchButton" type="reset" value="Reset">
      </div>
    </div>
  </div>
  <script>
  	$('#empID').val('<?php echo $empID; ?>');
	$('#moduleName').val('<?php echo $moduleName; ?>');
	$('#userRole').val('<?php echo $userRole; ?>');
  </script>
</form>

<?php if($canWrite == 1) { ?>
<div class="centerButtonContainer">
    <input class="addButton" type="button" value="Add New Super Permission" onclick="window.location.href = '<?php echo base_url() . $this->currentController . '/save_special_permission/' ?>';" />
</div>
<?php } ?>

<div class="centerElementsContainer">
	<div class="recordCountContainer">
		<?php echo "Total Records Count: " . $totalRecordsCount; ?>
    </div>
	
	<?php
		if($pageLinks) {
	?>
		<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
	<?php
    	}
	?>
</div>


<div class="listContentMain">
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain dottedBorder">
    <tr>
        <td class="formHeaderRow" colspan="7" style="padding-left: 8px">Activities</td>
    </tr>
	<tr class="listHeader">
    	<td class="listHeaderCol">Employee</td>
    	<td class="listHeaderCol">Department</td>
    	<td class="listHeaderCol">User Role</td>
    	<td class="listHeaderCol">Module</td>
    	<td class="listHeaderCol">Screen</td>
    	<td class="listHeaderCol">Assigned Role</td>
    	<td class="listHeaderCol" width="100px">Action</td>
    </tr>
    <?php
    for($ind = 0; $ind < count($arrPermissions); $ind++) {
	?>
    <tr class="listContent">
    	<td class="listContentCol"><?php echo ($arrPermissions[$ind]['emp_full_name'] != '') ? $arrPermissions[$ind]['emp_full_name'] : '-'; ?></td>
    	<td class="listContentCol"><?php echo ($arrPermissions[$ind]['job_category_name'] != '') ? $arrPermissions[$ind]['job_category_name'] : '-'; ?></td>
    	<td class="listContentCol"><?php echo ($arrPermissions[$ind]['actual_user_role'] != '') ? $arrPermissions[$ind]['actual_user_role'] : '-'; ?></td>
    	<td class="listContentCol"><?php echo $arrPermissions[$ind]['display_name']; ?></td>
    	<td class="listContentCol"><?php echo ($arrPermissions[$ind]['sub_module_name'] != '') ? $arrPermissions[$ind]['sub_module_name'] : '-'; ?></td>
    	<td class="listContentCol"><?php echo $arrPermissions[$ind]['user_role_name']; ?></td>
		<td class="listContentCol paddingTopBottom">
        	<?php if($canWrite == 1) { ?>
            <img title="View/Edit" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/view.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/save_special_permission/' . $arrPermissions[$ind]['emp_permission_id']; ?>';">
            <?php } if($canDelete == 1) { ?>
            <img title="Delete" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/delete.png';?>" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/', '<?php echo $arrPermissions[$ind]['emp_permission_id']; ?>');">
            <?php } ?>
        </td>
    </tr>
    <?php
	}
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="7" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
</div>
<div style="clear:both">&nbsp;<div>