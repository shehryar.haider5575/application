<?php echo "<pre>"; print_r($modulesAllowed); exit; ?>

<?php echo form_open(''); ?>

<table border="0" cellspacing="0" cellpadding="0" style="width:100%">
	<tr>
        <td class="loginLeftCol" align="left">User Role:</td>
        <td class="loginRightCol">
			<select id="user_role" name="user_role" class="" style="width:200px;">
				<option value="0">Select One</option>
				<?php
                if (count($userRoles)) {
					foreach($userRoles as $userRole) {
						$txtSelectedRole = '';
						if($_POST['user_role'] == $userRole['user_role_id']) {
							$txtSelectedRole = ' selected="true" ';
						}
				?>
					<option value="<?php echo $userRole['user_role_id']; ?>" <?php echo $txtSelectedRole; ?>><?php echo $userRole['user_role_name']; ?></option>
				<?php
                	}
				}
				?>
			</select>
		</td>
	</tr>
	<tr>
        <td class="loginLeftCol" align="left">Department:</td>
        <td class="loginRightCol">
			<select id="department" name="department" class="" style="width:200px;">
				<option value="0">Select One</option>
				<?php
				if (count($departments)) {
					foreach($departments as $department) {
						$txtSelectedDepartment = '';
						if($_POST['department'] == $department['department_id']) {
							$txtSelectedDepartment = ' selected="true" ';
						}
				?>
					<option value="<?php echo $department['department_id']; ?>" <?php echo $txtSelectedDepartment; ?>><?php echo $department['department_name']; ?></option>
				<?php
			}
		}
		?>
			</select>
		</td>
	</tr>
	<tr>
        <td class="loginLeftCol" align="left">Market:</td>
        <td class="loginRightCol">
			<select id="market" name="market" class="" style="width:200px;">
				<option value="0">Select One</option>
				<?php
				if (count($markets)) {
					foreach($markets as $market) {
						$txtSelectedMarket = '';
						if($_POST['market'] == $market['market_id']) {
							$txtSelectedMarket = ' selected="true" ';
						}
				?>
					<option value="<?php echo $market['market_id']; ?>" <?php echo $txtSelectedMarket; ?>><?php echo $market['market_name']; ?></option>
				<?php
			}
		}
		?>
			</select>
		</td>
	</tr>
	<tr>
        <td class="loginLeftCol" align="left">Employee Id:</td>
        <td class="loginRightCol">
			<!-- <select id="employee" name="employee" class="" style="width:200px;">
				< ?php if (count($userRoles)) {foreach($userRoles as $userRole) { ?>
					<option value="< ?php echo $userRole['user_role_id']; ?>">< ?php echo $userRole['user_role_name']; ?></option>
				< ?php }} ?>
			</select> -->
			<input class="textBox" type="text" name="employee_id" value="<?php echo set_value('employee_id'); ?>" size="50" />
		</td>
	</tr>
	<tr>
        <td class="loginLeftCol" align="left">User Name:</td>
        <td class="loginRightCol"><input class="textBox" type="text" name="login_name" value="<?php echo set_value('login_name'); ?>" size="50" /></td>
	</tr>
    <tr>
        <td class="loginLeftCol" align="left">Password:</td>
        <td class="loginRightCol"><input class="textBox" type="password" name="login_password" value="" size="50" /></td>
	</tr>
	<tr>
		<td class="loginLeftCol" align="left">Status:</td>
		<td>
			<select name="status" id="status" class="" style="width:125px;">
				<option value='1' selected="selected">Enable</option>
				<option value='0'>Disable</option>
			</select>
		</td>
	</tr>
	<tr>
		<td></td>
        <td class="loginButtonCol" align="left"><input class="smallButton" name="addUser" type="submit" value="Save"></td>
    </tr>
</table>

<?php echo form_close(); ?>