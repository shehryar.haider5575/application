<?php
  $txtSortField 	= (isset($_POST['sort_field'])) 	? $_POST['sort_field'] 		: '';
  $txtSortOrder 	= (isset($_POST['sort_order'])) 	? $_POST['sort_order'] 		: '';
?>
<!-- <form name="frmListPayroll" id="frmListPayroll" method="post" action="<?php echo $frmActionURL; ?>">
  <div class="searchBoxMain">
    <div class="searchHeader">Search Criteria</div>
    <div class="searchcontentmain">
      <div class="searchCol">
        <div class="labelContainer">Employee:</div>
        <div class="textBoxContainer">
			<select name="empID" id="empID" class="dropDown">
              <option value="">All</option>
              <?php
				if (count($arrEmployees)) {
					foreach($arrEmployees as $key => $arrEmp) {
				?>
					<optgroup label="<?php echo $key; ?>">
						<?php for($i = 0; $i < count($arrEmp); $i++) { ?>					
							<option value="<?php echo $arrEmp[$i]['emp_id']; ?>"><?php echo  $arrEmp[$i]['emp_code']. ' --- ' .$arrEmp[$i]['emp_full_name']; ?></option>
						<?php } ?>
					</optgroup>
				<?php	}
				}
				?>
          </select>
        </div>
      </div>
      <div class="searchCol">
        <div class="labelContainer">Month:</div>
          <div class="textBoxContainer">
            <select name="selMonth" id="selMonth" class="dropDown">
                <option value="">All</option>
                  <?php
          if (count($arrMonths)) {
            foreach($arrMonths as $strKey => $strValue) {
          ?>
          <option value="<?php echo $strKey; ?>"><?php echo $strValue; ?></option>
          <?php
            }
          }
          ?>
              </select>
          </div>
      </div>
      <div class="searchCol">
        <div class="labelContainer">Year:</div>
        <div class="textBoxContainer">
        	<select name="selYear" id="selYear" class="dropDown">
            	<option value="">All</option>
                <?php for($ind = $this->salaryYearStarted; $ind <= date('Y'); $ind++) { ?>
                <option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
                <?php } ?>
            </select>
        </div>
      </div>
      <div class="buttonContainer">
      	<input type="hidden" name="sort_field" id="sort_field" value="<?php echo $txtSortField; ?>" />
      	<input type="hidden" name="sort_order" id="sort_order" value="<?php echo $txtSortOrder; ?>" />
        <input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search">
      </div>
    </div>
  </div> 
  <script>
  	$('#empID').val('<?php echo $empID; ?>');
  	$('#empIP').val('<?php echo $empIP; ?>');
  	$('#empDesignation').val('<?php echo $empDesignation; ?>');
	$('#empDepartment').val('<?php echo $empDepartment; ?>');
  	$('#empSupervisor').val('<?php echo $empSupervisor; ?>');
  	$('#empCompany').val('<?php echo $empCompany; ?>');
  	$('#selMonth').val('<?php echo $selMonth; ?>');
  	$('#selYear').val('<?php echo $selYear; ?>');
  </script>
</form> -->


<div class="listContentMain">
    <table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
    <tr class="listHeader">
        <td class="listHeaderCol">Department Name</td>
        <td class="listHeaderCol">Month</td>
        <td class="listHeaderCol">Year</td>
        <td class="listHeaderCol">Nunber Of Employees</td>
        <td class="listHeaderColLast">Action</td>
    </tr>
    <?php
        for($ind = 0; $ind < count($fetch_timing); $ind++) { ?>
    <tr<?php echo $trCSSClass; ?> id="tr<?php echo $ind; ?>">
        <td class="listContentCol"><?php echo $fetch_timing[$ind]['job_category_name']; ?></td>
        <td class="listContentCol"><?php echo $fetch_timing[$ind]['month']; ?></td>
        <td class="listContentCol"><?php echo $fetch_timing[$ind]['years']; ?></td>
        <td class="listContentCol"><?php echo $fetch_timing[$ind]['emp']; ?></td>
        <td class="listContentColLast">
        <div class="empColButtonContainer">
            &nbsp;&nbsp;
            <img title="View" style="margin:-7px 0;cursor:pointer" width="25" src="<?php echo $this->imagePath . '/display.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/over_time/' . $fetch_timing[$ind]['emp_job_category_id'].'/'. $fetch_timing[$ind]['month'].'/'. $fetch_timing[$ind]['years'] ?>';">
            </div>
        </td>
    </tr>
    <?php } 
    if(!$ind) { ?>
        <tr class="listContentAlternate">
        <td colspan="7" align="center" class="listContentCol">No Record Found</td>
        </tr>
    <?php } ?>
    </table>
</div>
<div style="clear:both">&nbsp;<div>