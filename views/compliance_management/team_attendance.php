<?php
$strDate 			= (isset($_POST['strDate'])) 			? $_POST['strDate'] 			: $strDate;
?>
<script>
$(function() {
	$( ".datePicker" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( ".datePicker" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( ".datePicker" ).datepicker( "option", "minDate", '<?php echo date('Y-m-d', strtotime('6 months ago')); ?>' );
	$( ".datePicker" ).datepicker( "option", "maxDate", "0" );
	$( "#strDate" ).datepicker( "setDate", "<?php echo ($strDate != '') ? $strDate : date('Y-m-d'); ?>" );
});

function showResults(strType) {
	
	$('.isNormal').hide();
	$('.isOnTime').hide();
	$('.isLate').hide();
	$('.isHalfday').hide();
	$('.isAbsent').hide();
	$('#txtRecordType').val('');
	
	if(strType == 'isPresent') {
		$('.isNormal').show();
		$('.isOnTime').show();
		$('.isLate').show();
		$('.isHalfday').show();
		$('#txtRecordType').val(strType);
	} else if(strType != '') {
		$('.' + strType).show();
		$('#txtRecordType').val(strType);
	} else {
		$('.isNormal').show();
		$('.isOnTime').show();
		$('.isLate').show();
		$('.isHalfday').show();
		$('.isAbsent').show();
	}
	
	var intIndex = 1;
	
	$('.rowIndex:visible').each(function() {
	  	$(this).html(intIndex);
		intIndex++;
	});
}
</script>
<form name="frmAttDetail" id="frmAttDetail" method="post" action="<?php echo $frmActionURL; ?>">
  <div class="searchBoxMain">
    <div class="searchHeader">Search Criteria</div>
    <div class="searchcontentmain">
      <div class="searchCol">
      	<?php
        if(count($arrSupervisors) > 1) {
		?>
        <div class="labelContainer">Supervisor:</div>
        <div class="textBoxContainer">
          <select name="empSupervisor" id="empSupervisor" class="dropDown">
              <option value="">Select Supervisor</option>
              <?php if(isAdmin($this->userRoleID)) { ?>
              <option value="0">All Employees</option>
              <?php }
				if (count($arrSupervisors)) {
					foreach($arrSupervisors as $key => $arrSupervisor) {
				?>
					<optgroup label="<?php echo $key; ?>">
						<?php for($i=0;$i<count($arrSupervisor);$i++) { ?>					
							<option value="<?php echo $arrSupervisor[$i]['emp_id']; ?>"><?php echo $arrSupervisor[$i]['emp_code']." - ".$arrSupervisor[$i]['emp_full_name']; ?></option>
						<?php } ?>
					</optgroup>
				<?php	}
				}
				?>
          </select>
        </div>
        <?php
        }
		?>
      </div>
      <div class="searchCol">
        <div class="labelContainer">Date:</div>
        <div class="textBoxContainer">
        	<input type="text" class="textBox datePicker" name="strDate" id="strDate" />
        </div>
      </div>
      <div class="searchCol">
        <div class="errorMessage" style="width:325px; text-align:left; padding:0">
        	Attendance Update Time:
        </div>
        <div class="errorMessage" style="width:325px; text-align:left; padding:0">
            1:20 PM (Morning Shift Check-in / Night Shift Check-out)<br />
            1:20 AM (Morning Shift Check-out / Night Shift Check-in)
        </div>
      </div>
      <div class="buttonContainer">
      	<input type="hidden" name="txtExport" id="txtExport" value="0" />
      	<input type="hidden" name="txtRecordType" id="txtRecordType" value="" />
        <input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search" onclick="$('#txtExport').val('0')">
        <input class="searchButton" name="btnExport" id="btnExport" type="submit" value="Export To PDF" onclick="$('#txtExport').val('1')">
		<input class="searchButton" type="reset" value="Reset">
      </div>
    </div>
  </div>
  <script>
  	$('#empSupervisor').val('<?php echo $empSupervisor; ?>');
  </script>
</form>
<div class="centerElementsContainer">
	<div class="recordCountContainer">
		<?php echo "Total Records Count: " . $totalRecordsCount; ?>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<b>Legends:</b>&nbsp;&nbsp;&nbsp;<span style="background-color:#8FB05C;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span> <span class="mandatoryStar"> Off Day</span>
        &nbsp;&nbsp;&nbsp;<span style="background-color:#F9F084;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span> <span class="mandatoryStar"> Late</span>
        &nbsp;&nbsp;&nbsp;<span style="background-color:#F38374;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span> <span class="mandatoryStar"> Half Day</span>
        &nbsp;&nbsp;&nbsp;<span style="background-color:#EA4D42;border:1px dotted #00769C">&nbsp;&nbsp;&nbsp;&nbsp;</span> <span class="mandatoryStar"> Absent</span>
    </div>
	
	<?php
	if($pageLinks) {
	?>
	<div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
	<?php 	}	?>
</div>
<div class="listContentMain">
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
  <tr class="listHeader">
    <td class="listHeaderCol">#</td>
    <td class="listHeaderCol" width="200px">Employee</td>
    <td class="listHeaderCol" width="120px">Date</td>
    <td class="listHeaderCol" width="120px">Day</td>
    <td class="listHeaderCol" width="120px">Day Type</td>
    <td class="listHeaderCol">Shift Time</td>
    <td class="listHeaderCol">In</td>
    <td class="listHeaderCol">Out</td>
    <td class="listHeaderCol">Working Hrs</td>
    <!--<td class="listHeaderCol">Overtime</td>-->
    <td class="listHeaderCol">Short Hrs</td>
    <td class="listHeaderCol" width="160px">Leave Taken / Remarks</td>
    <?php if($displayClocking) { ?>
    <td class="listHeaderCol">Clocking</td>
    <?php } ?>
  </tr>
  <?php
  	$totalLates 	= 0;
  	$totalAbsents 	= 0;
  	$totalHalfDays 	= 0;
	$totalOnTime	= 0;
	$totalInMissing	= 0;
	
    for($ind = 0; $ind < count($arrRecords); $ind++) {
		
		$inMissing = false; # IN MISSING
		$thisTimeStamp = strtotime($arrRecords[$ind]['DATE']);
		
		if($thisTimeStamp >= $this->startRamadan && $thisTimeStamp <= $this->endRamadan) {
			$this->fullHours 					= 6.45;
			$this->halfdayHours 				= 6.15;
		} else {
			$this->fullHours 					= 8.45;
			$this->halfdayHours 				= 8.15;
		}
		
		if($thisTimeStamp >= $this->dateTimeChange && $thisTimeStamp >= $this->startRamadan && $thisTimeStamp <= $this->endRamadan) {
				
			$this->fullHalfDayHours 			= 4.55;
			$this->halfHalfDayHours 			= 4.30;
			$this->fullHours 					= 6.55;
			$this->halfdayHours 				= 6.30;
			$this->secHalfday 					= 1500;
			$this->lateMinSeconds 				= 300;
			$this->lateMaxTime 					= 0.25;
			
		} else if($thisTimeStamp >= $this->dateTimeChange) {
				
			$this->fullHalfDayHours 			= 4.55;
			$this->halfHalfDayHours 			= 4.30;
			$this->fullHours 					= 8.55;
			$this->halfdayHours 				= 8.30;
			$this->secHalfday 					= 1500;
			$this->lateMinSeconds 				= 300;
			$this->lateMaxTime 					= 0.25;
			
		}
			
		if(strtotime($arrRecords[$ind]['DATE']) < strtotime('now')) { # - 86400
						
			if($arrRecords[$ind]['DAYTYPE'] == 'WORKDAY') {
				if(($arrRecords[$ind]['IN'] == '' && $arrRecords[$ind]['OUT'] != '')) {
					$inMissing = true; # IN MISSING
					$totalInMissing++;
				}
			}
			
			$arrIN = explode(':', $arrRecords[$ind]['IN']);
			$strDay = strtoupper(readableDate($arrRecords[$ind]['DATE'], 'l'));
			
			$boolisLate = false;
			$boolisHalfday = false;
			
			if($arrRecords[$ind]['SHORT'] > 0 && $arrRecords[$ind]['SHORT'] <= $this->lateMaxTime) {
				$boolisLate = true;
			}			
			if(!$boolisLate && $arrRecords[$ind]['SHORT'] > $this->lateMaxTime) {
				$boolisHalfday = true;
			}
			
			$shiftTimeBoth = explode(':', $arrRecords[$ind]['SIN']);
			$shiftTime = $shiftTimeBoth[0];
			$shiftTimeMin = $shiftTimeBoth[1];
			
			if($thisTimeStamp >= $this->dateTimeChange) {
				if($shiftTimeMin == '00') {
					$shiftTimeMin = '55';
					if((int)$shiftTime > 0) {
						$shiftTime = $shiftTime - 1;
					} else {
						$shiftTime = '23';
					}
				} else {
					$shiftTimeMin = $shiftTimeMin - ($this->lateMinSeconds / 60);
				}

			} else {
				$shiftTimeMin += 10;
				if($shiftTimeMin == '00') {
					$shiftTimeMin = '45';
					if((int)$shiftTime > 0) {
						$shiftTime = $shiftTime - 1;
					} else {
						$shiftTime = '23';
					}
				} else {
					$shiftTimeMin = $shiftTimeMin - ($this->lateMinSeconds / 60);
				}
			}
			
			$rowBGColor = '';
			$showClocking = true;
			$isOff = false;
			$notToday = true;
			if($arrRecords[$ind]['DATE'] == date('Y-m-d')) {
				$notToday = false;
			}
			
			if($arrRecords[$ind]['DAYTYPE'] == 'OFFDAY' || $arrRecords[$ind]['DAYTYPE'] == 'HOLIDAY') {
				$rowBGColor = ' bgcolor="#8FB05C" style="color:#FFF" class="isNormal"';
				$isOff = true;
				$totalOffDays++;
			} else {
						
				if($arrRecords[$ind]['REMARK'] == '') { # && !$inMissing
				
					if($strDay == 'SATURDAY') {
				
						if (($arrRecords[$ind]['DAYTYPE'] == 'WORKDAY' && 
									$arrRecords[$ind]['IN'] == '') || 
									($arrRecords[$ind]['DAYTYPE'] == 'WORKDAY' && 
									$boolisHalfday && 
									$arrRecords[$ind]['OUT'] != '' && 
									$notToday)) {
							$rowBGColor = ' bgcolor="#EA4D42" style="color:#FFF" class="isAbsent"';
							$totalAbsents++;
						} else if ($boolisLate) {	# 15 MINUTES
							$rowBGColor = ' bgcolor="#F9F084" class="isLate"';
							$totalLates++;
						} else {
							$totalOnTime++;
							$rowBGColor = ' class="isOnTime"';							
						}
						
					} else {
				
						if (($arrRecords[$ind]['DAYTYPE'] == 'WORKDAY' && 
									$arrRecords[$ind]['IN'] == '') || 
									($arrRecords[$ind]['DAYTYPE'] == 'WORKDAY' && 
									$arrRecords[$ind]['WORK'] < $this->halfHalfDayHours && 
									$arrRecords[$ind]['OUT'] != '' && 
									$notToday)) {
							$rowBGColor = ' bgcolor="#EA4D42" style="color:#FFF" class="isAbsent"';
							$totalAbsents++;
						} else if(!$boolisLate && $boolisHalfday && $notToday) {
							$rowBGColor = ' bgcolor="#F38374" style="color:#FFF" class="isHalfday"';
							$totalHalfDays++;
						} else if ($boolisLate) {	# 15 MINUTES
							$rowBGColor = ' bgcolor="#F9F084" class="isLate"';
							$totalLates++;
						} else {
							$totalOnTime++;
							$rowBGColor = ' class="isOnTime"';							
						}
						
					}
				} /* else if($inMissing) {
					$rowBGColor = ' class="isNormal"';							
				} */ else {
					$rowBGColor = ' class="isNormal"';							
				}
			}
			
			if($arrRecords[$ind]['IN'] == '' && $arrRecords[$ind]['OUT'] == '') {
				$showClocking = false;
			}
	?>
  <tr<?php echo $rowBGColor; ?> height="30px">
  	<td class="listContentCol rowIndex" style="width:40px"><?php echo ($ind + 1); ?></td>
    <td class="listContentCol"><?php echo getValue($arrEmployees, 'emp_code', $arrRecords[$ind]['BADGENO'], 'emp_code') . ' - ' . getValue($arrEmployees, 'emp_code', $arrRecords[$ind]['BADGENO'], 'emp_full_name'); ?></td>
    <td class="listContentCol"><?php echo readableDate($arrRecords[$ind]['DATE'], 'jS M, Y'); ?></td>
    <td class="listContentCol"><?php echo $strDay; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['DAYTYPE']; ?></td>
    <td class="listContentCol"><?php echo (!$isOff) ? readableDate($shiftTime . ':' . $shiftTimeMin, 'g:i A') : '-'; ?></td>
    <td class="listContentCol"><?php echo ($arrRecords[$ind]['IN'] != '') ? readableDate($arrRecords[$ind]['IN'], 'g:i A') : '-'; ?></td>
    <td class="listContentCol"><?php echo ($arrRecords[$ind]['OUT'] != '' && $arrRecords[$ind]['DATE'] != date('Y-m-d')) ? readableDate($arrRecords[$ind]['OUT'], 'g:i A') : '-'; ?></td>
    <td class="listContentCol"><?php echo ($arrRecords[$ind]['WORK'] != '' && $arrRecords[$ind]['DATE'] != date('Y-m-d') && !$isOff) ? $arrRecords[$ind]['WORK'] . ' hr' : '-'; ?></td>
    <!--<td class="listContentCol"><?php //echo ($arrRecords[$ind]['OVERTIME'] != '' && $arrRecords[$ind]['DATE'] != date('Y-m-d') && !$isOff) ? $arrRecords[$ind]['OVERTIME'] . ' hr' : '-'; ?></td>-->
    <td class="listContentCol"><?php echo ($arrRecords[$ind]['SHORT'] != '' && $arrRecords[$ind]['DATE'] != date('Y-m-d') && !$isOff) ? $arrRecords[$ind]['SHORT'] . ' hr' : '-'; ?></td>
    <td class="listContentCol"><?php echo $arrRecords[$ind]['LEAVETYPE']; if(trim($arrRecords[$ind]['LEAVETYPE']) != '') { ?><br /><?php } echo $arrRecords[$ind]['REMARK']; ?></td>
    <?php if($displayClocking) { ?>
    <td class="listContentCol" align="center"><?php if($showClocking) { ?><img src="<?php echo $this->imagePath; ?>/clocking-icon.png" alt="Clocking" onclick="showPopup('<?php echo $this->baseURL . '/' . $this->currentController . '/clocking_detail/' . $arrRecords[$ind]['BADGENO'] . '/' . $arrRecords[$ind]['DATE']; ?>', 700, 600)" style="cursor:pointer" /><?php } ?></td>  
    <?php } ?>  
  </tr>
  <?php
		}
	} 
  ?>
  <?php
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="12" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
  	?>
</table>
</div>
<div class="clear"></div>
<?php if(count($arrRecords)) { ?>
<table class="listTableMain">
    <tr>
    	<td class="listContentCol" colspan="3" height="2px"></td>
    </tr>
    <tr class="listContent">
    	<td class="listContentCol" width="180px" style="font-size:18px">Total Records</td>
    	<td class="listContentCol" width="180px" style="font-size:18px; color:#F00"><?php echo (int)$totalRecordsCount; ?></td>
    	<td class="listContentCol normalLink" style="font-size:18px; cursor:pointer" onclick="showResults('')">Show All Records</td>
    </tr>
    <tr class="listContent">
    	<td class="listContentCol" style="font-size:18px">Present/Off Day</td>
    	<td class="listContentCol" style="font-size:18px; color:#F00"><?php echo (int)($totalRecordsCount - $totalAbsents); ?></td>
    	<td class="listContentCol normalLink" style="font-size:18px; cursor:pointer" onclick="showResults('isPresent')">Show Present Records</td>
    </tr>
    <tr class="listContent">
    	<td class="listContentCol" style="font-size:18px">On Time</td>
    	<td class="listContentCol" style="font-size:18px; color:#F00"><?php echo (int)$totalOnTime; ?></td>
    	<td class="listContentCol normalLink" style="font-size:18px; cursor:pointer" onclick="showResults('isOnTime')">Show On Time Records</td>
    </tr>
    <tr class="listContent">
    	<td class="listContentCol" style="font-size:18px">Late(s)</td>
    	<td class="listContentCol" style="font-size:18px; color:#F00"><?php echo (int)$totalLates; ?></td>
    	<td class="listContentCol normalLink" style="font-size:18px; cursor:pointer" onclick="showResults('isLate')">Show Late Records</td>
    </tr>
    <tr class="listContent">
    	<td class="listContentCol" style="font-size:18px">Halfday(s)</td>
    	<td class="listContentCol" style="font-size:18px; color:#F00"><?php echo (int)$totalHalfDays; ?></td>
    	<td class="listContentCol normalLink" style="font-size:18px; cursor:pointer" onclick="showResults('isHalfday')">Show Halfday Records</td>
    </tr>
    <tr class="listContent">
    	<td class="listContentCol" style="font-size:18px">Absent(s)</td>
    	<td class="listContentCol" style="font-size:18px; color:#F00"><?php echo (int)$totalAbsents; ?></td>
    	<td class="listContentCol normalLink" style="font-size:18px; cursor:pointer" onclick="showResults('isAbsent')">Show Absent Records</td>
    </tr>
    <tr class="listContent">
    	<td class="listContentCol" colspan="8">&nbsp;</td>
    </tr>
</table>
<div class="clear">&nbsp;</div>
<?php } ?>