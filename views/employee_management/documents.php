<form name="frmDocument" id="frmDocument" method="post">
	<div class="searchBoxMain">
    	<div class="searchHeader">Search Criteria</div>
    	<div class="searchcontentmain">
      		<div class="searchCol">
        		<div class="labelContainer">Employee:</div>
                <div class="textBoxContainer">
        			<select name="empID" id="empID" class="dropDown">
                      <option value="">Select Employee</option>
                      <?php
                        if (count($arrEmployees)) {
                            foreach($arrEmployees as $key => $arrEmp) {
                        ?>
                            <optgroup label="<?php echo $key; ?>">
                                <?php for($i = 0; $i < count($arrEmp); $i++) { ?>					
                                    <option value="<?php echo $arrEmp[$i]['emp_id']; ?>"><?php echo $arrEmp[$i]['emp_full_name']; ?></option>
                                <?php } ?>
                            </optgroup>
                        <?php	}
                        }
                        ?>
                  </select>
              	</div>
          	</div>
          	<div class="buttonContainer">
            	<input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Show Documents">
          	</div>
      	</div>
    </div>
</form>
<script type="text/javascript">
$('#empID').val('<?php echo $_POST['empID']; ?>');
</script>
<br />
<table border="0" cellspacing="0" cellpadding="0" class="listTableMain dottedBorder">
	<tr class="listHeader">
    	<td class="listHeaderCol" width="50%">Title</th>
    	<td class="listHeaderCol" width="20%">Document Type</th>
    	<td class="listHeaderCol" width="20%">Document</th>
    </tr>
    <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
	?>
    <tr class="listContent">
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['doc_title']; ?></td>
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['doc_type']; ?></td>
    	<td class="listContentCol"><?php echo ($arrRecords[$ind]['doc_file'] != '') ? '<a href="' . $this->baseURL . '/' . $empDocFolderShow . $arrRecords[$ind]['doc_file'] . '" style="color:#f00" target="_blank">View Document</a>' : ' - '; ?></td>
    </tr>
    <?php
	}
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="3" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>