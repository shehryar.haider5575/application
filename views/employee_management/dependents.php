<?php
$dependentName 			    = (isset($_POST['dependentName'])) 			    ? $_POST['dependentName'] 			    : $record['dependent_name'];
$dependentRelationship 	= (isset($_POST['dependentRelationship'])) 	? $_POST['dependentRelationship'] 	: $record['dependent_relationship'];
$dependent_contact 			= (isset($_POST['dependent_contact'])) 			? $_POST['dependent_contact'] 			: $record['dependent_contact'];
$dependentAge 			    = (isset($_POST['dependentAge'])) 			    ? $_POST['dependentAge'] 			      : $record['dependent_age'];
?>
<script>
$(function() {
	$( "#dependentDOB" ).datepicker({
									changeMonth: true,
									changeYear: true
									});
	$( "#dependentDOB" ).datepicker( "option", "dateFormat", "<?php echo $dateFormat; ?>" );
	$( "#dependentDOB" ).datepicker( "setDate", "<?php echo $dependentDOB; ?>" );
	$( "#dependentDOB" ).datepicker( "option", "maxDate", '<?php echo date('Y-m-d'); ?>' );
	
});
</script>

<?php if($canWrite == YES) { ?>
<form name="frmDependents" id="frmDependents" method="post">
  <div class="employeeFormMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
    <tr>
    	<td class="formHeaderRow" colspan="2">Add/Edit Emergency Contacts</td>
    </tr>
    <tr>
      <td class="formLabelContainer" width="20%"> Name:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer" align="left"><input type="text" name="dependentName" maxlength="100" id="dependentName" class="textBox" value="<?php echo $dependentName; ?>"></td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer"> Relationship:<span class="mandatoryStar"> *</span></td>
      <td class="formTextBoxContainer">
        <select id="dependentRelationship" name="dependentRelationship" class="dropDown">
            <option value="">Select Relationship</option>
            <?php
            if (count($arrRelationships)) {
                foreach($arrRelationships as $arrRelationship) {
            ?>
                <option value="<?php echo $arrRelationship; ?>"><?php echo $arrRelationship; ?></option>
            <?php
                }
            }
            ?>
        </select>
      </td>
    </tr>
    <tr>
        <td class="formLabelContainer"> dependent Contact:<span class="mandatoryStar"></span></td>
        <td class="formTextBoxContainer">
            <input type="text" name="dependent_contact" id="dependent_contact" class="textBox datePicker" value="<?php echo $dependent_contact ?>">
        </td>
    </tr>
    <tr class="formAlternateRow">
      <td class="formLabelContainer"><input type="hidden" name="employeeID" id="employeeID" value="<?php echo $arrEmployee['emp_id']; ?>"></td>
      <td class="formTextBoxContainer">
      	<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Save">&nbsp;
        <input type="button" class="smallButton" id="deletButton" value="Back" onclick="history.go(-1)">
      </td>
    </tr>
  </table>
  </div>
</form>
<br  />
<?php } ?>

<script>
	$('#dependentRelationship').val('<?php echo $dependentRelationship; ?>');
</script>

<table border="0" cellspacing="0" cellpadding="0" class="listTableMain dottedBorder">
	<tr class="listHeader">
    	<td class="listHeaderCol">Name</td>
    	<td class="listHeaderCol">Relationship</td>
        <td class="listHeaderCol">Contact No</td>
        <?php if($canWrite == YES) { ?>
    	<td class="listHeaderColLast">Action</td>
		<?php } ?>
    </tr>
    <?php
    for($ind = 0; $ind < count($arrRecords); $ind++) {
	?>
    <tr class="listContent">
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['dependent_name']; ?></td>
    	<td class="listContentCol"><?php echo $arrRecords[$ind]['dependent_relationship']; ?></td>
      <td class="listContentCol"><?php if($arrRecords[$ind]['dependent_contact'] != null && $arrRecords[$ind]['dependent_contact'] != '0') echo date($arrRecords[$ind]['dependent_contact']); else echo "-"; ?></td>
        <?php if(($canWrite == YES) || ($canDelete == YES)) { ?>
          <td class="listContentColLast">
            <div class="empColButtonContainer">
              <?php if($canWrite == YES) { ?>
                  <input type="button" class="smallButton" value="View/Edit" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction . '/' . $arrEmployee['emp_id'] . '/' . $arrRecords[$ind]['dependent_id']; ?>';" />
                    <?php } if($canDelete == YES) { ?>
                    <input type="button" class="smallButton" value="Delete" onclick="deleteRecord('/<?php echo $this->currentController . '/' . $this->currentAction; ?>/<?php echo $arrEmployee['emp_id']; ?>', '<?php echo $arrRecords[$ind]['dependent_id']; ?>');" />
                    <?php } ?>
              </div>
          </td>
        <?php } ?>
    </tr>
    <?php
	}
	if(!$ind) {
	?>
	<tr class="listContentAlternate">
		<td colspan="5" align="center" class="listContentCol">No Record Found</td>
	</tr>
    <?php
	}
	?>
</table>
<?php if($canWrite == NO) { ?>
<script>$("#frmDependents :input").attr("disabled", true);</script>
<?php } ?>