<div class="listPageMain">
    <div class="notificationWideBox">
    <div class="rightPadding"><input class="smallButton" name="btnBack" id="btnBack" type="button" value="Back" onclick="history.go(-1)"></div>
	<div class="dashboardTitle">Welcome to SBT-Polling System</div>    
    
    <div class="pollingHeaderImage" style="background:url('<?php echo $imagePath.'/modules/forum/poll_headers/'.$arrRecordPoll['poll_header'].'.jpg'; ?>');">
    	<span class="" style="width:260px;height:230px;float:right;padding-top:15px;padding-right:10px;">
			<div class="articleHeading paddingAll center bold"><?php echo $arrRecordPoll['poll_topic']; ?></div>
			<br>
            <div class="articleText center">Initiated By: <br><?php echo $arrRecordPoll['emp_full_name']; ?></div>
            <br>
			<div class="articleText center">Started On:	<?php echo readableDate($arrRecordPoll['poll_start_date'], 'M j, Y'); ?></div>
			<div class="articleText center">Last Date:	<?php echo readableDate($arrRecordPoll['poll_end_date'], 'M j, Y'); ?></div>
		</span>
    </div>
    <div class="clear" />
    <?php if(count($arrPollQuestionAnswers) > 0) { ?>
	<div class="topDescriptionContainer" style="width:1220px;">
        <form name="frmAnswerPoll" id="frmAnswerPoll" method="post">
        <table border="0" cellspacing="0" cellpadding="0" width="100%" class="">
            <tr>
                <td class="formHeaderRow" colspan="2">Poll Question(s) & Choice(s)</td>
            </tr>
            <?php for($i=0; $i < count($arrPollQuestionAnswers); $i++) { ?>
            <tr>
                <td class="listContentCol paddingAll" width="50%">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="articleText bold">Question<?php echo $i+1; ?></td>
                        </tr>
                        <tr>
                            <td><?php echo $arrPollQuestionAnswers[$i]['poll_question']; ?></td>
                        </tr>
                    </table>
                </td>
                <td class="listContentColLast paddingAll" width="50%">
                    <table border="0" cellspacing="0" cellpadding="0" class="paddingAll">
                        <tr>
                            <td colspan="2" class="articleText bold">Choices</td>
                        </tr>
                    
                        <?php for($j=0; $j < count($arrPollQuestionAnswers[$i]['answers']); $j++) 
                        { 
                           	$checked = "";
						    if($arrPollQuestionAnswers[$i]['poll_answer_type'] == 1)
                            { $inputType = "radio"; }
                            else if ($arrPollQuestionAnswers[$i]['poll_answer_type'] == 2)
                            { $inputType = "checkbox"; }					
							
							if($j==0) { $checked = "checked"; }
                        ?>
                            <tr>
                                <td><input <?php echo $checked; ?> type="<?php echo $inputType; ?>" name="results[<?php echo $arrPollQuestionAnswers[$i]['poll_question_id']; ?>][]" value="<?php echo $arrPollQuestionAnswers[$i]['answers'][$j]['poll_answer_id']; ?>"></td>
                                <td class="paddingLeftFive"><?php echo $arrPollQuestionAnswers[$i]['answers'][$j]['poll_answer']; ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                    
                </td>
            </tr>    
            <?php } ?>
            <tr>
                <td class="formLabelContainer"></td>
                <td class="formTextBoxContainer">
                	<?php if($hideSubmitButton == 0) { ?>
                		<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Submit">
                    <?php } ?>
					<input type="hidden" name="pollID" value="<?php echo $arrRecordPoll['poll_id']; ?>" />                    
                </td>
            </tr>
        </table>
        </form>
	</div>
    <?php } ?>
    
    <?php if(count($pollResult) > 0) { 
		for($ind = 0; $ind < count($pollResult); $ind++)
		{			
	?>
    	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script type="text/javascript">
          google.load("visualization", "1", {packages:["corechart"]});
          google.setOnLoadCallback(drawChart);
          function drawChart() {
            var data = google.visualization.arrayToDataTable([
              ['Choices', 'Votes', { role: 'style' }],
              <?php for($jnd = 0; $jnd < count($pollResult[$ind]['choices']); $jnd++) {
				?>
              ['<?php echo ucwords($pollResult[$ind]['choices'][$jnd]['poll_answer']); ?>',  <?php echo (int)$pollResult[$ind]['choices'][$jnd]['poll_count']; ?>,  ''],
              <?php
			  } ?>
            ]);
        
            var options = {
              title: '<?php echo "Question ".($ind+1).": ".$pollResult[$ind]['poll_question']; ?>',
              legend: { position: 'none' },
              isStacked: false,
              bar: {groupWidth: "30%"},
              vAxis: {title: 'Votes',  titleTextStyle: {color: 'red'}},
              hAxis: {title: 'Choices',  titleTextStyle: {color: 'red'}}
            };
				
			var chart = new google.visualization.ColumnChart(document.getElementById('chart_div<?php echo $ind; ?>'));           
            chart.draw(data, options);
          }
        </script>
        
        <div id="chart_div<?php echo $ind; ?>" style="float:left; width: 100%; height: 400px;"></div>
		
	<?php
		}
	} ?>
	</div>
</div>