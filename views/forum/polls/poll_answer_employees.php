<div class="centerElementsContainer" style="width:650px">
    <div class="recordCountContainer"><?php echo "Total Records Count: ".$totalRecordsCount; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <?php
    if($pageLinks) {
    ?>
        <div class="pagingContainer" align="center"><?php echo $pageLinks; ?></div>
    <?php 	}	?>
</div>
<div class="listPageMain" style="width:650px">
<table align="center" cellspacing="0" cellpadding="0" border="0" style="width:650px" class="listTableMain dottedBorder">
  	<tr>
        <td class="formHeaderRow" colspan="4">Employees of Choice: <?php echo $pollChoice; ?></td>
    </tr>
    <tr class="listHeader">
    	<td class="listHeaderCol" width="80px">Code</td>
        <td class="listHeaderCol" width="300px">Employee Name</td>
        <td class="listHeaderCol" width="190px">Department</td>
        <td class="listHeaderColLast" width="80px">IP</td>
    </tr>
    <?php for($i=0; $i < count($arrEmployees); $i++) { ?>
    <tr>
    	<td class="listContentCol"><?php echo $arrEmployees[$i]['emp_code']; ?></td>
        <td class="listContentCol"><?php echo $arrEmployees[$i]['emp_full_name']; ?></td>
        <td class="listContentCol"><?php echo $arrEmployees[$i]['job_category_name']; ?></td>
        <td class="listContentColLast" style="padding: 0 0 0 5px"><?php echo $arrEmployees[$i]['emp_ip_num']; ?></td>
    </tr>
    <?php } ?>
</table>
</div>