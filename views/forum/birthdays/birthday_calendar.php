<?php
$monthNames = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
$selMonth 	= isset($_POST['selMonth']) 	? $_POST['selMonth'] 	: date('m');
?>

<?php
$cDay	= date('d');
$cYear = date("Y");
 
$prev_year = $cYear;
$next_year = $cYear;
$prev_month = $selMonth - 1;
$next_month = $selMonth + 1;
 
if ($prev_month == 0 ) {
    $prev_month = 12;
    $prev_year = $cYear - 1;
}
if ($next_month == 13 ) {
    $next_month = 1;
    $next_year = $cYear + 1;
}
?>

<form name="frmBday" id="frmBday" method="post">
  <div class="searchBoxMain">
    <div class="searchHeader">Search Criteria</div>
    <div class="searchcontentmain">
    <div>
        <div class="labelContainer">Month:</div>
        <div class="textBoxContainer">
        	<select id="selMonth" name="selMonth" class="dropDown">
            	<option value="">Month</option>
            	<option value="01">January</option>
            	<option value="02">February</option>
            	<option value="03">March</option>
            	<option value="04">April</option>
            	<option value="05">May</option>
            	<option value="06">June</option>
            	<option value="07">July</option>
            	<option value="08">August</option>
            	<option value="09">September</option>
            	<option value="10">October</option>
            	<option value="11">November</option>
            	<option value="12">December</option>
          	</select>
        </div>
      </div>
      <div>
        <input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Show Birthdays">
      </div>
    </div>
  </div>
  <script>
	$('#selMonth').val('<?php echo $selMonth; ?>');
  </script>
</form>

<div class="listPageMain">
    <div class="calendarMain">
		<div class="dashboardTitle" colspan="7"><?php echo $monthNames[$selMonth - 1].' '.$cYear; ?></div>
		<table width="100%" border="0" cellpadding="2" cellspacing="2">
		<tr class="calendarDaysMain">
			<td class="calendarDayBox">Sunday</td>
			<td class="calendarDayBox">Monday</td>
			<td class="calendarDayBox">Tuesday</td>
			<td class="calendarDayBox">Wednesday</td>
			<td class="calendarDayBox">Thursday</td>
			<td class="calendarDayBox">Friday</td>
			<td class="calendarDayBox">Saturday</td>
		</tr>
		<?php 
		$timestamp = mktime(0,0,0,$selMonth,1,$cYear);
		$maxday = date("t",$timestamp);
		$thismonth = getdate ($timestamp);
		$startday = $thismonth['wday'];
		
		for ($i=0; $i<($maxday+$startday); $i++) {
			$spanBirthday = '';
			if(($i % 7) == 0 ) echo '<tr class="calendarDatesMain">';
			if($i < $startday) echo '<td class="calendarDateBox"></td>';
			elseif(($i - $startday + 1) == $cDay && $selMonth == date('m'))
			{
				$spanDateText = '<span class="calendarDateText">'. ($i - $startday + 1) .'</span>';
					if((int)$birthdays[$i - $startday + 1] > 0)	{							
						$spanBirthday = '<br><br><span><img src="'. $this->imagePath .'/birthday_notification.png" alt="Birthday Notification" /></span>&nbsp;<span class="articleText anchor" onclick="showPopup(\''.$this->baseURL . '/' . $this->currentController . '/birthdays_detail/' . $selMonth . '/' . ($i - $startday + 1) .'\', 700, 400)">'.(int)$birthdays[$i - $startday + 1].' Birthday(s)</span>';
					}
					echo '<td class="calendarCurrentDateBox">'.$spanDateText.$spanBirthday.'</td>';
			} else 
			{
					$spanDateText = '<span class="calendarDateText">'. ($i - $startday + 1) .'</span>';
					if((int)$birthdays[$i - $startday + 1] > 0)	{								
						$spanBirthday = '<br><br><span><img src="'. $this->imagePath .'/birthday_notification.png" alt="Birthday Notification" /></span>&nbsp;<span class="articleText anchor" onclick="showPopup(\''.$this->baseURL . '/' . $this->currentController . '/birthdays_detail/' . $selMonth . '/' . ($i - $startday + 1) .'\', 700, 400)">'.(int)$birthdays[$i - $startday + 1].' Birthday(s)</span>';
					}
					echo '<td class="calendarDateBox">'.$spanDateText.$spanBirthday.'</td>';
			}
			if(($i % 7) == 6 ) echo '</tr>';
			
		}
		?>
		</table>
    </div>
</div>