<?php if($canWrite == YES) { ?>
	<form name="frmIssues" id="frmIssues" method="post" enctype="multipart/form-data">
	  <div class="listPageMain">
		<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
		<tr>
			<td class="formHeaderRow" colspan="3">Submit Request Attadence</td>
		</tr>
		<tr>
		  <td class="formLabelContainer">Employees:<span class="mandatoryStar"> *</span></td>
		  <td class="formTextBoxContainer">
			<select id="emp_id" name="emp_id" class="dropDown" >
				<option value="">Select Employee</option>
				<?php
                    if (count($arrEmployees)) {
                        foreach ($arrEmployees as $key => $arrEmp) { ?>
                        <optgroup label="<?php echo $key; ?>">
                            <?php for ($i = 0; $i < count($arrEmp); $i++) { ?>
                                <option value="<?php echo $arrEmp[$i]['emp_id']; ?>"><?php echo $arrEmp[$i]['emp_code'].' --- '.$arrEmp[$i]['emp_full_name']; ?></option>
                            <?php } ?>
                        </optgroup>
                        <?php
                    	}
                    }
                ?>
			</select>
		  </td>
        </tr>


		<tr class="formAlternateRow">
		  <td class="formLabelContainer">Data :<span class="mandatoryStar"> *</span></td>
		  <td class="formTextBoxContainer">
            <input type="date" name="att_date" id="att_date" max="<?= date('Y-m-d'); ?>">
		  </td>
		</tr>
		<tr>
			<td class="formLabelContainer">Check In :<span class="mandatoryStar"> *</span></td>
			<td class="formTextBoxContainer" colspan="2">
				<input type="time" name="att_checkIN" id="att_checkIN">
			</td>
		</tr>
		<tr class="formAlternateRow">
		  <td class="formLabelContainer">Check Out :<span class="mandatoryStar"> *</span></td>
		  <td class="formTextBoxContainer" colspan="2">
             <input type="time" name="att_checkOUT" id="att_checkOUT">
		  </td>
		</tr>
		<tr class="formAlternateRow">
		  <td class="formLabelContainer">Reason :</td>
		  <td class="formTextBoxContainer" colspan="2">
             <textarea type="time" name="att_Reason" id="att_Reason"></textarea>
		  </td>
		</tr>
		<tr>
		  <td class="formLabelContainer"></td>
		  <td class="formTextBoxContainer">
			<input type="submit" class="smallButton" name="btnSave" id="btnSave" value="Submit">
		  </td>
		</tr>
	  </table>
	  </div>
	</form>
	<br  />
	<?php } ?>


<?php if($canWrite == NO) { ?>
<script>$("#frmIssues :input").attr("disabled", true);</script>
<?php } ?>