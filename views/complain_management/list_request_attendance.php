
<!-- <form name="frmLoanStatus" id="frmLoanStatus" method="post" action="<?php echo $frmActionURL; ?>">
  <div class="searchBoxMain">
    <div class="searchHeader">Search Criteria</div>
    <div class="searchcontentmain">
      <div class="searchCol">
        <div class="labelContainer">Leaves Of:</div>
        <div class="textBoxContainer">
        	<select id="select-state" name="loanMonth" class="dropDown select-state" style="width:85px">
            	<option value="">Month</option>
            	<option value="01">Jan</option>
            	<option value="02">Feb</option>
            	<option value="03">Mar</option>
            	<option value="04">Apr</option>
            	<option value="05">May</option>
            	<option value="06">Jun</option>
            	<option value="07">Jul</option>
            	<option value="08">Aug</option>
            	<option value="09">Sep</option>
            	<option value="10">Oct</option>
            	<option value="11">Nov</option>
            	<option value="12">Dec</option>
          	</select>&nbsp;&nbsp;&nbsp;&nbsp;
        	<select id="select-state" name="loanYear" class="dropDown select-state" style="width:85px; margin-left:5px">
            	<option value="">Year</option>
            	<?php for($ind = $this->HRMYearStarted; $ind <= date('Y'); $ind++) { ?>
            	<option value="<?php echo $ind; ?>"><?php echo $ind; ?></option>
				<?php } ?>
          	</select>
        </div>
      </div>
            <?php
			  if (count($companies)) {
				  for($ind = 0; $ind < count($companies); $ind++) {
            if($companies[$ind]['company_id'] != $employee_branch[0]['company_id'])
            {
			  ?>
				  <option value="<?php echo $companies[$ind]['company_id']; ?>"
          <?php
            if(isset($emploanRequest[0]['company_to_id']))
            {
              echo $emploanRequest[0]['company_to_id'] == $companies[$ind]['company_id'] ? 'selected' : null;
            }
          ?>
          ><?php echo $companies[$ind]['company_name']; ?></option>
			  <?php
            }
				  }
			  }
			  ?>
        </select>
        </div>
      </div>
	  <div class="searchCol">
        <div class="labelContainer">Branch To:</div>
        <div class="textBoxContainer">
      	<select name="branch_to" id="select-state" class="dropDown select-state" style="width:175px">
            <option value="">All</option>
            <?php
			  if (count($companies)) {
				  for($ind = 0; $ind < count($companies); $ind++) {
            if($companies[$ind]['company_id'] != $employee_branch[0]['company_id'])
            {
			  ?>
				  <option value="<?php echo $companies[$ind]['company_id']; ?>"
          <?php
            if(isset($emploanRequest[0]['company_to_id']))
            {
              echo $emploanRequest[0]['company_to_id'] == $companies[$ind]['company_id'] ? 'selected' : null;
            }
          ?>
          ><?php echo $companies[$ind]['company_name']; ?></option>
			  <?php
            }
				  }
			  }
			  ?>
	  <div class="searchCol">
        <div class="labelContainer">Select Employee:</div>
        <div class="textBoxContainer">
      	<select name="emp_id" class="dropDown select-state" id="select-state"  style="width:175px">
            <option value="">All</option>
            <?php
				if (count($all_employees)) {
					for($ind = 0; $ind < count($all_employees); $ind++) {?>
					<option value="<?php echo $all_employees[$ind]['emp_id']; ?>" 
					<?php
						if(isset($emploanRequest[0]['emp_id']))
						{
							echo $emploanRequest[0]['emp_id'] == $all_employees[$ind]['emp_id'] ? 'selected' : null;
						}
					?>
					><?php echo $all_employees[$ind]['emp_code'].' --- '.$all_employees[$ind]['emp_full_name']; ?></option>
					<?php
					}
			  	}
			  ?>
        </select>
        </div>
      </div>
	  <div class="searchCol">
        <div class="labelContainer">loan Status:</div>
        <div class="textBoxContainer">
      	<select name="loanStatus" id="select-state" class="dropDown select-state" style="width:175px">
            <option value="">All</option>
            <option value="-1">Pending Approval</option>
            <option value="1">Approved</option>
            <option value="2">Rejected</option>
        </select>
        </div>
      </div>            
      <div class="buttonContainer">
      	<input type="hidden" name="sort_field" id="sort_field" value="<?php echo $txtSortField; ?>" />
      	<input type="hidden" name="sort_order" id="sort_order" value="<?php echo $txtSortOrder; ?>" />
        <input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="Search">
        <input class="searchButton" name="btnBack" id="btnBack" type="button" value="Back" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction; ?>';">
      </div>
    </div>
  </div>
</form> -->
	
<div class="listContentMain">
	<table border="0" cellspacing="0" cellpadding="0" class="listTableMain">
		<tr>
            <td class="formHeaderRow" colspan="5">List Attendance Request</td>
        </tr>
		<tr class="listHeader">
			<td class="listHeaderCol">ID</td>
			<td class="listHeaderCol">Employee Name</td>
			<td class="listHeaderCol" style="max-width:140px;min-width:140px">Date</td>
			<td class="listHeaderCol" style="max-width:140px;min-width:140px">Check In</td>
			<td class="listHeaderCol" style="max-width:140px;min-width:140px">Check Out</td>
			<td class="listHeaderCol" style="max-width:140px;min-width:140px">Reason</td>
			<td class="listHeaderCol" style="max-width:150px;min-width:150px">Processed by</td>
			<td class="listHeaderCol" style="max-width:150px;min-width:150px">Processed Date</td>
			<td class="listHeaderColLast" align="center">Action/Status</td>
		</tr>
		<?php
			for($ind = 0; $ind < count($arrRecords); $ind++) {

				if($arrRecords[$ind]['status'] == 0) {
					$cssBtn = 'smallButtonUpdate';
					$trCSSClass = 'listContentAlternate" style="background-color:#F9F084"';
					$strStatus = 'Pending';
				} elseif($arrRecords[$ind]['status'] == 1) {
					$trCSSClass = 'listContentAlternate" style="background-color:#D9FFA0"';
					$strStatus = 'Approved';
				} elseif($arrRecords[$ind]['status'] == 2) {
					$trCSSClass = 'listContentAlternate" style="background-color:#F38374"';
					$strStatus = 'Rejected';
				}  else {
					$trCSSClass = 'listContentAlternate';
					$strStatus = '';
				}

				?>
				<tr class="<?php echo $trCSSClass; ?>" id="tr<?php echo $ind; ?>">
					<td class="listContentCol"><?php echo $arrRecords[$ind]['id']; ?></td>
					<td class="listContentCol"><?php echo $arrRecords[$ind]['emp_full_name']; ?></td>
					<td class="listContentCol"><?php echo $arrRecords[$ind]['date']; ?></td>
					<td class="listContentCol"><?php echo $arrRecords[$ind]['checkIN']; ?></td>
					<td class="listContentCol"><?php echo $arrRecords[$ind]['checkOUT']; ?></td>
					<td class="listContentCol"><?php echo $arrRecords[$ind]['reason']; ?></td>
					<td class="listContentCol"><?php echo $arrRecords[$ind]['processed_by']; ?></td>
					<td class="listContentCol"><?php echo $arrRecords[$ind]['processed_date']; ?></td>
					<td class="listContentColLast" align="center">
					<?php
						if($arrRecords[$ind]['status'] == 0 && $this->userRoleID == 2 && $this->userEmpNum != $arrRecords[$ind]['processed_by']) {	 
						?>

						&nbsp;&nbsp;
						<img title="Approve" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/leave_approve.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/list_request_attendance/' . $arrRecords[$ind]['id'].'/1/'. $arrRecords[$ind]['emp_id'].'/'. $arrRecords[$ind]['date'].'/'. $arrRecords[$ind]['checkIN'].'/'. $arrRecords[$ind]['checkOUT']; ?>';">
						&nbsp;&nbsp;
						<img title="Delete" style="margin:-7px 0;cursor:pointer" width="20" src="<?php echo $this->imagePath . '/delete.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/list_request_attendance/' . $arrRecords[$ind]['id'].'/2/'. $arrRecords[$ind]['emp_id'].'/'. $arrRecords[$ind]['date'].'/'. $arrRecords[$ind]['checkIN'].'/'. $arrRecords[$ind]['checkOUT']; ?>';">
						<?php } 
						// elseif($arrRecords[$ind]['status'] == 0 && $this->userRoleID == 26 && $arrRecords[$ind]['processed_by'] == $this->userEmpNum) {
						else{
							echo $strStatus;
						}
						?>
						<br />
						&nbsp;&nbsp;
						<!-- <img title="Edit" style="margin:-7px 0;cursor:pointer" width="30" src="<?php echo $this->imagePath . '/view.png';?>" onclick="window.location.href = '<?php echo $this->baseURL . '/' . $this->currentController . '/list_request_attendance/' . $arrRecords[$ind]['id'].'/3' ?>';"> -->


						
					</td>
				</tr>
				<?php
			}
		if(!$ind) {
		?>
		<tr class="listContentAlternate">
			<td colspan="11" align="center" class="listContentCol">No Record Found</td>
		</tr>
		<?php
		}
		?>
	</table>
</div>
<div style="clear:both">&nbsp;<div>