<?php
class Model_Payroll_Management extends Model_Master {
	
	public $strHierarchy;
	private $tblNoDeletion	= array(
									TABLE_EMPLOYEE_PAYROLL
									);
	
	function __construct() {
		 parent::__construct();	
	}
	
	function getPayrolls($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true) {
		// return $arrWhere;
		$this->db->distinct();
		$this->db->select(' ep.*, e.*, c.company_currency_id ');				
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'ep.emp_id = e.emp_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
		$this->db->join(TABLE_COMPANIES. ' c ', 'c.company_id = ep.payroll_company_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('ep.created_date', 'DESC');
		
		if(!isAdmin($this->userRoleID))
		{
			$this->db->where('e.emp_status', STATUS_ACTIVE);
		}
		
		$objResult = $this->db->get(TABLE_EMPLOYEE_PAYROLL . ' ep ');

		// if(count($arrWhere)) {
			// print_r('SELECT ep.*, e.*, c.company_currency_id FROM hrm_employee_payroll ep INNER JOIN hrm_employee e ON ep.emp_id = e.emp_id INNER JOIN hrm_employee_supervisors es ON es.emp_id = e.emp_id INNER JOIN hrm_companies c ON c.company_id = ep.payroll_company_id WHERE ' .$arrWhere['empID'] .' ' .$arrWhere['selRegion'] .' ' .$arrWhere['empCompany'] .' ' .$arrWhere['empSupervisor'] .' ' .$arrWhere['selMonth'] .' ' .$arrWhere['selYear']);exit;
		// 	$objResult = $this->db->query('SELECT ep.*, e.*, c.company_currency_id FROM hrm_employee_payroll ep INNER JOIN hrm_employee e ON ep.emp_id = e.emp_id INNER JOIN hrm_employee_supervisors es ON e.emp_id = es.emp_id INNER JOIN hrm_companies c ON c.company_id = ep.payroll_company_id WHERE ' .$arrWhere['empID'] .' ' .$arrWhere['selRegion'] .' ' .$arrWhere['empCompany'] .' ' .$arrWhere['empSupervisor'] .' ' .$arrWhere['selMonth'] .' ' .$arrWhere['selYear'] );
		// }else{
		// 	$objResult = $this->db->query('SELECT ep.*, e.*, c.company_currency_id FROM hrm_employee_payroll ep INNER JOIN hrm_employee e ON ep.emp_id = e.emp_id INNER JOIN hrm_employee_supervisors es ON es.emp_id = e.emp_id INNER JOIN hrm_companies c ON c.company_id = ep.payroll_company_id');
		// }
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		return $arrResult;
	}
	
	function getTotalPayrolls($arrWhere = array()) {
		
		$this->db->distinct();
		$this->db->select(' ep.payroll_id ');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'ep.emp_id = e.emp_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
		$this->db->join(TABLE_COMPANIES. ' c ', 'c.company_id = ep.payroll_company_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_EMPLOYEE_PAYROLL . ' ep ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		// if(count($arrWhere)) {
			// print_r("SELECT ep.*, e.*, c.company_currency_id FROM hrm_employee_payroll ep INNER JOIN hrm_employee e ON ep.emp_id = e.emp_id INNER JOIN hrm_employee_supervisors es ON es.emp_id = e.emp_id INNER JOIN hrm_companies c ON c.company_id = ep.payroll_company_id WHERE " .$arrWhere['empID'] ." " .$arrWhere['selRegion'] ." " .$arrWhere['empCompany'] ." " .$arrWhere['empSupervisor'] ." " .$arrWhere['selMonth'] ." " .$arrWhere['selYear']);exit;
		// 	$objResult = $this->db->query("SELECT ep.*, e.*, c.company_currency_id FROM hrm_employee_payroll ep INNER JOIN hrm_employee e ON ep.emp_id = e.emp_id INNER JOIN hrm_employee_supervisors es ON e.emp_id = es.emp_id INNER JOIN hrm_companies c ON c.company_id = ep.payroll_company_id WHERE " .$arrWhere['empID'] ." " .$arrWhere['selRegion'] ." " .$arrWhere['empCompany'] ." " .$arrWhere['empSupervisor'] ." " .$arrWhere['selMonth'] ." " .$arrWhere['selYear'] );
		// }else{
		// 	$objResult = $this->db->query('SELECT ep.*, e.*, c.company_currency_id FROM hrm_employee_payroll ep INNER JOIN hrm_employee e ON ep.emp_id = e.emp_id INNER JOIN hrm_employee_supervisors es ON es.emp_id = e.emp_id INNER JOIN hrm_companies c ON c.company_id = ep.payroll_company_id');
		// }
		// $arrResult = $objResult->result_array();
		// $objResult->free_result();

		// $arrResult = count($arrResult);
		return $arrResult;
	}
	
	function getEmployeesPayroll($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true) {
		
		$this->db->distinct();
		$this->db->select(' e.*, u.user_role_id, ur.user_role_name, u.employee_id as user_exists, l.location_name, jc.job_category_name, c.company_currency_id ');
		
		$this->db->join(TABLE_USER . ' u ', 'e.emp_id = u.employee_id', 'left');
		$this->db->join(TABLE_USER_ROLE . ' ur ', 'ur.user_role_id = u.user_role_id', 'left');
		$this->db->join(TABLE_LOCATION . ' l ', 'l.location_id = e.emp_location_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
		$this->db->join(TABLE_JOB_CATEGORY . ' jc ', 'jc.job_category_id = e.emp_job_category_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_PAYROLL . ' p ', 'p.emp_id = e.emp_id', 'left');
		$this->db->join(TABLE_COMPANIES. ' c ', 'c.company_id = p.payroll_company_id', 'left');
				
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		
		
		if((int)$arrWhere['p.payroll_company_id']) {
			$this->db->where(" (p.payroll_company_id = '"  . (int)$arrWhere['p.payroll_company_id'] . "' OR e.emp_company_id = '" . (int)$arrWhere['p.payroll_company_id'] . "') ", null, false);
			unset($arrWhere['p.payroll_company_id']);
		}
		
		if($arrWhere['e.emp_name'] != '') {
			$this->db->where('(e.emp_full_name like \'%' . $arrWhere['e.emp_name'] . '%\')'); 
			unset($arrWhere['e.emp_name']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if($arrWhere['edu.edu_level_id'] || $arrWhere['edu.edu_major_id']) {			
			$this->db->join(TABLE_EMPLOYEE_EDUCATION . ' edu ', 'edu.emp_id = e.emp_id', 'left');
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		if($doSort) {
			if(!isset($_POST['sort_field']) || $_POST['sort_field'] == '') {
				$this->db->order_by('e.emp_full_name', 'ASC');
			} else if($this->currentController == 'employee_management') {
				$sortColumn = $_POST['sort_field'];
				$sortOrder = $_POST['sort_order'];
				
				if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
					$this->db->order_by($sortColumn, $sortOrder);
				}
				$this->db->order_by('e.emp_full_name', 'ASC');
			} else {
				$this->db->order_by('e.emp_full_name', 'ASC');
			}
		}
		
		/*if(!isAdmin($this->userRoleID))
		{
			$this->db->where('e.emp_status', STATUS_ACTIVE);
		}*/
		
		$objResult = $this->db->get(TABLE_EMPLOYEE . ' e ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}

	function get_department_emp(){
		// print_r("SELECT jc.job_category_name, count(DISTINCT att.employee_id) as emp, e.emp_job_category_id, month(att.att_date) as month, year(att.att_date) as years FROM hrmbackup.hrm_attendancce as att
		// inner join hrmbackup.hrm_employee as e on att.employee_id = e.emp_id
		// inner join hrmbackup.hrm_job_category as jc on e.emp_job_category_id = jc.job_category_id
		// where att.ot_status = 1 AND e.emp_ot_eligibility = 1 group by jc.job_category_id");exit;
		$objResult = $this->db->query("SELECT jc.job_category_name, count(DISTINCT att.employee_id) as emp, e.emp_job_category_id, month(att.att_date) as month, year(att.att_date) as years FROM hrm_attendancce as att
		inner join hrm_employee as e on att.employee_id = e.emp_id
		inner join hrm_job_category as jc on e.emp_job_category_id = jc.job_category_id
		group by jc.job_category_id");
		$arrResult = $objResult->result_array();
		$objResult->free_result();

		return $arrResult;
	}

	function fetch_overtime($dep_id, $month, $year){
		$query 			= "SELECT e.emp_id, e.emp_full_name, jc.job_category_id, jc.job_category_name, att_date, att.hours-9 as hours, po.ot_statuss FROM hrm_attendancce as att
		inner join hrm_employee as e on att.employee_id = e.emp_id
		left join hrm_payroll_overtime as po on att.employee_id = po.emp_id AND day(att.att_date) = po.day AND month(att.att_date) = po.month AND year(att.att_date) = po.year
		inner join hrm_job_category as jc on e.emp_job_category_id = jc.job_category_id
		where att.ot_status = 1 AND e.emp_ot_eligibility = 1 AND e.emp_job_category_id = '".$dep_id."' AND month(att.att_date) = '".$month."' AND year(att.att_date) = ".$year;
		// print_r($query);exit;
		$objResult 		= $this->db->query($query);
		$arrResult 		= $objResult->result_array();
		$objResult->free_result();
		return $arrResult;
	}

	function fetch_overtime_by_emp($dep_id, $month, $year, $emp){
		$query 			= "SELECT e.total_salary, att.att_date, att.hours-9 as hours FROM hrm_attendancce as att
		inner join hrm_employee as e on att.employee_id = e.emp_id
		inner join hrm_job_category as jc on e.emp_job_category_id = jc.job_category_id
		where att.ot_status = 1 AND e.emp_ot_eligibility = 1 AND e.emp_id = '".$emp."' AND e.emp_job_category_id = '".$dep_id."' AND month(att.att_date) = '".$month."' AND year(att.att_date) = '".$year."' group by e.emp_id";
		// print_r($query);exit;
		$objResult 		= $this->db->query($query);
		$arrResult 		= $objResult->result_array();
		$objResult->free_result();
		return $arrResult;
	}
	
	// function get_emp_depart($emp){
	// 	$query = "SELECT e.emp_job_category_id, jc.shift_in, jc.Shift_out, jc.grace_time, jc.dep_weekly_off_1, jc.dep_weekly_off_2 FROM hrm_employee e left join hrm_job_category jc on e.emp_job_category_id = jc.job_category_id where emp_id =".$emp;
	// 	$objResult = $this->db->query($query);
	// 	$arrResult = $objResult->result_array();
	// 	$objResult->free_result();
	// 	return $arrResult;
	// }
}
?>