<?php
class Model_Recruitment_Management extends Model_Master {
	
	private $tblNoDeletion	= array(
									//TABLE_CANDIDATE,
									//TABLE_USER,
									//TABLE_VACANCY
									);
	
	function __construct() {
		 parent::__construct();	
	}
	
	function deleteValue($tblName, $arrDeleteValues = array())
	{
		if(count($arrDeleteValues)) {
			
			$this->db->where($arrDeleteValues);
			if(!in_array($this->tblNoDeletion, $tblName)) {
				$this->db->delete($tblName);
			} else {
				$arrValues = array(
									'user_status' => STATUS_DELETED,
									'deleted_by' => $this->userEmpNum,
									'deleted_date' => date(DATE_TIME_FORMAT)
									);
				$this->db->update($tblName, $arrValues, $arrDeleteValues);
			}
			return ($this->db->affected_rows() > 0);
			
		}
	}
	
	function getCandidates($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $notApplied = false) {
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if($arrWhere['candidate_id']) {
			$arrWhere['c.candidate_id'] = $arrWhere['candidate_id'];
			unset($arrWhere['candidate_id']);
		}
		
		$this->db->select(' c.*, u.user_status ');
		
		$this->db->distinct();
		$this->db->join(TABLE_USER . ' u ', 'c.candidate_id = u.candidate_id', 'left');
		
		if($notApplied) {
			$this->db->where('c.candidate_id not in ', ' (select distinct candidate_id from ' . TABLE_CANDIDATE_VACANCY . ') ', false);
		} else {
			$this->db->join(TABLE_CANDIDATE_VACANCY . ' cv ', 'c.candidate_id = cv.candidate_id', 'left');
			$this->db->join(TABLE_VACANCY . ' jv ', 'cv.vacancy_id = jv.vacancy_id', 'left');
			$this->db->join(TABLE_JOB_TITLE . ' jt ', 'jv.job_title_code = jt.job_title_id', 'left');
			
			if(isset($arrWhere['ji.interview_date']) && isAdmin($this->userRoleID)) {
				$this->db->join(TABLE_CANDIDATE_INTERVIEW . ' ji ', 'ji.candidate_id = c.candidate_id ', 'left');
			} else {
				$this->db->join(TABLE_CANDIDATE_INTERVIEW . ' ji ', 'ji.candidate_id = c.candidate_id and ji.candidate_vacancy_id = cv.candidate_vacancy_id', 'left');
			}
			
			if(!isAdmin($this->userRoleID)) {
				$this->db->join(TABLE_CANDIDATE_INTERVIEW_INTERVIEWER . ' jii ', 'jii.interview_id = ji.interview_id', 'left');
				$this->db->where('jii.interviewer_id', $this->userEmpNum);
			} else if(isset($arrWhere['jii.interviewer_id'])) {
				$this->db->join(TABLE_CANDIDATE_INTERVIEW_INTERVIEWER . ' jii ', 'jii.interview_id = ji.interview_id ', 'left');
			}		
		}
		
		if($arrWhere['e.edu_level_id'] || $arrWhere['e.edu_major_id']) {			
			$this->db->join(TABLE_CANDIDATE_EDUCATION . ' e ', 'e.edu_candidate_id = c.candidate_id', 'left');
		}
		
		if($arrWhere['c.contact_number'] != '') {
			$this->db->where('(c.contact_number like \'%' . $arrWhere['c.contact_number'] . '%\' or c.home_contact_number like \'%' . $arrWhere['c.contact_number'] . '%\')'); 
			unset($arrWhere['c.contact_number']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);
		}
		
		if(!isset($_POST['sort_field'])) {
			$this->db->order_by('c.modified_date', 'DESC');
		} else {
			$sortColumn = $_POST['sort_field'];
			$sortOrder = $_POST['sort_order'];
			
			if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
				$this->db->order_by('c.' . $sortColumn, $sortOrder);
			}
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_CANDIDATE . ' c ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTotalCandidates($arrWhere = array(), $notApplied = false) {
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if($arrWhere['candidate_id']) {
			$arrWhere['c.candidate_id'] = $arrWhere['candidate_id'];
			unset($arrWhere['candidate_id']);
		}
		
		$this->db->select(' count(DISTINCT c.candidate_id) as total_records ');
		
		$this->db->join(TABLE_USER . ' u ', 'c.candidate_id = u.candidate_id', 'left');
		
		if($notApplied) {
			$this->db->where('c.candidate_id not in ', ' (select distinct candidate_id from ' . TABLE_CANDIDATE_VACANCY . ') ', false);
		} else {
			$this->db->join(TABLE_CANDIDATE_VACANCY . ' cv ', 'c.candidate_id = cv.candidate_id', 'left');
			$this->db->join(TABLE_VACANCY . ' jv ', 'cv.vacancy_id = jv.vacancy_id', 'left');
			$this->db->join(TABLE_JOB_TITLE . ' jt ', 'jv.job_title_code = jt.job_title_id', 'left');
			
			if(isset($arrWhere['ji.interview_date']) && isAdmin($this->userRoleID)) {
				$this->db->join(TABLE_CANDIDATE_INTERVIEW . ' ji ', 'ji.candidate_id = c.candidate_id ', 'left');
			} else {
				$this->db->join(TABLE_CANDIDATE_INTERVIEW . ' ji ', 'ji.candidate_id = c.candidate_id and ji.candidate_vacancy_id = cv.candidate_vacancy_id', 'left');
			}
			
			if(!isAdmin($this->userRoleID)) {
				$this->db->join(TABLE_CANDIDATE_INTERVIEW_INTERVIEWER . ' jii ', 'jii.interview_id = ji.interview_id', 'left');
				$this->db->where('jii.interviewer_id', $this->userEmpNum);
			} else if(isset($arrWhere['jii.interviewer_id'])) {
				$this->db->join(TABLE_CANDIDATE_INTERVIEW_INTERVIEWER . ' jii ', 'jii.interview_id = ji.interview_id ', 'left');
			}		
		}
		
		if($arrWhere['e.edu_level_id'] || $arrWhere['e.edu_major_id']) {			
			$this->db->join(TABLE_CANDIDATE_EDUCATION . ' e ', 'e.edu_candidate_id = c.candidate_id', 'left');
		}
		
		if($arrWhere['c.contact_number'] != '') {
			$this->db->where('(c.contact_number like \'%' . $arrWhere['c.contact_number'] . '%\' or c.home_contact_number like \'%' . $arrWhere['c.contact_number'] . '%\')'); 
			unset($arrWhere['c.contact_number']);
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);
		}
		
		if(!isset($_POST['sort_field'])) {
			$this->db->order_by('c.candidate_id', 'DESC');
		} else {
			$sortColumn = $_POST['sort_field'];
			$sortOrder = $_POST['sort_order'];
			
			if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
				$this->db->order_by('c.' . $sortColumn, $sortOrder);
			}
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_CANDIDATE . ' c ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult[0]['total_records'];
	}
	
	function getCandidateDetail($arrWhere = array(), $boolSystem = true) {
		
		$this->db->distinct();
		$this->db->select(' c.*, u.user_status ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->join(TABLE_USER . ' u ', 'c.candidate_id = u.candidate_id', 'left');
			
		if(!isAdmin($this->userRoleID) && $boolSystem) {
			$this->db->join(TABLE_CANDIDATE_INTERVIEW . ' ji ', 'ji.candidate_id = c.candidate_id', 'left');
			$this->db->join(TABLE_CANDIDATE_INTERVIEW_INTERVIEWER . ' jii ', 'jii.interview_id = ji.interview_id', 'left');
			$this->db->where('jii.interviewer_id', $this->userEmpNum);
		}			
			
		$objResult = $this->db->get(TABLE_CANDIDATE . ' c ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		if(count($arrResult) == 1) {
			$arrResult = $arrResult[0];
		}
		
		return $arrResult;
	}
	
	function notExistingCandidate($strField = 'candidate_id', $strValue = 0, $strID = 0)
	{	
		$this->db->select(' count(*) as count ');
		$this->db->where($strField, $strValue);
		
		if($strID) {
			$this->db->where('candidate_id not in ', '(' . $strID . ')', false);
		}
		
		$objResult = $this->db->get(TABLE_CANDIDATE);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		if($arrResult[0]['count'] >= 1) {
			return false;
		}
		return true;
	}
	
	function getCandidateHistory($arrWhere = array()) {
				
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->order_by('candidate_history_id', 'ASC');
		$objResult = $this->db->get(TABLE_CANDIDATE_HISTORY);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getCandidateInterviewDetails($candidateID = 0) {
		
		$this->db->select(' ch.candidate_history_id, i.candidate_vacancy_id, cv.vacancy_id, i.interview_name, i.interview_date, i.interview_time, e.emp_full_name ');
		
		$this->db->join(TABLE_CANDIDATE_INTERVIEW_INTERVIEWER . ' ii ', ' ii.interview_id = i.interview_id ', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', ' ii.interviewer_id = e.emp_id ', 'left');
		$this->db->join(TABLE_CANDIDATE_VACANCY . ' cv ', ' i.candidate_vacancy_id = cv.candidate_vacancy_id ', 'left');
		$this->db->join(TABLE_CANDIDATE_HISTORY . ' ch ', ' ch.interview_id = i.interview_id ', 'left');
		
		if(count($arrWhere)) {
			$this->db->where('cv.candidate_id', $candidateID);
		}
		
		$objResult = $this->db->get(TABLE_CANDIDATE_INTERVIEW . ' i ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getCandidateLastInterviewID($candidateID = 0, $vacancyID = 0) {
		
		$this->db->select(' ch.interview_id ');
				
		$this->db->where('ch.candidate_id', $candidateID);
		$this->db->where('ch.vacancy_id', $vacancyID);
		$this->db->where('ch.status_id', STATUS_SCHEDULE_INTERVIEW);
		
		$this->db->order_by('candidate_history_id', 'DESC');
		$this->db->limit(1);
		
		$objResult = $this->db->get(TABLE_CANDIDATE_HISTORY . ' ch ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		$arrResult = $arrResult[0];
		
		return (int)$arrResult['interview_id'];
	}
	
	function getInterviewers($arrWhere = array()) {
		
		$this->db->select(' interviewer_id ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$objResult = $this->db->get(TABLE_CANDIDATE_INTERVIEW_INTERVIEWER);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
		
	}
	
	function getResponses($arrWhere = array()) {
		
		$this->db->select(' performed_by ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$objResult = $this->db->get(TABLE_CANDIDATE_HISTORY);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
		
	}
	
	function getRequisitions($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);
		}
		
		if(!isset($_POST['sort_field'])) {
			$this->db->order_by('r.created_date', 'DESC');
		} else {
			$sortColumn = $_POST['sort_field'];
			$sortOrder = $_POST['sort_order'];
			
			if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
				$this->db->order_by('r.' . $sortColumn, $sortOrder);
			}
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_EMPLOYEE_REQUISITION . ' r ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTotalRequisitions($arrWhere = array()) {
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		$this->db->select(' count(*) as total_count ');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);
		}
		
		$objResult = $this->db->get(TABLE_EMPLOYEE_REQUISITION . ' r ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return (int)$arrResult[0]['total_count'];
	}
	
	function getAppliedCandidatesDateStat($numRows = 0, $arrWhere = array()) {	
		
		$this->db->select(' date(created_date) daydate,
(select count(*) from hrm_job_candidate where date(created_date) = daydate and arrival_source = \'System\') as manual,
(select count(*) from hrm_job_candidate where date(created_date) = daydate and arrival_source != \'System\') as online ');
			
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$numRows > 0) {
			$this->db->limit((int)$numRows);
		}
		
		$this->db->group_by('daydate');
		$this->db->order_by('daydate', 'DESC');
		$objResult = $this->db->get(TABLE_CANDIDATE);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getCandidatesSourceStat($arrWhere = array()) {	
		
		$this->db->select(' count(*) as total, arrival_source');
		$this->db->group_by('arrival_source');
		$this->db->order_by('total', 'DESC');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);	
			$this->db->limit(count(explode(',', ARRIVAL_SOURCES)) + 2);			
		} else {		
			$this->db->limit(10);		
		}
		
		$objResult = $this->db->get(TABLE_CANDIDATE);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getCandidatesSourceStatsWebMarketing($arrWhere = array()) {
		
		$this->db->select(' date(created_date) daydate, COUNT(arrival_source) AS arrival_source_counts ');
		$this->db->group_by('date(created_date)');				
				
		if(count($arrWhere)) {
			$this->db->where($arrWhere);	
		}		
		
		$objResult = $this->db->get(TABLE_CANDIDATE);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getCandidatesStatusStat($arrWhere = array()) {	
		
		$this->db->select(' count(*) as total, status_id');
		$this->db->group_by('status_id');
		$this->db->order_by('total', 'DESC');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
			
		$objResult = $this->db->get(TABLE_CANDIDATE_HISTORY);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getVacancyStat($arrWhere = array()) {	
		
		$this->db->select(' count(*) as total, vacancy_id');
		$this->db->group_by('vacancy_id');
		$this->db->order_by('vacancy_id', 'ASC');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);
		}
		
		$this->db->limit(10);
		
		$objResult = $this->db->get(TABLE_CANDIDATE_VACANCY);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getRecruitersStat($arrWhere = array()) {	
		
		$this->db->select(' count(*) as total, resume_downloaded_by');
		$this->db->group_by('resume_downloaded_by');
		$this->db->order_by('total', 'DESC');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);
		}
		
		$objResult = $this->db->get(TABLE_CANDIDATE);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getStatusStats($arrWhere = array()) {	
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		$this->db->select(' count(*) as total, performed_by');
		$this->db->group_by('performed_by');
		$this->db->order_by('total', 'DESC');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
			
		$objResult = $this->db->get(TABLE_CANDIDATE_HISTORY);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getCandidateCallsStats($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {	
		
		$this->db->select(' cc.*, e.emp_full_name, c.first_name, c.last_name ');
		$this->db->join(TABLE_EMPLOYEE . ' e ', ' cc.caller_emp_id = e.emp_id ', 'left');
		$this->db->join(TABLE_CANDIDATE . ' c ', ' cc.called_candidate_id = c.candidate_id ', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		$this->db->order_by('call_id', 'DESC');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
			
		$objResult = $this->db->get(TABLE_CANDIDATE_CALLS . ' cc ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getCandidateCallsStatsTotal($arrWhere = array()) {	
		
		$this->db->select(' count(*) as total_records ');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
			
		$objResult = $this->db->get(TABLE_CANDIDATE_CALLS . ' cc ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return (int)$arrResult[0]['total_records'];
	}
	
	function getDayUsers($arrWhere = array()) {
		
		$this->db->distinct();
		$this->db->select('performed_by');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$objResult = $this->db->get(TABLE_CANDIDATE_HISTORY . ' ch ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();		
		
		return $arrResult;
	}
	
	function getDayHistory($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $notApplied = false) {
		
		$this->db->select(' ch.*, c.first_name, c.last_name ');
		$this->db->join(TABLE_CANDIDATE . ' c ', ' ch.candidate_id = c.candidate_id ', 'left');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('candidate_history_id', 'ASC');
		$this->db->order_by('performed_by', 'ASC');
		$objResult = $this->db->get(TABLE_CANDIDATE_HISTORY . ' ch ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();		
		
		return $arrResult;
	}
	
	function getCandidateStatuses() {
		
		$this->db->order_by('status_sort_order', 'ASC');
		$objResult = $this->db->get(TABLE_CANDIDATE_STATUS);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getSubStatuses($statusID = 1) {
		$this->db->where('status_id in ', '(select sub_status_id from '.TABLE_CANDIDATE_STATUS_ORDER.' where status_id = '.(int)$statusID.')', false);
		$objResult = $this->db->get(TABLE_CANDIDATE_STATUS);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getCandidateVacancies($arrWhere = array()) {
		
		$this->db->select(' cv.*, v.vacancy_name ');
		$this->db->join(TABLE_VACANCY . ' v ', ' v.vacancy_id = cv.vacancy_id ', 'left');
		if(count($arrWhere)) {
			foreach($arrWhere as $colName => $colValue) {
				$arrWhere['cv.' . $colName] = $colValue;
				unset($arrWhere[$colName]);
			}
			$this->db->where($arrWhere);			
		}
			
		$objResult = $this->db->get(TABLE_CANDIDATE_VACANCY . ' cv ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getEducationalHistory($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->select(' e.*, l.edu_level_name, m.edu_major_name ');
		$this->db->join(TABLE_EDUCATION_LEVELS . ' l ', 'l.edu_level_id = e.edu_level_id', 'left');
		$this->db->join(TABLE_EDUCATION_MAJORS . ' m ', 'm.edu_major_id = e.edu_major_id', 'left');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('e.edu_level_id', 'ASC');
		$objResult = $this->db->get(TABLE_CANDIDATE_EDUCATION . ' e ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getWorkHistory($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('work_to', 'DESC');
		$objResult = $this->db->get(TABLE_CANDIDATE_WORK_EXPERIENCE);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getVacancies($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->distinct();
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->select(' v.*, e.emp_full_name, jt.job_title_name, l.location_name ');
			
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = v.hiring_manager_id', 'left');
		$this->db->join(TABLE_JOB_TITLE . ' jt ', 'jt.job_title_id = v.job_title_code', 'left');
		$this->db->join(TABLE_LOCATION . ' l ', 'l.location_id = v.vacancy_location', 'left');
		
		if(!isAdmin($this->userRoleID)) {
			$this->db->join(TABLE_CANDIDATE_VACANCY . ' cv ', 'v.vacancy_id = cv.vacancy_id', 'left');
			$this->db->join(TABLE_CANDIDATE_INTERVIEW . ' ji ', 'ji.candidate_vacancy_id = cv.candidate_vacancy_id', 'left');
			$this->db->join(TABLE_CANDIDATE_INTERVIEW_INTERVIEWER . ' jii ', 'jii.interview_id = ji.interview_id', 'left');
			$this->db->where('jii.interviewer_id', $this->userEmpNum);
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}		
		
		if(!isset($_POST['sort_field'])) {
			$this->db->order_by('v.vacancy_id', 'DESC');
		} else {
			$sortColumn = $_POST['sort_field'];
			$sortOrder = $_POST['sort_order'];
			
			if(strlen($sortColumn) > 2 && strlen($sortOrder) >= 3) {
				$this->db->order_by($sortColumn, $sortOrder);
			}
		}
		
		$this->db->order_by('created_date', 'DESC');
		$objResult = $this->db->get(TABLE_VACANCY . ' v');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getAllVacancies($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {	
		
		$this->db->select(' v.*, l.location_name, w.shift_name ');
		$this->db->join(TABLE_WORK_SHIFT . ' w ', 'w.shift_id = v.work_shift', 'left');
		$this->db->join(TABLE_LOCATION . ' l ', 'l.location_id = v.vacancy_location', 'left');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$this->db->order_by('v.vacancy_id', 'DESC');
		$objResult = $this->db->get(TABLE_VACANCY . ' v');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getReferredCandidates($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $showTotal = true) {
		
		if($showTotal) {
			$this->db->select(' e.emp_full_name, e.emp_code, e.emp_ip_num, count(c.reference) as total_references');
			$this->db->join(TABLE_EMPLOYEE . ' e ', ' e.emp_code = c.reference ', 'left');
			$this->db->group_by('e.emp_code');
			$this->db->order_by('total_references', 'DESC');
		} else {
			$this->db->select(' c.candidate_id, c.first_name, c.last_name ');
			$this->db->join(TABLE_USER . ' u ', ' u.candidate_id = c.candidate_id ', 'left');
			$this->db->join(TABLE_EMPLOYEE . ' e ', ' e.emp_id = u.employee_id ', 'left');
			$this->db->order_by('c.created_date', 'ASC');
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}
		
		$objResult = $this->db->get(TABLE_CANDIDATE . ' c ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getHiredConfirmedCandidates($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db->select('c.candidate_id, u.employee_id, e.emp_employment_status');
		$this->db->join(TABLE_USER . ' u ', ' u.candidate_id = c.candidate_id ', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', ' e.emp_id = u.employee_id ', 'left');
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);
		}
		
		$objResult = $this->db->get(TABLE_CANDIDATE . ' c ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
}
?>