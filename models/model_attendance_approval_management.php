<?php
class Model_Attendance_Approval_Management extends Model_Master {
	
	public $strHierarchy;
	
    private $tblNoDeletion	= array(
        TABLE_ATTENDANCE_APPROVAL
        );

	function __construct() {
		parent::__construct();
	}

	function InsertData($emp_id, $att_date, $att_checkIN, $att_checkOUT, $att_Reason, $processedBy, $processedDate){
		$query 			= "INSERT INTO hrmbackup.hrm_attendance_approval(`emp_id`,`date`,`checkIN`,`checkOUT`,`reason`,`status`,`processed_by`,`processed_date`) VALUES('".$emp_id."','".$att_date."','".$att_checkIN."','".$att_checkOUT."','".$att_Reason."','0','".$processedBy."','".$processedDate."')  ";
		$objResult 		= $this->db->query($query);
		if ($objResult == true) {
			return 'Updated';
		}else{
			return 'Some Error Occurred, Please Try Again.';
		}
		// $arrResult 		= $objResult->result_array();
		// $objResult->free_result();
		// return $arrResult[0]['leave_days'];
	}

	function GetData(){
		$query 			= "SELECT ta.*, e.emp_full_name FROM hrmbackup.hrm_attendance_approval AS ta LEFT JOIN hrmbackup.hrm_employee AS e ON e.emp_id = ta.emp_id ";
		// print_r($query);exit;
		$objResult 		= $this->db->query($query);
		$arrResult 		= $objResult->result_array();
		$objResult->free_result();
		return $arrResult;
	}
	
	function updateAttendanceStatus($id, $status, $emp_id, $date, $checkIN, $checkOUT, $hours, $ot_status){
		if($status == 1){
			// print_r($status);exit;
			$query 			= "UPDATE hrmbackup.hrm_attendance_approval SET status = '". $status ."' WHERE id = '".$id."' ";
			$objResult 		= $this->db->query($query);
			
			if ($objResult == true) {
				$check_query = "select * from hrmbackup.hrm_attendancce WHERE employee_id = '".$emp_id."' AND att_date = '".$date."' ";
				$check_objResult 	= $this->db->query($check_query);
				$check_arrResult 			= $check_objResult->result_array();
				$check_objResult->free_result();
				if($check_arrResult != true){
					$att_query = "INSERT INTO hrmbackup.hrm_attendancce (employee_id, att_date, att_in, att_out, hours, ot_status) VALUES ('".$emp_id."', '".$date."', '". $checkIN ."', '". $checkOUT ."', '".$hours."', '".$ot_status."') " ;
					// print_r($att_query);exit;
					$att_objResult = $this->db->query($att_query);
					if ($att_objResult == true) {
						return 'Updated';
					}else{
						return 'Attendance Not Updated Some Error Occurred, Please Try Again.';
					}
				}else{
					$att_query = "UPDATE hrmbackup.hrm_attendancce SET att_in = '". $checkIN ."' , att_out = '". $checkOUT ."' WHERE employee_id = '".$emp_id."' AND att_date = '".$date."' "  ;
					// print_r($att_query);exit;
					$att_objResult = $this->db->query($att_query);
					if ($att_objResult == true) {
						return 'Updated';
					}else{
						return 'Attendance Not Updated Some Error Occurred, Please Try Again.';
					}
				}
			}else{
				return 'Some Error Occurred, Please Try Again.';
			}
		// $arrResult 		= $objResult->result_array();
		// $objResult->free_result();
		// return $arrResult;
		}
	}
	function updateAttendanceStatusReject($id, $status){
		if($status == 2){
			$query 			= "UPDATE hrmbackup.hrm_attendance_approval SET status = '". $status ."' WHERE id = '".$id."' ";
			$objResult 		= $this->db->query($query);
			if ($objResult == true) {
				return 'Updated';
			}else{
				return 'Some Error Occurred, Please Try Again.';
			}
		}
	}
}
?>