<?php
class Model_Attendance_Management extends Model_Master {
	
	public $strHierarchy;
		
	function __construct() {
		parent::__construct();
	}
	function fetch_timing(){
		$query = $this->db->get("hrm_attendancce");
		return $query;
	}
	function getBranch($branch_name){
		$query = "SELECT * from hrm_companies WHERE company_name LIKE '".$branch_name."%'";
		$objResult 		= $this->db->query($query);
		$arrResult 		= $objResult->result_array();
		$objResult->free_result();
		return $arrResult;
	}
	function updateEmployeeBranch($branch_id, $emp_code){

		$query 			= "UPDATE hrm_employee SET emp_company_id = '".$branch_id."' Where emp_code = '".$emp_code."'";
		$objResult 		= $this->db->query($query);
		$arrResult 		= $objResult->result_array();
		$objResult->free_result();
		return $arrResult;
	}
	function insertBranch($branch_name){
		$query = "INSERT into hrm_companies (`company_name`,`company_address`,`company_country_id`,`company_currency_id`,`company_head_id`,`company_annual_leaves`,`company_sick_leaves`) VALUES ('".$branch_name."','".$branch_name."','91','91','0','10','10')";
		$objResult 		= $this->db->query($query);
		$arrResult 		= $objResult->result_array();
		$objResult->free_result();
		return $arrResult[0];
	}

	function fetch_attendance_by_date($emp_id, $date){
		$query = "SELECT * from hrm_attendancce WHERE att_date LIKE '%".$date."%' and employee_id = ".$emp_id;
		$objResult 		= $this->db->query($query);
		$arrResult 		= $objResult->result_array();
		$objResult->free_result();

		return $arrResult[0];

	}

	function fetch_daily_attendance($emp_id, $date){
		$query 			= "SELECT * from hrm_attendancce WHERE employee_id = '".$emp_id."' and att_date = '".$date."' ";
		$objResult 		= $this->db->query($query);
		$arrResult 		= $objResult->result_array();
		$objResult->free_result();
		return $arrResult[0];
	}

	function fetch_month_timing($emp_id, $month, $year){
		$date 		 	= $year.'-'.$month;
		$query 			= "SELECT * from hrm_attendancce WHERE att_date LIKE '%".$date."%' and employee_id = ".$emp_id;
		$objResult 		= $this->db->query($query);
		$arrResult 		= $objResult->result_array();
		$objResult->free_result();

		return $arrResult;

	}
	function insert($data)
	{
		$this->db->insert_batch('hrm_attendancce', $data);
	}
	// function totalLeavesTemp(){
	// 	return "asdfsdfasdfd";
	// }
	function getAttendanceRecordsPK($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true, $filterStr = false) {
				
		$db = mysqli_connect(localhost, $this->config->item('localhost'));
		$strQuery = "SELECT U.Badgenumber as USERID, min(C.CHECKTIME) as 'IN', max(C.CHECKTIME) as 'OUT', cast(C.CHECKTIME as date) as 'DATE' FROM " . TABLE_ATTENDANCE_PK . " C left join USERINFO U ON U.USERID = C.USERID WHERE ";
		
		if($arrWhere['USERID'] != '') {
			$strQuery .= " U.Badgenumber = " . (int)$arrWhere['USERID'];
		} else if($arrWhere['USERID in '] != '') {
			$strQuery .= " U.Badgenumber in (" . $arrWhere['USERID in '] . ")";
		}
		if($arrWhere['DATE <= '] != '') {
			$strQuery .= " AND C.CHECKTIME <= '" . $arrWhere['DATE <= '] . "'";
		}
		if($arrWhere['DATE >= '] != '') {
			$strQuery .= " AND C.CHECKTIME >= '" . $arrWhere['DATE >= '] . "'";
		}
		
		$strQuery .= " GROUP BY U.Badgenumber, cast(C.CHECKTIME as date) ORDER BY U.Badgenumber, 'DATE' ASC";
		
		$objResult = mysqli_connect($db, $strQuery);
		$arrResult = array();
		
		while( $arrRow = mysqli_fetch_array( $objResult, MYSQLI_FETCH_ASSOC) ) {
			$arrResult[] = $arrRow;
		}
		
		return $arrResult;
	}
	
	function getAttendanceRecordsDxb($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true, $filterStr = false) {
				
		$db = mysqli_connect(ATTENDANCE_DB_HOST_DXB, $this->config->item('attendance_db_dxb'));
		$strQuery = "SELECT UserID AS USERID, max(Punch1) as 'IN', max(OutPunch) as 'OUT', cast(PDate as date) as 'DATE' FROM " . TABLE_ATTENDANCE_DXB . " WHERE ";
		
		if($arrWhere['USERID'] != '') {
			$strQuery .= " USERID = " . (int)$arrWhere['USERID'];
		} else if($arrWhere['USERID in '] != '') {
			$strQuery .= " USERID in (" . $arrWhere['USERID in '] . ")";
		}
		
		if($arrWhere['DATE <= '] != '') {
			$strQuery .= " and PDate <= '" . $arrWhere['DATE <= '] . "'";
		}
		if($arrWhere['DATE >= '] != '') {
			$strQuery .= " and PDate >= '" . $arrWhere['DATE >= '] . "'";
		}
		
		$strQuery .= " GROUP BY USERID, cast(PDate as date) ORDER BY USERID, 'DATE' ASC";
		
		$objResult = mysqli_connect($db, $strQuery);
		$arrResult = array();
		
		while( $arrRow = mysqli_fetch_array( $objResult, MYSQLI_FETCH_ASSOC) ) {
			$arrResult[] = $arrRow;
		}
		
		return $arrResult;
	}
	
	function getClockingRecordsPK($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true) {
		
		$db = mysqli_connect(localhost, $this->config->item('localhost'));
		$strQuery = "SELECT U.Badgenumber as USERID, C.CHECKTIME as 'DATE' FROM " . TABLE_ATTENDANCE_PK . " C left join USERINFO U ON U.USERID = C.USERID WHERE ";
		
		if($arrWhere['USERID'] != '') {
			$strQuery .= " U.Badgenumber = " . (int)$arrWhere['USERID'];
		} else if($arrWhere['USERID in '] != '') {
			$strQuery .= " U.Badgenumber in (" . $arrWhere['USERID in '] . ")";
		}
		if($arrWhere['DATE <= '] != '') {
			$strQuery .= " AND C.CHECKTIME <= '" . $arrWhere['DATE <= '] . "'";
		}
		if($arrWhere['DATE >= '] != '') {
			$strQuery .= " AND C.CHECKTIME >= '" . $arrWhere['DATE >= '] . "'";
		}
		
		$strQuery .= " ORDER BY 'DATE' ASC";
		
		$objResult = mysqli_connect($db, $strQuery);
		$arrResult = array();
		
		while( $arrRow = mysqli_fetch_array( $objResult, MYSQLI_FETCH_ASSOC) ) {
			$arrResult[] = $arrRow;
		}
		
		return $arrResult;
	}
	
	function getLeaves($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		// return $arrWhere;	
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
			
			if(strpos($key, 'custom_string') !== false) {
				$this->db->where($arrWhere[$key], NULL, false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$this->db->order_by('l.leave_id', 'DESC');
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}		
		
		$objResult = $this->db->get(TABLE_ATTENDANCE_LEAVES . ' l ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getTotalLeaves($arrWhere = array()) {
		
		$this->db->select(' count(*) as total_count ');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
			
			if(strpos($key, 'custom_string') !== false) {
				$this->db->where($arrWhere[$key], NULL, false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}		
		
		$objResult = $this->db->get(TABLE_ATTENDANCE_LEAVES . ' l ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return (int)$arrResult[0]['total_count'];
	}
	
	function getLeavesForApproval($arrWhere = array(), $rowsLimit = '', $rowsOffset = '', $doSort = true) {
		
		$this->db->distinct();
		$this->db->select(' e.emp_full_name, e.emp_authority_id, e.emp_annual_leaves, e.emp_sick_leaves, l.* ');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = l.emp_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
			if(strpos($key, 'custom_string') !== false) {
				$this->db->where($arrWhere[$key], NULL, false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		if((int)$rowsLimit > 0) {
			$this->db->limit((int)$rowsLimit, (int)$rowsOffset);
		}		
		
		$this->db->order_by('l.leave_id', 'DESC');
		
		$objResult = $this->db->get(TABLE_ATTENDANCE_LEAVES . ' l ');
		// print_r($this->db->last_query());exit;
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	// function getTotalLeavesForApproval($arrWhere = array()) {
	// 	return "sdfdfdsdfdsfsdfsd";
	// }
	function getTotalLeavesForApproval($arrWhere = array()) {
		
		$this->db->distinct();
		$this->db->select(' count(*) as total_count ');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = l.emp_id', 'left');
		$this->db->join(TABLE_EMPLOYEE_SUPERVISORS . ' es ', 'es.emp_id = e.emp_id', 'left');
		
		foreach($arrWhere as $key => $value) {
			if(strpos($key, ' in ')) {
				$this->db->where($key, $arrWhere[$key], false);
				unset($arrWhere[$key]);
			}
			
			if(strpos($key, 'custom_string') !== false) {
				$this->db->where($arrWhere[$key], NULL, false);
				unset($arrWhere[$key]);
			}
		}
		
		if(count($arrWhere)) {
			$this->db->where($arrWhere);			
		}
		
		$objResult = $this->db->get(TABLE_ATTENDANCE_LEAVES . ' l ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return (int)$arrResult[0]['total_count'];
	}
	
	function getLeavesSummary($arrWhere = array(), $rowsLimit = '', $rowsOffset = '') {
		
		$this->db_attend = $this->load->database('attendance', TRUE);
		
		if(count($arrWhere)) {
			$this->db_attend->where($arrWhere);			
		}		
		
		$this->db_attend->order_by('emp_code', 'ASC');
		
		$objResult = $this->db_attend->get(TABLE_ATTENDANCE_SUMMARY);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult;
	}
	
	function getLeaveDetails($leaveID) {
		
		$this->db->select(' l.*, lc.leave_category, e.emp_full_name, e.emp_designation, e.emp_annual_leaves, e.emp_sick_leaves, e.emp_flexi_leaves, e.emp_edu_leaves, e.emp_maternity_leaves, e1.emp_full_name as processed_by_name ');
		$this->db->join(TABLE_ATTENDANCE_LEAVE_CATEGORIES . ' lc ', 'lc.leave_category_id = l.leave_category', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e ', 'e.emp_id = l.emp_id', 'left');
		$this->db->join(TABLE_EMPLOYEE . ' e1 ', 'e1.emp_id = l.processed_by', 'left');
		
		$this->db->where(array('leave_id' => (int)$leaveID));
		
		$objResult = $this->db->get(TABLE_ATTENDANCE_LEAVES . ' l ');
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		
		return $arrResult[0];
	}

	function totalPresent($emp_id, $month, $year){
		
		$date = $year.'-'.$month.'-01';
		// print("SELECT count(*) from hrm_attendancce WHERE att_date LIKE '%".$date."%' and employee_id = ".$emp_id." and att_in != '00:00:00' and att_out != '00:00:00'");exit;
		$query = "SELECT count(*) AS present from hrm_attendancce WHERE att_date LIKE '%".$date."%' and employee_id = ".$emp_id." and att_in != '00:00:00' and att_out != '00:00:00'";
		$objResult = $this->db->query($query);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		return $arrResult;

	}

	function totalAbsent($emp_id, $month, $year){
		$date 		 = $year.'-'.$month;
		$totalLeaves = $this->totalLeaves($emp_id,$month,$year);
		$query = "SELECT count(*) AS `absent` from hrm_attendancce WHERE att_date LIKE '%".$date."%' and employee_id = ".$emp_id." and att_in = '00:00:00' and att_out = '00:00:00'";
		$objResult = $this->db->query($query);
		$arrResult = $objResult->result_array();
		$objResult->free_result();

		return $arrResult[0]['absent'] - $totalLeaves;
	}
	
	function checkIfLeave($emp_id, $month, $year){
		$date 		 	= $year.'-'.$month;
		$query 			= "SELECT * from hrm_employee_leaves WHERE leave_from LIKE '%".$date."%' and leave_type = 1 and emp_id = ".$emp_id." and leave_status = '1' ";
		// return $query;
		$objResult 		= $this->db->query($query);
		$arrResult 		= $objResult->result_array();
		$objResult->free_result();

		return $arrResult[0]['leave_days'];
	}

	function totalLeaves($emp_id, $month, $year){
		$date = $year.'-'.$month;

		$leave_query = "SELECT leave_days from hrm_employee_leaves WHERE leave_from LIKE '%".$date."%' and emp_id = ".$emp_id." and leave_status = '1'";

		$leaveobjResult = $this->db->query($leave_query);
		$leavearrResult = $leaveobjResult->result_array();
		$leaveobjResult->free_result();

		$supers 			= $leavearrResult;
		$super_key 			= 'leave_days';
		$totalLeaves 		= array_map(function($supers) use ($super_key) {
			return is_object($supers) ? $supers->$super_key : $supers[$super_key];
		}, $supers);
		return $totalLeaves = array_sum($totalLeaves);
	}

	function save_attIN($emp_id, $strDate, $stratt_in){

		$att_query = "INSERT INTO hrm_attendancce (employee_id, att_date, att_in, att_out, hours) VALUES ('".$emp_id."', '".$strDate."', '". $stratt_in ."', NULL, NULL) ";
		// print_r($att_query);exit;
		$objResult = $this->db->query($att_query);
		if ($objResult == true) {
			return 'Updated';
		}else{
			return 'Some Error Occurred, Please Try Again.';
		}
	}
	
	function update_attIN($emp_id, $strDate, $stratt_in){

		$att_query = "UPDATE hrm_attendancce SET att_in = '". $stratt_in ."' WHERE employee_id = '".$emp_id."' AND att_date = '".$strDate."' "  ;
		$objResult = $this->db->query($att_query);
		if ($objResult == true) {
			return 'Updated';
		}else{
			return 'Some Error Occurred, Please Try Again.';
		}
	}

	function save_attOUT($emp_id, $strDate, $stratt_out, $hours, $status){
		$att_query = "UPDATE hrm_attendancce SET att_out = '". $stratt_out ."', hours = '". $hours ."', ot_status = '". $status ."' WHERE employee_id = '".$emp_id."' AND att_date = '".$strDate."' "  ;
		// print_r($att_query);exit;
		$objResult = $this->db->query($att_query);
		if ($objResult == true) {
			return 'Updated';
		}else{
			return 'Some Error Occurred, Please Try Again.';
		}
	}

	function updateattOUT($emp_id, $strDate, $stratt_out, $hours, $status){
		$att_query = "UPDATE hrm_attendancce SET att_out = '". $stratt_out ."', hours = '". $hours ."', ot_status = '". $status ."' WHERE employee_id = '".$emp_id."' AND att_date = '".$strDate."' "  ;
		// print_r($att_query);exit;
		$objResult = $this->db->query($att_query);
		if ($objResult == true) {
			return 'Updated';
		}else{
			return 'Some Error Occurred, Please Try Again.';
		}
	}

	function getSupervisors($emp_id){
		$objResult = $this->db->query('Select supervisor_emp_id from hrm_employee_supervisors where emp_id = '.$emp_id);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		return $arrResult;
	}

	function getEmployeetax($emp_id){
		$query = "SELECT e.emp_id, tax.* FROM hrm_tax_chart as tax inner join hrm_employee as e on e.emp_code = tax.emp_id where e.emp_id = ".$emp_id;
		$objResult = $this->db->query($query);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		return $arrResult;
	}

	function getEmployeeNetSalary($emp_id, $month, $year){
		$query = "SELECT net_salary, payroll_earning_basic, payroll_earning_medical_relief, payroll_deduction_tax FROM `hrm_employee_payroll` WHERE emp_id = ".$emp_id." AND payroll_month = ".$month." AND payroll_year = ".$year;
		$objResult = $this->db->query($query);
		$arrResult = $objResult->result_array();
		$objResult->free_result();
		return $arrResult[0];
	}
}
?>