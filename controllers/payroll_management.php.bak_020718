<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payroll_Management extends Master_Controller {
	
	private $arrData = array();
	public $arrRoleIDs = array();
	private $maxLinks;
	private $limitRecords;
	private $employeeID = 0;
	
	function __construct() {
		
		parent::__construct();
		
		$this->load->model('model_payroll_management', 'payroll', true);
		$this->load->model('model_employee_management', 'employee', true);
		
		$this->arrRoleIDs       				= array(HR_ADMIN_ROLE_ID, WEB_ADMIN_ROLE_ID, SUPER_ADMIN_ROLE_ID, HR_EMPLOYEE_ROLE_ID, HR_TEAMLEAD_ROLE_ID, HR_MANAGER_ROLE_ID, ACCOUNT_MANAGER_ROLE_ID, ACCOUNT_EMPLOYEE_ROLE_ID);
		$this->arrData["baseURL"] 				= $this->baseURL . '/';
		$this->arrData["imagePath"] 			= $this->imagePath;
		$this->arrData["screensAllowed"] 		= $this->screensAllowed;
		$this->arrData["currentController"] 	= $this->currentController;
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->arrData["emailTemplatesFolder"]	= EMAIL_TEMPLATE_FOLDER;
		$this->maxLinks 						= MAX_PAGING_VISIBLE;
		$this->limitRecords 					= MAX_RECORDS_LISTING;
		$this->arrData["forcedAccessRoles"]		= $this->config->item('forced_access_roles');
		
		$currentActionArray = array();
		while(!empty($this->arrData["screensAllowed"]))
		{
			$temp = array_pop($this->arrData["screensAllowed"]);
			if($temp['module_name']== $this->currentAction){
				array_push($currentActionArray,$temp);
			}
		}
		$currentActionArray = $currentActionArray[0];
		$this->arrData['canWrite'] = $currentActionArray["can_write"];
		$this->arrData['canDelete'] = $currentActionArray["can_delete"];		
	}
	
	public function index() {
		
		$moduleID = getValue($this->modulesAllowed, 'module_name', $this->currentController, 'module_id');
		$this->arrData['allowedSubModulesList'] = getValue($this->modulesAllowedForMenu, 'module_name', $this->currentController, 'sub_menu'); 
		$this->template->write_view('content', 'payroll_management/index', $this->arrData);
		$this->template->render();
		
	}
	
	public function save_account($employeeID = 0) {
		
		$employeeID = (int)$employeeID;
		if(!$employeeID) {
			$employeeID = $this->employeeID;
		}
		$arrWhere = array();
		
		if($employeeID) {			
			$arrWhere = array(
							'emp_id' => $employeeID
							);
							
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$arrWhere['emp_company_id'] = $this->userCompanyID;
			}
		} else {
			if(!isAdmin($this->userRoleID)) {				
				# SET LOG				
				redirect($this->baseURL . '/message/access_denied');
				exit;
			}
		}
		
		#################################### FORM VALIDATION START ####################################		
			
		$this->form_validation->set_rules('empBank', 'Bank', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('empBranch', 'Branch Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empAccountNum', 'Account Number', 'trim|required|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
				
			$arrValues = array(
								'emp_salary_bank_id' => $this->input->post("empBank"),
								'emp_salary_bank_branch' => $this->input->post("empBranch"),
								'emp_salary_bank_account_number' => $this->input->post("empAccountNum"),
								'modified_by' => $this->userEmpNum,
								'modified_date' => date($this->arrData["dateTimeFormat"])
								);
											
			$this->employee->saveValues(TABLE_EMPLOYEE, $arrValues, $arrWhere);
						
			# SET LOG
			debugLog("Banking Details Updated: [EmpID: ".$employeeID."]");
						
			$this->session->set_flashdata('success_message', 'Details Saved Successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/list_accounts');
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT EMPLOYEE RECORD
		if($employeeID) {
			$arrEmpWhere = array();
			$arrEmpWhere['e.emp_id'] = $employeeID;			
							
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$arrEmpWhere['e.emp_company_id'] = $this->userCompanyID;
			}
			
			$this->arrData['record'] = $this->employee->getEmployeeDetail($arrEmpWhere, false);
		}
		
		if(!count($this->arrData['record'])) {
			redirect(base_url() . 'message/access_denied');
			exit;
		}
		
		# CODE FOR PAGE CONTENT
		$this->arrData['employeeID'] = $employeeID;
		$this->arrData['arrBanks'] = $this->configuration->getBanks();
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'payroll_management/save_account', $this->arrData);
		$this->template->render();
	}
	
	public function list_accounts($pageNum = 1) {
				
		if((int)$pageNum <= 0) {
			$pageNum = 1;
		}
		
		$arrWhere = array();
		
		if ($this->input->post()) {
			
			if($this->input->post("empCode")) {
				$arrWhere['e.emp_code'] = $this->input->post("empCode");
				$this->arrData['empCode'] = $this->input->post("empCode");
			}
			
			if($this->input->post("empIP")) {
				$arrWhere['e.emp_ip_num'] = $this->input->post("empIP");
				$this->arrData['empIP'] = $this->input->post("empIP");
			}
			
			if($this->input->post("empCompany")) {
				$arrWhere['e.emp_company_id'] = $this->input->post("empCompany");
				$this->arrData['empCompany'] = $this->input->post("empCompany");
			}
			
			if($this->input->post("empDepartment")) {
				$arrWhere['e.emp_job_category_id'] = $this->input->post("empDepartment");
				$this->arrData['empDepartment'] = $this->input->post("empDepartment");
			}
			
			if($this->input->post("empSupervisor")) {
				$strHierarchy = $this->employee->getHierarchyWithMultipleAuthorities($this->input->post("empSupervisor"));
				$arrWhere['es.supervisor_emp_id in '] = '(' . $strHierarchy . ')';
				$this->arrData['empSupervisor'] = $this->input->post("empSupervisor");
			}
			
			if($this->input->post("empBank")) {
				$arrWhere['e.emp_salary_bank_id'] = $this->input->post("empBank");
				$this->arrData['empBank'] = $this->input->post("empBank");
			}
			
		} else {
			if(!isAdmin($this->userRoleID)) {
				$arrWhere['e.emp_employment_status <= '] = STATUS_EMPLOYEE_ONNOTICEPERIOD;
				$this->arrData['empStatus'] = 'active';
			}
		}
			
		if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
			$arrWhere['e.emp_company_id'] = $this->userCompanyID;
			$this->arrData['empCompany'] = $this->userCompanyID;
		}
		
		if(!isAdmin($this->userRoleID)) {
			$arrWhere['e.emp_status'] = STATUS_ACTIVE;
		}
		
		$this->arrData['totalRecordsCount'] = $this->employee->getTotalEmployees($arrWhere);
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->employee->getEmployees($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#/', 'frmListEmployees');
		
		# CODE FOR PAGE CONTENT
		$this->arrData['arrCompanies'] = $this->configuration->getCompanies();
		if(!isAdmin($this->userRoleID)) {
			$empJobType = $this->configuration->getValues(TABLE_GRADES, 'job_type', array('grade_id' => $this->arrData['arrEmployee']['emp_grade_id']));
			$empJobType = $empJobType[0]['job_type'];
			$this->arrData['empDesignations'] = $this->configuration->getGrades(array('grade_status' => STATUS_ACTIVE, 'job_type' => $empJobType));
		} else {
			$this->arrData['empDesignations'] = $this->configuration->getGrades(array('grade_status' => STATUS_ACTIVE));
		}
		$this->arrData['empDepartments'] = $this->configuration->getValues(TABLE_JOB_CATEGORY, '*', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		
		if(!isAdmin($this->userRoleID)) {		
			$this->arrData['arrSupervisors'] = $this->employee->getEmployees(array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE));
			$this->arrData['arrEmployees'] = $this->employee->getEmployees(array('es.supervisor_emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE));
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE));
		} else {
			$this->arrData['arrSupervisors'] = $this->employee->getSupervisors();
			$this->arrData['arrEmployees'] = $this->employee->getEmployees(array('e.emp_code >' => ZERO));
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		}		
		
		$finalResult = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrEmployees'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}
		}		
		$this->arrData["arrEmployees"] = $finalResult;
		
		$finalResultSupervisors = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResultSupervisors[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrSupervisors'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResultSupervisors[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResultSupervisors[$arrJobCategories[$i]['job_category_name']]);
			}
		}		
		$this->arrData["arrSupervisors"] = $finalResultSupervisors;
		
		if($this->userRoleID == WEB_ADMIN_ROLE_ID) {
			foreach($this->arrData['arrRecords'][0] as $cName => $cVal) {
				$this->arrData['arrTblColumns'][] =  $cName;
			}
		}
		
		$this->arrData['arrEmploymentStatuses'] = $this->employee->populateEmploymentStatus();
		$this->arrData['arrBanks'] = $this->configuration->getBanks();
		$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'payroll_management/list_accounts', $this->arrData);
		$this->template->render();
	}
	
	public function save_payroll($payrollID = 0) {
				
		#################################### FORM VALIDATION START ####################################		
			
		$this->form_validation->set_rules('empID', 'Employee', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('payrollMonth', 'Month', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('payrollYear', 'Year', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('payrollBasic', 'Basic Salary', 'trim|required|numeric|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			
			$arrWhere = array(
							  'ep.emp_id' => $this->input->post("empID"),
							  'ep.payroll_month' => $this->input->post("payrollMonth"),
							  'ep.payroll_year' => $this->input->post("payrollYear")
							  );
							  
			$arrRecords = $this->payroll->getPayrolls($arrWhere);
			
			$arrValues = array(
								'emp_id' => $this->input->post("empID"),
								'payroll_month' => $this->input->post("payrollMonth"),
								'payroll_year' => $this->input->post("payrollYear"),
								'payroll_earning_basic' => $this->input->post("payrollBasic"),
								'payroll_earning_housing' => $this->input->post("payrollHousing"),
								'payroll_earning_transport' => $this->input->post("payrollTransport"),
								'payroll_earning_utility' => $this->input->post("payrollUtility"),
								'payroll_earning_travel' => $this->input->post("payrollTravel"),
								'payroll_earning_health' => $this->input->post("payrollHealth"),
								'payroll_earning_fuel' => $this->input->post("payrollFuel"),
								'payroll_earning_mobile' => $this->input->post("payrollMobile"),
								'payroll_earning_medical_relief' => $this->input->post("payrollMedical"),
								
								'payroll_earning_bonus' => $this->input->post("payrollBonus"),
								'payroll_earning_annual_leave_encashment' => $this->input->post("payrollLeaveEnc"),
								'payroll_earning_claims' => $this->input->post("payrollClaim"),
								'payroll_earning_commission' => $this->input->post("payrollCommission"),
								'payroll_earning_annual_ticket' => $this->input->post("payrollAnnualTicket"),
								'payroll_earning_gratuity' => $this->input->post("payrollGratuity"),
								'payroll_earning_survey_expense' => $this->input->post("payrollSurveyExpense"),
								'payroll_earning_settlement' => $this->input->post("payrollSettlement"),
								
								'payroll_earning_misc' => $this->input->post("payrollEarningMisc"),
								'payroll_deduction_tax' => $this->input->post("payrollTax"),
								'payroll_deduction_pf' => $this->input->post("payrollPF"),
								'payroll_deduction_loan' => $this->input->post("payrollLoan"),
								'payroll_deduction_eobi' => $this->input->post("payrollEOBI"),
								'payroll_deduction_misc' => $this->input->post("payrollDeductionMisc"),
								'modified_by' => $this->userEmpNum,
								'modified_date' => date($this->arrData["dateTimeFormat"])
								);
			
			if(count($arrRecords)) {
				$arrValues['modified_by'] = $this->userEmpNum;
				$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
				$this->payroll->saveValues(TABLE_EMPLOYEE_PAYROLL, $arrValues, $arrWhere);
			} else {
				$arrValues['created_by'] = $this->userEmpNum;
				$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
				$this->payroll->saveValues(TABLE_EMPLOYEE_PAYROLL, $arrValues);
			}
						
			$this->session->set_flashdata('success_message', 'Payroll Details Saved Successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/list_payroll');
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT EMPLOYEE RECORD
		if($payrollID) {
			$this->arrData['record'] = $this->payroll->getPayrolls(array('ep.payroll_id' => $payrollID));
			$this->arrData['record'] = $this->arrData['record'][0];
			
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$arrEmployee = $this->employee->getEmployees(
															  array(
																	  'e.emp_id' => $this->arrData['record']['emp_id'],
																	  'e.emp_company_id' => $this->userCompanyID
																  )
															  );
				if(!count($arrEmployee)) {
					redirect(base_url() . 'message/access_denied');
					exit;
				}
			}
		}
		
		if(!isAdmin($this->userRoleID)) {
			
			$arrWhereSupervisors = array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE);
			$arrWhereEmployees = array('es.supervisor_emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE);
			
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$arrWhereSupervisors['e.emp_company_id'] = $this->userCompanyID;
				$arrWhereEmployees['e.emp_company_id'] = $this->userCompanyID;
			}
			
			$this->arrData['arrSupervisors'] = $this->employee->getEmployees($arrWhereSupervisors);
			$this->arrData['arrEmployees'] = $this->employee->getEmployees($arrWhereEmployees);
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE));
		} else {
			$this->arrData['arrSupervisors'] = $this->employee->getSupervisors();
			$this->arrData['arrEmployees'] = $this->employee->getEmployees(array('e.emp_code >' => ZERO));
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		}		
		
		$finalResult = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrEmployees'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}
		}		
		$this->arrData["arrEmployees"] = $finalResult;
		
		# CODE FOR PAGE CONTENT
		$this->arrData['arrMonths'] = $this->config->item('months');
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'payroll_management/save_payroll', $this->arrData);
		$this->template->render();
	}
	
	public function list_payroll($pageNum = 1) {
		
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {				
				if(!$this->payroll->deleteValue(TABLE_EMPLOYEE_PAYROLL, null, array('payroll_id' => (int)$this->input->post("record_id")))) {
					echo NO; exit;
				} else {
					echo YES; exit;
				}								
			}
		}
		##########################
		
		if((int)$pageNum <= 0) {
			$pageNum = 1;
		}
		
		$arrWhere = array();
		
		if ($this->input->post()) {
			
			if($this->input->post("empID")) {
				$arrWhere['e.emp_id'] = $this->input->post("empID");
				$this->arrData['empID'] = $this->input->post("empID");
			}
			
			if($this->input->post("empCompany")) {
				$arrWhere['e.emp_company_id'] = $this->input->post("empCompany");
				$this->arrData['empCompany'] = $this->input->post("empCompany");
			}
			
			if($this->input->post("empSupervisor")) {
				$strHierarchy = $this->employee->getHierarchyWithMultipleAuthorities($this->input->post("empSupervisor"));
				$arrWhere['es.supervisor_emp_id in '] = '(' . $strHierarchy . ')';
				$this->arrData['empSupervisor'] = $this->input->post("empSupervisor");
			}
			
			if($this->input->post("selMonth")) {
				$arrWhere['ep.payroll_month'] = $this->input->post("selMonth");
				$this->arrData['selMonth'] = $this->input->post("selMonth");
			}
			
			if($this->input->post("selYear")) {
				$arrWhere['ep.payroll_year'] = $this->input->post("selYear");
				$this->arrData['selYear'] = $this->input->post("selYear");
			}
			
		} else {
			if(!isAdmin($this->userRoleID)) {
				$arrWhere['e.emp_employment_status <= '] = STATUS_EMPLOYEE_ONNOTICEPERIOD;
				$this->arrData['empStatus'] = 'active';
			}
		}
			
		if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
			$arrWhere['e.emp_company_id'] = $this->userCompanyID;
			$this->arrData['empCompany'] = $this->userCompanyID;
		}
		
		if(!isAdmin($this->userRoleID)) {
			$arrWhere['e.emp_status'] = STATUS_ACTIVE;
		}
		
		$this->arrData['totalRecordsCount'] = $this->payroll->getTotalPayrolls($arrWhere);
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->payroll->getPayrolls($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#/', 'frmListPayroll');
		
		# CODE FOR PAGE CONTENT
		$this->arrData['arrCompanies'] = $this->configuration->getCompanies();
		
		$this->arrData['empDepartments'] = $this->configuration->getValues(TABLE_JOB_CATEGORY, '*', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		
		if(!isAdmin($this->userRoleID)) {
			
			$arrWhereSupervisors = array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE);
			$arrWhereEmployees = array('es.supervisor_emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE);
			
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$arrWhereSupervisors['e.emp_company_id'] = $this->userCompanyID;
				$arrWhereEmployees['e.emp_company_id'] = $this->userCompanyID;
			}
			
			$this->arrData['arrSupervisors'] = $this->employee->getEmployees($arrWhereSupervisors);
			$this->arrData['arrEmployees'] = $this->employee->getEmployees($arrWhereEmployees);
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE));
		} else {
			$this->arrData['arrSupervisors'] = $this->employee->getSupervisors();
			$this->arrData['arrEmployees'] = $this->employee->getEmployees(array('e.emp_code >' => ZERO));
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		}		
		
		$finalResult = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrEmployees'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}
		}		
		$this->arrData["arrEmployees"] = $finalResult;
		
		$finalResultSupervisors = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResultSupervisors[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrSupervisors'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResultSupervisors[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResultSupervisors[$arrJobCategories[$i]['job_category_name']]);
			}
		}		
		$this->arrData["arrSupervisors"] = $finalResultSupervisors;
		
		$this->arrData['arrEmploymentStatuses'] = $this->employee->populateEmploymentStatus();
		$this->arrData['arrBanks'] = $this->configuration->getBanks();
		$this->arrData['arrMonths'] = $this->config->item('months');
		$this->arrData['arrLocations'] = $this->configuration->getLocations();
		$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'payroll_management/list_payroll', $this->arrData);
		$this->template->render();
	}
}

/* End of file employee_management.php */
/* Location: ./application/controllers/employee_management.php */