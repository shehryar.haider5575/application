<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include('attendance_management.php');
class Payroll_Management extends Attendance_Management {
	
	private $arrData = array();
	public $arrRoleIDs = array();
	private $maxLinks;
	private $limitRecords;
	public $payrollCloseMonth;
	public $payrollCloseYear;
	private $employeeID = 0;
	
	function __construct() {
		
		parent::__construct();
		$this->load->model('model_attendance_management');
		$this->load->model('model_payroll_management', 'payroll', true);
		$this->load->model('model_employee_management', 'employee', true);
		$this->load->model('model_deduction_management', 'deduction', true);
		$this->load->model('model_compliance_management', 'compliance', true);
		$this->load->model('model_system_configuration', 'model_system', true);
		$this->load->model('model_attendance_management', 'attandance', true);
		
		$this->arrRoleIDs       				= array(HR_ADMIN_ROLE_ID, WEB_ADMIN_ROLE_ID, SUPER_ADMIN_ROLE_ID, HR_EMPLOYEE_ROLE_ID, HR_TEAMLEAD_ROLE_ID, HR_MANAGER_ROLE_ID, ACCOUNT_MANAGER_ROLE_ID, ACCOUNT_EMPLOYEE_ROLE_ID, 26);
		$this->arrData["baseURL"] 				= $this->baseURL . '/';
		$this->arrData["imagePath"] 			= $this->imagePath;
		$this->arrData["screensAllowed"] 		= $this->screensAllowed;
		$this->arrData["currentController"] 	= $this->currentController;
		$this->arrData["dateFormat"] 			= DATE_FORMAT;
		$this->arrData["dateTimeFormat"] 		= DATE_TIME_FORMAT;
		$this->arrData["showDateFormat"] 		= SHOW_DATE_TIME_FORMAT;
		$this->arrData["emailTemplatesFolder"]	= EMAIL_TEMPLATE_FOLDER;
		$this->arrData["pictureFolder"]			= PROFILE_PICTURE_FOLDER;
		$this->arrData["pictureFolderShow"]		= str_replace('./', '', PROFILE_PICTURE_FOLDER);
		$this->maxLinks 						= MAX_PAGING_VISIBLE;
		$this->limitRecords 					= MAX_RECORDS_LISTING;
		$this->arrData["forcedAccessRoles"]		= $this->config->item('forced_access_roles');
		
		$payrollCloseDate = explode('-', PAYROLL_CLOSE_DATE);
		$this->payrollCloseMonth = (int)$payrollCloseDate[1];
		$this->payrollCloseYear = (int)$payrollCloseDate[0];
		
		$currentActionArray = array();
		while(!empty($this->arrData["screensAllowed"]))
		{
			$temp = array_pop($this->arrData["screensAllowed"]);
			if($temp['module_name']== $this->currentAction){
				array_push($currentActionArray,$temp);
			}
		}
		$currentActionArray = $currentActionArray[0];
		$this->arrData['canWrite'] = $currentActionArray["can_write"];
		$this->arrData['canDelete'] = $currentActionArray["can_delete"];
		
		$this->load->model('model_attendance_management');
		$fetch_timing = $this->model_attendance_management->fetch_timing();
		$data1 = [];
		foreach ($fetch_timing->result() as $key => $row) {
			$data1[$key]['employee_id'] = $row->employee_id;
			$data1[$key]['attendance_date'] = $row->att_date;
			$data1[$key]['attendance_in'] = $row->att_in;
			$data1[$key]['attendance_out'] = $row->att_out;
			$data1[$key]['worktime'] = (int)$row->att_out - (int)$row->att_in; 
		}
		
		// print_r($data1);
		//exit;
		// print_r($data1);
		//exit;
		//if ($empID == $data1[$key])

		# DEDUCTION FORMULA #
		// foreach ($data1 as $key => $value) {
		// 	if ($empID == $value['employee_id']){

		// 	}
		// }
		// $ThatTime = "10:00:00";
		// $thistime = "09:00:00";
		// $minuteslate = (strtotime($ThatTime) - strtotime($thistime)) / 60;
		// $deduction = 0;
		// //echo $minuteslate;

		// function calDeduct($minuteslate, $deduction){
		//     if($minuteslate > 15){
		// 	$total = ($minuteslate - 15)*5;
		// 	$deduction = array();
		// 	array_push($deduction, $total);
		// 	return $deduction = array_sum($deduction);
			
		// }
		// }
		// $deduction = calDeduct($minuteslate, $deduction);
		// print_r($deduction);
		// echo $deduction;
		
	}
	
	public function index() {
		
		$moduleID = getValue($this->modulesAllowed, 'module_name', $this->currentController, 'module_id');
		$this->arrData['allowedSubModulesList'] = getValue($this->modulesAllowedForMenu, 'module_name', $this->currentController, 'sub_menu'); 
		$this->template->write_view('content', 'payroll_management/index', $this->arrData);
		$this->template->render();
		
	}
	
	public function save_account($employeeID = 0) {
		
		$employeeID = (int)$employeeID;
		if(!$employeeID) {
			$employeeID = $this->employeeID;
		}
		$arrWhere = array();
		
		if($employeeID) {			
			$arrWhere = array(
							'emp_id' => $employeeID
							);
							
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$arrWhere['emp_company_id'] = $this->userCompanyID;
			}
		} else {
			if(!isAdmin($this->userRoleID)) {				
				# SET LOG				
				redirect($this->baseURL . '/message/access_denied');
				exit;
			}
		}
		
		#################################### FORM VALIDATION START ####################################		
			
		$this->form_validation->set_rules('empPayMode', 'Pay Mode', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empBank', 'Bank', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('empBranch', 'Branch Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('empAccountNum', 'Account Number', 'trim|required|xss_clean');
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
				
			$arrValues = array(
								'emp_salary_bank_id' => $this->input->post("empBank"),
								'emp_salary_bank_branch' => $this->input->post("empBranch"),
								'emp_salary_bank_account_number' => $this->input->post("empAccountNum"),
								'emp_pay_mode' => $this->input->post("empPayMode"),
								'emp_payroll_execution' => $this->input->post("empPayrollExe"),
								'modified_by' => $this->userEmpNum,
								'modified_date' => date($this->arrData["dateTimeFormat"])
								);
											
			$this->employee->saveValues(TABLE_EMPLOYEE, $arrValues, $arrWhere);
						
			# SET LOG
			debugLog("Banking Details Updated: [EmpID: ".$employeeID."]");
						
			$this->session->set_flashdata('success_message', 'Details Saved Successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/list_accounts');
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR CURRENT EMPLOYEE RECORD
		if($employeeID) {
			$arrEmpWhere = array();
			$arrEmpWhere['e.emp_id'] = $employeeID;			
							
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$arrEmpWhere['e.emp_company_id'] = $this->userCompanyID;
			}
			
			$this->arrData['record'] = $this->employee->getEmployeeDetail($arrEmpWhere, false);
		}
		
		if(!count($this->arrData['record'])) {
			redirect(base_url() . 'message/access_denied');
			exit;
		}
		
		# CODE FOR PAGE CONTENT
		$this->arrData['employeeID'] = $employeeID;
		$this->arrData['arrBanks'] = $this->configuration->getBanks();		
		$this->arrData["arrPayModes"] = $this->config->item('payment_modes');
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'payroll_management/save_account', $this->arrData);
		$this->template->render();
	}
	
	public function list_accounts($pageNum = 1) {
				
		if((int)$pageNum <= 0) {
			$pageNum = 1;
		}
		
		$arrWhere = array();
		
		if ($this->input->post()) {
			
			if($this->input->post("empCode")) {
				$arrWhere['e.emp_code'] = $this->input->post("empCode");
				$this->arrData['empCode'] = $this->input->post("empCode");
			}
			
			if($this->input->post("empIP")) {
				$arrWhere['e.emp_ip_num'] = $this->input->post("empIP");
				$this->arrData['empIP'] = $this->input->post("empIP");
			}
			
			if($this->input->post("empCompany")) {
				$arrWhere['e.emp_company_id'] = $this->input->post("empCompany");
				$this->arrData['empCompany'] = $this->input->post("empCompany");
			}
			
			if($this->input->post("empDepartment")) {
				$arrWhere['e.emp_job_category_id'] = $this->input->post("empDepartment");
				$this->arrData['empDepartment'] = $this->input->post("empDepartment");
			}
			
			if($this->input->post("empSupervisor")) {
				$strHierarchy = $this->employee->getHierarchyWithMultipleAuthorities($this->input->post("empSupervisor"));
				$arrWhere['es.supervisor_emp_id in '] = '(' . $strHierarchy . ')';
				$this->arrData['empSupervisor'] = $this->input->post("empSupervisor");
			}
			
			if($this->input->post("empBank")) {
				$arrWhere['e.emp_salary_bank_id'] = $this->input->post("empBank");
				$this->arrData['empBank'] = $this->input->post("empBank");
			}
			
		} else {
			if(!isAdmin($this->userRoleID)) {
				$arrWhere['e.emp_employment_status <= '] = STATUS_EMPLOYEE_ONNOTICEPERIOD;
				$this->arrData['empStatus'] = 'active';
			}
		}
			
		if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
			$arrWhere['e.emp_company_id'] = $this->userCompanyID;
			$this->arrData['empCompany'] = $this->userCompanyID;
		}
		
		if(!isAdmin($this->userRoleID)) {
			$arrWhere['e.emp_status'] = STATUS_ACTIVE;
		}
		
		$this->arrData['totalRecordsCount'] = $this->employee->getTotalEmployees($arrWhere);
		$offSet = ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] = $this->employee->getEmployees($arrWhere, $this->limitRecords, $offSet);
		$numPages = ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] = displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#/', 'frmListEmployees');
		
		# CODE FOR PAGE CONTENT
		if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
			$this->arrData['arrCompanies'] = $this->configuration->getCompanies(array('company_id' => $this->userCompanyID));
		} else {
			$this->arrData['arrCompanies'] = $this->configuration->getCompanies();
		}
		
		if(!isAdmin($this->userRoleID)) {
			
			$empJobType = $this->configuration->getValues(TABLE_GRADES, 'job_type', array('grade_id' => $this->arrData['arrEmployee']['emp_grade_id']));
			$empJobType = $empJobType[0]['job_type'];
			$this->arrData['empDesignations'] = $this->configuration->getGrades(array('grade_status' => STATUS_ACTIVE, 'job_type' => $empJobType));
		} else {
			$this->arrData['empDesignations'] = $this->configuration->getGrades(array('grade_status' => STATUS_ACTIVE));
		}
		$this->arrData['empDepartments'] = $this->configuration->getValues(TABLE_JOB_CATEGORY, '*', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		
		if(isAdmin($this->userRoleID)) {		
			$this->arrData['arrSupervisors'] = $this->employee->getEmployees(array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE));
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$this->arrData['arrEmployees'] = $this->employee->getEmployees(array('es.supervisor_emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE, 'e.emp_company_id' => $this->userCompanyID));
			}else{
				$this->arrData['arrEmployees'] = $this->employee->getEmployees(array('es.supervisor_emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE));
			}
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE));
		} else {
			$this->arrData['arrSupervisors'] = $this->employee->getSupervisors();			
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$this->arrData['arrEmployees'] = $this->employee->getEmployees(array('e.emp_company_id' => $this->userCompanyID));
			} else {
				$this->arrData['arrEmployees'] = $this->employee->getEmployees(array());
			}
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		}		
		
		$finalResult = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrEmployees'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}
		}		
		$this->arrData["arrEmployees"] = $finalResult;
		
		$finalResultSupervisors = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResultSupervisors[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrSupervisors'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResultSupervisors[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResultSupervisors[$arrJobCategories[$i]['job_category_name']]);
			}
		}		
		$this->arrData["arrSupervisors"] = $finalResultSupervisors;
		
		if($this->userRoleID == WEB_ADMIN_ROLE_ID) {
			foreach($this->arrData['arrRecords'][0] as $cName => $cVal) {
				$this->arrData['arrTblColumns'][] =  $cName;
			}
		}
		
		$this->arrData['arrEmploymentStatuses'] = $this->employee->populateEmploymentStatus();
		$this->arrData['arrBanks'] = $this->configuration->getBanks();
		$this->arrData['frmActionURL'] = $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'payroll_management/list_accounts', $this->arrData);
		$this->template->render();
	}
	
 	public function save_payroll($payrollID = 0, $emp_getID = 0, $basic = 0) {

		if(($emp_getID != null && $emp_getID != 0) && ($basic != null && $basic != 0)){
			
			$date1 = date('Y-m-d', strtotime('july '. (date("n") >= 7 ? 'this' : 'last' ) .' year'));
			$intial_month = date('m', strtotime('july '. (date("n") >= 7 ? 'this' : 'last' ) .' year'));
			$intial_year  = date('Y', strtotime('july '. (date("n") >= 7 ? 'this' : 'last' ) .' year'));
			for ($i=1; $i <= 11; $i++) { 
				if ($i == 1) {
					$count_month[] = $this->attandance->getEmployeeNetSalary($emp_getID, $intial_month, $intial_year);
				}else{
					$loop_month    = date('m', strtotime("+".$i." months", strtotime($date1)));
					$loop_year     = date('Y', strtotime("+".$i." months", strtotime($date1)));
					$count_month[] = $this->attandance->getEmployeeNetSalary($emp_getID, $loop_month, $loop_year);
				}
			}
			$emp_salary = $this->employee->getEmployeesalary($emp_getID);
			$havePayroll = 1;
			for ($j=0; $j <= count($count_month); $j++) {
				if ($count_month[$j] != null) {
					$havePayroll++;
					$count_net[] 		= $count_month[$j]['payroll_earning_basic'];
					$count_tax[] 		= !empty($count_month[$j]['payroll_deduction_tax']) ? $count_month[$j]['payroll_deduction_tax'] : 0;
					$count_medical[] 	= ((10 / 100) * ($count_month[$j]['payroll_earning_basic'] / 165) * 100);
				}else{
					$count_net[] 		= $emp_salary[0]['emp_basic_salary'];
					$count_medical[] 	= ((10 / 100) * (($emp_salary[0]['emp_basic_salary'] / 165) * 100));
				}
			}
			$net_earning	= array_sum($count_net) - array_sum($count_medical);
			$percentage 	= "";
			if ($net_earning > 600001 && $net_earning < 1200000) {
				$amount = $net_earning - 600000;
				$percentage = ((5 / 100) * $amount);
			}
			else if ($net_earning > 1200001 && $net_earning < 1800000) {
				$amount = ($net_earning - 1200000);
				$percentage = ((10 / 100) * $amount ) + 30000;
			}
			else if ($net_earning > 1800001 && $net_earning < 2500000) {
				$amount = ($net_earning - 1800000);
				$percentage = ((15 / 100) * $amount ) + 90000;
			}
			else if ($net_earning > 2500001 && $net_earning < 3500000) {
				$amount = ($net_earning - 2500000);
				$percentage = ((17.5 / 100) * $amount ) + 195000;
			}
			else if ($net_earning > 3500001 && $net_earning < 5000000) {
				$amount = ($net_earning - 3500000);
				$percentage = ((20 / 100) * $amount )  + 370000;
			}
			else if ($net_earning > 5000001 && $net_earning < 8000000) {
				$amount = ($net_earning - 5000000);
				$percentage = ((22.5 / 100) * $amount ) + 670000;
			}
			else if ($net_earning > 8000001 && $net_earning < 12000000) {
				$amount = ($net_earning - 8000000);
				$percentage = ((25 / 100) * $amount ) + 1345000;
			}
			else if ($net_earning > 12000001 && $net_earning < 30000000 ) {
				$amount = ($net_earning - 12000000);
				$percentage = ((27.5 / 100) * $amount ) + 2345000;
			}
			else if ($net_earning > 30000001 && $net_earning < 50000000 ) {
				$amount = ($net_earning - 30000000);
				$percentage = ((30 / 100) * $amount ) + 7295000;
			}
			else if ($net_earning > 50000001 && $net_earning <  75000000 ) {
				$amount = ($net_earning - 50000000);
				$percentage = ((32.5 / 100) * $amount ) + 13295000;
			}
			else if ($net_earning > 75000001) {
				$amount = ($net_earning - 75000000);
				$percentage = ((35 / 100) * $amount ) + 21420000;
			}
			if($count_tax){
				$final_tax 		= $percentage - array_sum($count_tax);
				$permonth_tax 	= $final_tax / (12 - $havePayroll);
				print_r((int)$permonth_tax);exit;
			}else{
				$permonth_tax 	= $percentage / 12;
				print_r((int)$permonth_tax);exit;
			}

			// old code
			
			// $emp_data 	= $this->attandance->getEmployeetax($emp_getID);
			// print_r(json_encode($emp_data[0]));exit;
		}
		if($emp_getID != null && $emp_getID != 0 ){
			$emp_salary = $this->employee->getEmployeesalary($emp_getID);
			print_r($emp_salary[0]['emp_basic_salary']);exit;
		}
		$emptaxvalue 	= $this->input->post("payrollTax");
		$basicIcome 	= $this->input->post("payrollBasic");
		$netSalary 		= $this->input->post("payrollNetSalary");
		#################################### FORM VALIDATION START ####################################		
			
		$this->form_validation->set_rules('empID', 		  'Employee', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('payrollMonth', 'Month', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('payrollYear',  'Year', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('payrollBasic', 'Basic Salary', 'trim|required|numeric|xss_clean');
		
		#################################### FORM VALIDATION END ######################################
		if ($this->form_validation->run() == true) {	
			$data_emp 				= [];
			$fetch_timing 			= $this->model_attendance_management->fetch_timing();
			$month_leave			= $this->input->post("payrollMonth") < 10 ? '0'.$this->input->post("payrollMonth") : $this->input->post("payrollMonth");
			$CheckIfLeave 			= $this->model_attendance_management->checkIfLeave($this->input->post("empID"),$month_leave,$this->input->post("payrollYear"));
			$fetch_month_timing 	= $this->model_attendance_management->fetch_month_timing($this->input->post("empID"),$month_leave,$this->input->post("payrollYear"));
			$count_attendance 		= count($fetch_month_timing);
			$total_days_after_leave = $count_attendance - $CheckIfLeave;
			
			// $count 					= 0;
			// $count_deduction 		= [];
			
			// $arrGraveWhere 			= array('config_key' => 'GRACE_TIME');
			// $arrLateWhere  			= array('config_key' => 'LATE_AMOUNT');
			// $arrCheckWhere 			= array('config_key' => 'CHECK_TIME');

			// $get_graceTime 			= $this->model_system->getSettings($arrGraveWhere);
			// $get_lateAmount 			= $this->model_system->getSettings($arrLateWhere);
			// $get_checkTime 			= $this->model_system->getSettings($arrCheckWhere);

			// print_r(json_encode($fetch_month_timing));exit;
			// foreach ($fetch_month_timing as $key => $value) {
			// 	if($value['att_in'] != '00:00:00'){
			// 		$ThatTime = $value['att_in'];
			// 		$thistime = $get_checkTime[0]['config_value'];
			// 		$minuteslate = abs(strtotime($thistime) - strtotime($ThatTime)) / 60;
					
			// 		$strLeave = getIfHalfLeaveApplied($this->input->post("empID"), $value['att_date']);
			// 		print_r(json_encode($strLeave));exit;

			// 		if($minuteslate > $get_graceTime[0]['config_value']){
			// 			$total = ($minuteslate - $get_graceTime[0]['config_value']) * $get_lateAmount[0]['config_value'];
			// 			$count_deduction[$count] = $total;
			// 		}
			// 		$count++;
			// 	}
			// }
			// $total_deduction = array_sum($count_deduction);

			// if($this->input->post("payrollMonth") <= $this->payrollCloseMonth && $this->input->post("payrollYear") <= $this->payrollCloseYear) {
			// 	$this->session->set_flashdata('error_message', 'Payroll For ' . date('F', mktime(0, 0, 0, $this->input->post("payrollMonth"), 10)) . ' ' . $this->input->post("payrollYear") . ' Has Already Been Closed For Changes');
			// 	redirect($this->baseURL . '/' . $this->currentController . '/list_payroll');
			// }
			// $arrWhere = array(
			// 				  'ep.emp_id' => $this->input->post("empID"),
			// 				  'ep.payroll_month' => $this->input->post("payrollMonth"),
			// 				  'ep.payroll_year' => $this->input->post("payrollYear")
			// 				  );
							  
			// $arrRecords = $this->payroll->getPayrolls($arrWhere);
			// //PAYROLL TAX VALUE HAS BEEN ADDED TO A VARIABLE INSTEAD OF AN ARRAY

			$emp_id 			= $this->input->post("empID");
			$emp_salary 		= $this->input->post("payrollBasic");
			$month_d 			= strlen($this->input->post("payrollMonth")) > 1 ? $this->input->post("payrollMonth") : "0".$this->input->post("payrollMonth");
				
			$date_payroll  		= $this->input->post("payrollYear")."-".$month_d."-01";
			$newdate 			= date("m", strtotime ( '-1 month', strtotime ( $date_payroll ) ));

			$count 				= 0;
			$deduction_late_coming;
			$late_count 		= 0;
			$workDays 			= 0;
			$absent 			= 0;
			$newJoining 		= 0;
			$leave 				= 0;
			$Halfleave 			= 0;
			$holidays 			= 0;
			$WFH 				= 0;
			$days_intervals  	= 0;
			$workHours;

			$date  				= "between '". $this->input->post("payrollYear")."-".$newdate ."-26 ' and '".$this->input->post("payrollYear")."-".$month_d ."-25 '";
			$Start_date 		= explode('-', $date_payroll);
			$get_month  		= $Start_date[1];
			$get_months 		= $get_month-1;
			$set_month 			= str_pad($get_months, 2, '0', STR_PAD_LEFT); ;
			$set_year 			= $Start_date[0];
			if($get_months == '13'){
				$set_year 		= $Start_date[0] + 1;
				$set_month 		= '01';
			}elseif ($get_months == '01') {
				$set_year 		= $Start_date[0] - 1;
				$set_month 		= $get_months;
			}
			$grobal_month 		= $set_year.'-'.$set_month.'-26';
			// print_r($grobal_month);exit;
			$grobal_months 		= new DateTime($grobal_month);

			$end 				= $set_year.'-'.$get_month.'-26';
			$end_month 			= new DateTime($end);

			$days_interval 		= $grobal_months->diff($end_month)->days;

			$start_month  		= $set_year.'-'.$set_month.'-26';
			$start 				= new DateTime($start_month);
			
			$end_emp  			= $set_year.'-'.$get_month.'-26';
			$end_emp_month 		= new DateTime($end_emp);
			$interval 			= new DateInterval('P1D');
			$period 			= new DatePeriod($start, $interval, $end_emp_month);
			
			$count_deduction 	= [];
			$count_mints 		= [];
			
			$arrGraveWhere 			= array('config_key' => 'GRACE_TIME');
			$arrLateWhere  			= array('config_key' => 'LATE_AMOUNT');
			$arrCheckWhere 			= array('config_key' => 'CHECK_TIME');
			
			$get_graceTime 			= $this->model_system->getSettings($arrGraveWhere);
			$get_lateAmount 		= $this->model_system->getSettings($arrLateWhere);
			$get_checkTime 			= $this->model_system->getSettings($arrCheckWhere);
			$branchTiming 			= $this->employee->headBranchHaveBranch($emp_id);
			$difference = round(abs(strtotime($branchTiming[0]['time_out']) - strtotime($branchTiming[0]['time_in']) ) /3600,2 );
			$hours_pay = (int)$netSalary / $days_interval / $difference / 60;
			foreach ($period as $dt)
			{
				if ($dt->format('N') != 7)
				{
					$daily_attendance = $this->attandance->fetch_daily_attendance($emp_id, $dt->format('Y-m-d'));
					if((empty($daily_attendance) || $daily_attendance['att_in'] == '00:00:00' && $daily_attendance['att_out'] == '00:00:00') && empty(getIfLeaveApplied($emp_id, $dt->format('Y-m-d'))) && empty(getIfHalfLeaveApplied($emp_id, $dt->format('Y-m-d'))) && empty(getHolidayLeave($dt->format('Y-m-d')) ) )
					{
						$absent++;
					}else if(!empty(getIfLeaveApplied($emp_id, $dt->format('Y-m-d')))) 
					{
						$leave++;
					}else if(!empty(getHolidayLeave($dt->format('Y-m-d')))) 
					{
						$holidays++;
					}else if(!empty(getIfHalfLeaveApplied($emp_id, $dt->format('Y-m-d')))) 
					{
						$ThatTime = $daily_attendance['att_in'];
						$CHECKOUT = $daily_attendance['att_out'];
						$time_in = $branchTiming[0]['time_in'];
						$minuteslate = abs(strtotime($time_in) - strtotime($ThatTime)) / 60;

						if($ThatTime > $time_in && $CHECKOUT < "13:30:00"){
							$total = $hours_pay + ($minuteslate) * 5;
							$count_deduction[$count] = $total;
							$count_mints[$count] = $minuteslate;
						}
						$count++;
						$Halfleave++;
					}else {
						$ThatTime = $daily_attendance['att_in'];
						$time_in = $branchTiming[0]['time_in'];
						$minuteslate = abs(strtotime($time_in) - strtotime($ThatTime)) / 60;
						if($ThatTime > $branchTiming[0]['time_in']){
							$total = $hours_pay + ($minuteslate) * 5;
							$count_deduction[$count] = $total;
							$count_mints[$count] 	= $minuteslate;

						}
						$count++;
						$workDays++;
					}
				}else{
					$next = date('d', strtotime('+1 day', strtotime( $dt->format('Y-m-d') ) ) );
					$prev = date('d', strtotime('-1 day', strtotime( $dt->format('Y-m-d') ) ) );
					$next_attendance = $this->attandance->fetch_daily_attendance($emp_id, date('Y-m-d', strtotime('+1 day', strtotime( $dt->format('Y-m-d') ))) );
					$prev_attendance = $this->attandance->fetch_daily_attendance($emp_id, date('Y-m-d', strtotime('-1 day', strtotime( $dt->format('Y-m-d')))) );
					if ($next != 26 || $prev != 24) {
						if (($next_attendance['att_in'] == '00:00:00' && $next_attendance['att_out'] == '00:00:00' ) && ($prev_attendance['att_in'] == '00:00:00' && $prev_attendance['att_out'] == '00:00:00') && empty(getIfLeaveApplied($empID, $dt->format('Y-m-d'))) && empty(getHolidayLeave($dt->format('Y-m-d')))) {
							$absent++;
						}
					}
				}
				
			}
			
			$total_lates 		= count($count_deduction);
			$total_deduction 	= array_sum($count_deduction);
			$total_late_hours 	= array_sum($count_mints);
			if ($workDays) {
				$workHours = $workDays * (abs($to_time - $from_time) / 3600);
			}else if ($Halfleave) {
				$workHours = $Halfleave * (abs($to_time - $from_time) / 3600) / 2;
			}
			$final_late=0;
			$basic_salaries;
			$end_basic_salaries;
			$deduction_absent 	= 0;
			$netSalary 			= $this->input->post("payrollNetSalary");
			if ($absent) {
				$deduction_absent = $netSalary / $days_interval;
				$days_interval = $days_interval - $absent;
			}elseif($leave){
				$days_interval = $days_interval - $leave;
			}elseif($Halfleave){
				$days_interval = $days_interval - $Halfleave;
			}elseif($holidays){
				$days_interval = $days_interval - $holidays;
			}
			$leave_absent = $absent;
			$totalSalary  = $basicIcome - $netSalary;
			
			$arrValues = array(
							'emp_id' 							=> $this->input->post("empID"),
							'payroll_month' 					=> $this->input->post("payrollMonth"),
							'payroll_year' 						=> $this->input->post("payrollYear"),
							'payroll_company_id' 				=> $this->userCompanyID,
							'payroll_earning_basic' 			=> $basicIcome, //$this->input->post("payrollBasic")
							'payroll_earning_housing' 			=> $this->input->post("payrollHousing"),
							'payroll_earning_transport' 		=> $this->input->post("payrollTransport"),
							'payroll_earning_utility'	 		=> $this->input->post("payrollUtility"),
							'payroll_earning_travel' 			=> $this->input->post("payrollTravel"),
							'payroll_earning_health' 			=> $this->input->post("payrollHealth"),
							'payroll_earning_fuel' 				=> $this->input->post("payrollFuel"),
							'payroll_earning_mobile' 			=> $this->input->post("payrollMobile"),
							'payroll_earning_medical_relief' 	=> $this->input->post("payrollMedical"),
							'net_salary' 						=> $totalSalary,
							'payroll_earning_bonus' 			=> $this->input->post("payrollBonus"),
							'payroll_earning_annual_leave_encashment' => $this->input->post("payrollLeaveEnc"),
							'payroll_earning_claims' 			=> $this->input->post("payrollClaim"),
							'payroll_earning_commission' 		=> $this->input->post("payrollCommission"),
							'payroll_earning_annual_ticket' 	=> $this->input->post("payrollAnnualTicket"),
							'payroll_earning_gratuity' 			=> $this->input->post("payrollGratuity"),
							'payroll_earning_survey_expense' 	=> $this->input->post("payrollSurveyExpense"),
							'payroll_earning_settlement' 		=> $this->input->post("payrollSettlement"),
							'payroll_earning_misc' 				=> $this->input->post("payrollEarningMisc"),
							'payroll_earning_food_allowance' 	=> $this->input->post("payrollFoodAllowance"),
							// 'payroll_deduction_tax' => ($emptaxvalue * $basicIcome / 100) , //$this->input->post("payrollTax"), changed this from post to a variable;
							'payroll_deduction_tax' 			=> $emptaxvalue, //$this->input->post("payrollTax"), changed this from post to a variable;
							'payroll_deduction_pf' 				=> $this->input->post("payrollPF"),
							'payroll_deduction_loan' 			=> $this->input->post("payrollLoan"),
							'payroll_deduction_eobi' 			=> $this->input->post("payrollEOBI"),
							'payroll_deduction_telephone' 		=> $this->input->post("payrollDeductionTelephone"),
							'payroll_deduction_misc' 			=> $this->input->post("payrollDeductionMisc"),
							'payroll_deduction_mobile' 			=> $this->input->post("payrollDeductionMobile"),
							'payroll_deduction_jeans' 			=> $this->input->post("payrollDeductionJeans"),
							'payroll_deduction_smoking' 		=> $this->input->post("payrollDeductionSmoking"),
							'payroll_deduction_wrong_time' 		=> $this->input->post("payrollDeductionWrongTime"),
							'payroll_deduction_cashier_checking' => $this->input->post("payrollDeductionCashierChecking"),
							'payroll_deduction_guard_attention' => $this->input->post("payrollDeductionGuardAttention"),
							//'payrollDeductionDVROff' => $this->input->post("payroll_deduction_dvr_off"),
							'payroll_deduction_dvr_off' 		=> $this->input->post("payrollDeductionDVROff"),
							'payroll_deduction_laptop_in_branch' => $this->input->post("payrollDeductionLaptopInBranch"),
							'payroll_deduction_no_response' 	=> $this->input->post("payrollDeductionNoResponse"),
							'payroll_deduction_branch_incharge' => $this->input->post("payrollDeductionBranchIncharge"),
							'payroll_deduction_dealing_without' => $this->input->post("payrollDeductionDealingWithout"),
							'payroll_deduction_misbehave' 		=> $this->input->post("payrollDeductionMisbehave"),
							'payroll_deduction_open_close' 		=> $this->input->post("payrollDeductionOpenClose"),
							'payroll_deduction_pending_work' 	=> $this->input->post("payrollDeductionPendingWork"),
							'payroll_deductsion_MMB_loan' 		=> $this->input->post("payrollDeductionMMBLoan"),
							'payroll_deduction_late_coming' 	=> $total_deduction,
							'payroll_deduction_absent' 			=> $deduction_absent,
							'total_absent' 						=> $absent,
							'total_late' 						=> $total_lates,
							'total_tranfer' 					=> '',
							'total_late_hours' 					=> $total_late_hours,
							'total_leave' 						=> $leave,
							'total_holidays' 					=> $holidays,
							'total_workDays' 					=> $days_interval,
							'modified_by' 						=> $this->userEmpNum,
							'modified_date' 					=> date($this->arrData["dateTimeFormat"])
						);
			if(count($arrRecords)) {
				$deduction_monthly = $this->deduction->getEMPpayrollMonth($emp_id, $month_d, $this->input->post("payrollYear"));
				$compliance_monthly = $this->compliance->getEMPpayrollMonth($emp_id, $month_d, $this->input->post("payrollYear"));

				$arrValues['modified_by'] = $this->userEmpNum;
				$arrValues['modified_date'] = date($this->arrData["dateTimeFormat"]);
				$arrValues['cctc_approval_deduction'] = $deduction_monthly;
				$arrValues['compliance_approval_deduction'] = $compliance_monthly;
				$this->payroll->saveValues(TABLE_EMPLOYEE_PAYROLL, $arrValues, $arrWhere);
			}else{
				$deduction_monthly = $this->deduction->getEMPpayrollMonth($emp_id, $month_d, $this->input->post("payrollYear"));
				$compliance_monthly = $this->compliance->getEMPpayrollMonth($emp_id, $month_d, $this->input->post("payrollYear"));

				$arrValues['created_by'] = $this->userEmpNum;
				$arrValues['created_date'] = date($this->arrData["dateTimeFormat"]);
				$arrValues['cctc_approval_deduction'] = $deduction_monthly;
				$arrValues['compliance_approval_deduction'] = $compliance_monthly;
				$this->payroll->saveValues(TABLE_EMPLOYEE_PAYROLL, $arrValues);
			}
						
			$this->session->set_flashdata('success_message', 'Payroll Details Saved Successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/list_payroll');
			
		} else {
			$this->arrData['validation_error_message'] = validation_errors();
		}
		# CODE FOR CURRENT EMPLOYEE RECORD
		if($payrollID) {
			$this->arrData['record'] = $this->payroll->getPayrolls(array('ep.payroll_id' => $payrollID));
			$this->arrData['record'] = $this->arrData['record'][0];
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$arrEmployee = $this->payroll->getEmployeesPayroll(
															  		array(
																	  	'e.emp_id' => $this->arrData['record']['emp_id'],
																	  	'p.payroll_id' => $payrollID,
																	 	'p.payroll_company_id' => $this->userCompanyID
																  	)
															  	);
				if(!count($arrEmployee)) {
					redirect(base_url() . 'message/access_denied');
					exit;
				}
			}
		}
		if(!isAdmin($this->userRoleID)) {
			$arrWhereSupervisors = array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE);
			$arrWhereEmployees = array('es.supervisor_emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE);
			
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$arrWhereSupervisors['e.emp_company_id'] = $this->userCompanyID;
				$arrWhereEmployees['e.emp_company_id'] = $this->userCompanyID;
			}
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE));
		} else {
			$this->arrData['arrSupervisors'] = $this->employee->getSupervisors();
			
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$this->arrData['arrEmployees'] = $this->payroll->getEmployeesPayroll(array('p.payroll_company_id' => $this->userCompanyID));
			} else {
				$this->arrData['arrEmployees'] = $this->employee->getEmployees(array());
			}
			
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		}		
		
		$finalResult = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrEmployees'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}
		}
		$this->arrData["arrEmployees"] = $finalResult;
		// print_r($finalResult);
		// 		exit;
		# CODE FOR PAGE CONTENT
		$this->arrData['arrMonths'] = $this->config->item('months');
		$this->arrData['payrollID'] = $payrollID;
		
		//print_r($arrMonths);
		# TEMPLATE LOADING
		$this->template->write_view('content', 'payroll_management/save_payroll', $this->arrData);
		$this->template->render();
	}
	
	public function list_payroll($pageNum = 1) {
	
		# CODE FOR DELETING RECORD
		if($this->input->post("record_id")) {
			if($this->arrData['canDelete'] == YES) {	
				
				$payrollRecord = $this->payroll->getPayrolls(array('ep.payroll_id' => (int)$this->input->post("record_id")));
				if($payrollRecord[0]['payroll_month'] <= $this->payrollCloseMonth && $payrollRecord[0]['payroll_year'] <= $this->payrollCloseYear) {
					echo NO; exit;
				}
				
				if(!$this->payroll->deleteValue(TABLE_EMPLOYEE_PAYROLL, null, array('payroll_id' => (int)$this->input->post("record_id")))) {
					echo NO; exit;
				} else {
					echo YES; exit;
				}								
			}
		}
		##########################
		
		if((int)$pageNum <= 0) {
			$pageNum = 1;
		}
		$arrWhere = array();
		if ($this->input->post()) {
				// $query_and_chr = false; 	
				// if($this->input->post("empID")) {
				// 	if($query_and_chr == false){
				// 		$empID 			= $this->input->post("empID");
				// 		$arrWhere['empID'] = " e.emp_id = ".$empID;
				// 		$query_and_chr = true;
				// 	}else{
				// 		$empID 			= $this->input->post("empID");
				// 		$arrWhere['empID'] = "AND e.emp_id = ".$empID;
				// 		$query_and_chr = true;
				// 	}
				// }
				// if($this->input->post("selRegion")) {
				// 	if($query_and_chr == false){
				// 		$selRegion 			= $this->input->post("selRegion");
				// 		$arrWhere['selRegion'] = " e.emp_region_id = ".$selRegion;
				// 		$query_and_chr = true;
				// 	}else{
				// 		$selRegion 			= $this->input->post("selRegion");
				// 		$arrWhere['selRegion'] = "AND e.emp_region_id = ".$selRegion;
				// 		$query_and_chr = true;
				// 	}
				// }
				// if($this->input->post("empCompany")) {
				// 	if($query_and_chr == false){
				// 		$empCompany 			= $this->input->post("empCompany");
				// 		$arrWhere['empCompany'] = " ep.payroll_company_id = ".$empCompany;
				// 		$query_and_chr = true;
				// 	}else{
				// 		$empCompany 			= $this->input->post("empCompany");
				// 		$arrWhere['empCompany'] = "AND ep.payroll_company_id = ".$empCompany;
				// 		$query_and_chr = true;
				// 	}
				// }
				// if($this->input->post("empSupervisor")) {
				// 	$strHierarchy = $this->employee->getHierarchyWithMultipleAuthorities($this->input->post("empSupervisor"));
				// 	if($query_and_chr == false){
				// 		// print_r($strHierarchy);exit;
				// 		$arrWhere['empSupervisor'] = "es.supervisor_emp_id in ". '(' . $strHierarchy . ')' ;
				// 		$empSupervisor 			= $this->input->post("empSupervisor");
				// 		// $arrWhere['empSupervisor'] = " es.supervisor_emp_id = ".$empSupervisor;
				// 		$query_and_chr = true;
				// 	}else{
						
				// 		$arrWhere['empSupervisor'] = "AND es.supervisor_emp_id in ".'(' . $strHierarchy . ')';
				// 		$empSupervisor 			= $this->input->post("empSupervisor");
				// 		// $arrWhere['empSupervisor'] = "AND es.supervisor_emp_id = ".$empSupervisor;
				// 		$query_and_chr = true;
				// 	}
				// }
				// if($this->input->post("selMonth")) {
				// 	if($query_and_chr == false){
				// 		$selMonth 			= $this->input->post("selMonth");
				// 		$arrWhere['selMonth'] = " ep.payroll_month = ".$selMonth;
				// 		$query_and_chr = true;
				// 	}else{
				// 		$selMonth 			= $this->input->post("selMonth");
				// 		$arrWhere['selMonth'] = "AND ep.payroll_month = ".$selMonth;
				// 		$query_and_chr = true;
				// 	}
				// }
				// if($this->input->post("selYear")) {
				// 	if($query_and_chr == false){
				// 		$selYear 			= $this->input->post("selYear");
				// 		$arrWhere['selYear'] = " ep.payroll_year = ".$selYear;
				// 		$query_and_chr = true;
				// 	}else{
				// 		$selYear 			= $this->input->post("selYear");
				// 		$arrWhere['selYear'] = "AND ep.payroll_year = ".$selYear;
				// 		$query_and_chr = true;
				// 	}
				// }
			
			if($this->input->post("empID")) {
				$arrWhere['e.emp_id'] = $this->input->post("empID");
				$this->arrData['empID'] = $this->input->post("empID");
			}

			if($this->input->post("selRegion")) {
				$arrWhere['e.emp_region_id'] = $this->input->post("selRegion");
				$this->arrData['selRegion'] = $this->input->post("selRegion");
			}
			
			if($this->input->post("empCompany")) {
				$arrWhere['ep.payroll_company_id'] = $this->input->post("empCompany");
				$this->arrData['empCompany'] = $this->input->post("empCompany");
			}
			
			if($this->input->post("empSupervisor")) {
				$strHierarchy = $this->employee->getHierarchyWithMultipleAuthorities($this->input->post("empSupervisor"));
				$arrWhere['es.supervisor_emp_id in '] = '(' . $strHierarchy . ')';
				$this->arrData['empSupervisor'] = $this->input->post("empSupervisor");
			}
			
			if($this->input->post("selMonth")) {
				$arrWhere['ep.payroll_month'] = $this->input->post("selMonth");
				$this->arrData['selMonth'] = $this->input->post("selMonth");
			}
			
			if($this->input->post("selYear")) {
				$arrWhere['ep.payroll_year'] = $this->input->post("selYear");
				$this->arrData['selYear'] = $this->input->post("selYear");
			}
			
		} else {
			if(!isAdmin($this->userRoleID)) {
				$arrWhere['e.emp_employment_status <= '] = STATUS_EMPLOYEE_ONNOTICEPERIOD;
				$this->arrData['empStatus'] = 'active';
			}
		}
			
		if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
			$arrWhere['ep.payroll_company_id'] = $this->userCompanyID;
			$this->arrData['empCompany'] = $this->userCompanyID;
		}
		
		if(!isAdmin($this->userRoleID)) {
			$arrWhere['e.emp_status'] = STATUS_ACTIVE;
		}
		
		if($this->input->post("empCompany") && $this->input->post("selMonth") && $this->input->post("selYear")) {
			
			$arrAllRecords 		= $this->payroll->getPayrolls($arrWhere);
			$totalEarning 		= 0;
			$totalDeduction 	= 0;
			$totalEarnings 		= 0;
			$totalDeductions 	= 0;
			$totalNetSalary 	= 0;
			
			for($ind = 0; $ind < count($arrAllRecords); $ind++) {
				$totalEarning = (int)$arrAllRecords[$ind]['net_salary'] + 
							(int)$arrAllRecords[$ind]['payroll_earning_housing'] +
							(int)$arrAllRecords[$ind]['payroll_earning_transport'] +
							(int)$arrAllRecords[$ind]['payroll_earning_utility'] +
							(int)$arrAllRecords[$ind]['payroll_earning_travel'] +
							(int)$arrAllRecords[$ind]['payroll_earning_health'] +
							(int)$arrAllRecords[$ind]['payroll_earning_fuel'] +
							(int)$arrAllRecords[$ind]['payroll_earning_mobile'] +
							(int)$arrAllRecords[$ind]['payroll_earning_medical_relief'] +
							(int)$arrAllRecords[$ind]['payroll_earning_bonus'] +
							(int)$arrAllRecords[$ind]['payroll_earning_annual_leave_encashment'] +
							(int)$arrAllRecords[$ind]['payroll_earning_claims'] +
							(int)$arrAllRecords[$ind]['payroll_earning_commission'] +
							(int)$arrAllRecords[$ind]['payroll_earning_annual_ticket'] +
							(int)$arrAllRecords[$ind]['payroll_earning_gratuity'] +
							(int)$arrAllRecords[$ind]['payroll_earning_survey_expense'] +
							(int)$arrAllRecords[$ind]['payroll_earning_settlement'] +
							(int)$arrAllRecords[$ind]['payroll_earning_misc'] +
							(int)$arrAllRecords[$ind]['payroll_earning_food_allowance'];
				
				$totalDeduction = (int)$arrAllRecords[$ind]['payroll_deduction_tax'] + 
								(int)$arrAllRecords[$ind]['payroll_deduction_pf'] + 
								(int)$arrAllRecords[$ind]['payroll_deduction_loan'] + 
								(int)$arrAllRecords[$ind]['payroll_deduction_eobi'] + 
								(int)$arrAllRecords[$ind]['payroll_deduction_telephone'] + 
								(int)$arrAllRecords[$ind]['payroll_deduction_misc'] +
								(int)$arrAllRecords[$ind]['payroll_deduction_mobile'] +
								(int)$arrAllRecords[$ind]['payroll_deduction_jeans'] +
								(int)$arrAllRecords[$ind]['payroll_deduction_smoking'] +
								(int)$arrAllRecords[$ind]['payroll_deduction_wrong_time'] +
								(int)$arrAllRecords[$ind]['payroll_deduction_cashier_checking'] +
								(int)$arrAllRecords[$ind]['payroll_deduction_guard_attention'] +
								(int)$arrAllRecords[$ind]['payroll_deduction_dvr_off'] +
								(int)$arrAllRecords[$ind]['payroll_deduction_laptop_in_branch'] +
								(int)$arrAllRecords[$ind]['payroll_deduction_no_response'] +
								(int)$arrAllRecords[$ind]['payroll_deduction_branch_incharge'] +
								(int)$arrAllRecords[$ind]['payroll_deduction_CCFLA'] +
								(int)$arrAllRecords[$ind]['payroll_deduction_dealing_without'] +
								(int)$arrAllRecords[$ind]['payroll_deduction_misbehave'] +
								(int)$arrAllRecords[$ind]['payroll_deduction_open_close'] +
								(int)$arrAllRecords[$ind]['payroll_deduction_late_coming'] + 
								(int)$arrAllRecords[$ind]['payroll_deduction_pending_work'] +
								(int)$arrAllRecords[$ind]['cctc_approval_deduction'] +
								(int)$arrAllRecords[$ind]['compliance_approval_deduction'] +
								(int)$arrAllRecords[$ind]['payroll_deductsion_MMB_loan'];

				$totalEarnings 		+= (int)$totalEarning;				 						
				$totalDeductions 	+= (int)$totalDeduction;						
				$totalNetSalary 	+= (int)($totalEarning - $totalDeduction);
			}
			
			$this->arrData['totalEarnings'] 	= $totalEarnings;
			$this->arrData['totalDeductions'] 	= $totalDeductions;
			$this->arrData['totalNetSalary'] 	= $totalNetSalary;
		}
		
		$this->arrData['totalRecordsCount'] 	= count($this->payroll->getTotalPayrolls($arrWhere));
		$offSet 								= ($pageNum - 1) * $this->limitRecords;
		$this->arrData['arrRecords'] 			= $this->payroll->getPayrolls($arrWhere, $this->limitRecords, $offSet);
		$numPages 								= ceil($this->arrData['totalRecordsCount'] / $this->limitRecords);
		$this->arrData['pageLinks'] 			= displayLinksFrm($numPages, $this->maxLinks, $pageNum, $this->currentController . '/' . $this->currentAction . '/#', 'frmListPayroll');
		# CODE FOR PAGE CONTENT
		if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
			$this->arrData['arrCompanies'] 		= $this->configuration->getCompanies(array('company_id' => $this->userCompanyID));
		} else {
			$this->arrData['arrCompanies'] 		= $this->configuration->getCompanies();
		}
		$this->arrData['empDepartments'] 		= $this->configuration->getValues(TABLE_JOB_CATEGORY, '*', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		if(!isAdmin($this->userRoleID)) {
			$arrWhereSupervisors 				= array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE);
			$arrWhereEmployees 					= array('es.supervisor_emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE);
			
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$arrWhereSupervisors['e.emp_company_id'] = $this->userCompanyID;
				$arrWhereEmployees['e.emp_company_id'] = $this->userCompanyID;
			}
			$this->arrData['arrSupervisors'] 	= $this->employee->getEmployees($arrWhereSupervisors);
			$this->arrData['arrEmployees'] 		= $this->employee->getEmployees($arrWhereEmployees);
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE));
		} else {
			$this->arrData['arrSupervisors'] 	= $this->employee->getSupervisors();
			
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$this->arrData['arrEmployees'] 	= $this->payroll->getEmployeesPayroll(array('p.payroll_company_id' => $this->userCompanyID));
			} else {
				$this->arrData['arrEmployees'] 	= $this->employee->getEmployees(array());
			}
			
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		}		
		
		$finalResult = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrEmployees'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}	
		}
		$this->arrData["arrEmployees"] = $finalResult;
		
		$finalResultSupervisors = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResultSupervisors[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrSupervisors'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResultSupervisors[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResultSupervisors[$arrJobCategories[$i]['job_category_name']]);
			}
		}		
		$this->arrData["arrSupervisors"] 		= $finalResultSupervisors;
		
		$this->arrData['arrEmploymentStatuses'] = $this->employee->populateEmploymentStatus();
		$this->arrData['arrRegions'] 			= $this->employee->getRegions();
		$this->arrData['arrBanks'] 				= $this->configuration->getBanks();
		$this->arrData['arrMonths'] 			= $this->config->item('months');
		$this->arrData['arrLocations'] 			= $this->configuration->getLocations();
		$this->arrData['frmActionURL'] 			= $this->baseURL . '/' . $this->currentController . '/' . $this->currentAction;
		// print_r($this->arrData['arrRecords']);exit;
		# TEMPLATE LOADING
		$this->template->write_view('content', 'payroll_management/list_payroll', $this->arrData);
		$this->template->render();
	}
	
	public function manage_payroll() {
		
		#################################### FORM VALIDATION START ####################################		
			
		$this->form_validation->set_rules('payrollMonth', 'Month', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('payrollYear', 'Year', 'trim|required|numeric|xss_clean');
		
		if($this->input->post("frmType") == 1) {
			$this->form_validation->set_rules('companyID', 'Company', 'trim|required|numeric|xss_clean');
		}
		
		#################################### FORM VALIDATION END ####################################
		
		if ($this->form_validation->run() == true) {
			
			$arrWhere = array(
							  'ep.emp_id' => $this->input->post("empID"),
							  'ep.payroll_month' => $this->input->post("payrollMonth"),
							  'ep.payroll_year' => $this->input->post("payrollYear")
							  );
							  
			if($this->input->post("frmType") == 1) {
				
				$arrCompanyWhere = array();
				$arrCompanyWhere['company_id'] = $this->input->post("companyID");				
				$arrCompanies = $this->configuration->getCompanies($arrCompanyWhere);
				
				$strCSV = "";
				
				for($ind = 0; $ind < count($arrCompanies); $ind++) {
					
					$totalBasic = 0;
					$totalHousing = 0;
					$totalTransport = 0;
					$totalUtility = 0;
					$totalTravel = 0;
					$totalHealth = 0;
					$totalFuel = 0;
					$totalMobile = 0;
					$totalMedical = 0;
					$totalBonus = 0;
					$totalAnnualLeaveEnc = 0;
					$totalClaims = 0;
					$totalAnnualTicket = 0;
					$totalCommission = 0;
					$totalSurveyExpense = 0;
					$totalGratuity = 0;
					$totalSettlement = 0;
					$totalEarningMisc = 0;
					$totalEarnings = 0;
					$totalFoodAllowance = 0;
					$totalTax = 0;
					$totalTax = $totalTax / 100 * $totalBasic;
					$totalPF = 0;
					$totalLoan = 0;
					$totalEOBI = 0;
					$totalTelephone = 0;
					$MobileUse = 0;
					$JeansTshirt = 0;
					$Smoking = 0;
					$WrongTime = 0;
					$CashierChecking = 0;
					//$CashierChecking = 0;
					$GuardAttention = 0;
					//$GuardAttention = 0;
					$DVROff = 0;
					$LaptopInBranch = 0;
					$NoResponse = 0;
					$BranchIncharge = 0;
					$CCFLA = 0;
					$DealingWithout = 0;
					$Misbehave = 0;
					$OpenClose = 0;
					$PendingWork = 0;
					$lateComing = 0;
					$totalDeductionMisc = 0;
					$totalDeductions = 0;
					$totalDeductionsMMBLoan = 0;
					
					$totalNetSalary = 0;
					
					$strCSV .= $arrCompanies[$ind]['company_name'] . "\n";
					$strCSV .= date('F', mktime(0, 0, 0, $this->input->post("payrollMonth"), 10)) . ' ' . $this->input->post("payrollYear") . "\n";
					
					$arrEmpWhere = array(
										'e.emp_id > ' => STATUS_ACTIVE,
										//'e.emp_status' => STATUS_ACTIVE,
										//'e.emp_employment_status < ' => STATUS_EMPLOYEE_SEPARATED,
										'e.emp_company_id' => $arrCompanies[$ind]['company_id']
										);
										
					$arrEmployees = $this->employee->getEmployees($arrEmpWhere);
					
					for($jnd = 0; $jnd < count($arrEmployees); $jnd++) {
						
						$arrPayroll = $this->payroll->getPayrolls(
							array(
								'ep.emp_id' 		=> $arrEmployees[$jnd]['emp_id'],
								'ep.payroll_month' 	=> $this->input->post("payrollMonth"),
								'ep.payroll_year' 	=> $this->input->post("payrollYear")
							)
						);
	
						if(count($arrPayroll)) {
							$totalBasic 			+= (int)$arrPayroll[0]['payroll_earning_basic'];
							$totalHousing 			+= (int)$arrPayroll[0]['payroll_earning_housing'];
							$totalTransport 		+= (int)$arrPayroll[0]['payroll_earning_transport'];
							$totalClaims 			+= (int)$arrPayroll[0]['payroll_earning_claims'];
							$totalAnnualTicket 		+= (int)$arrPayroll[0]['payroll_earning_annual_ticket'];
							$totalCommission 		+= (int)$arrPayroll[0]['payroll_earning_commission'];
							$totalSurveyExpense 	+= (int)$arrPayroll[0]['payroll_earning_survey_expense'];						
							$totalUtility 			+= (int)$arrPayroll[0]['payroll_earning_utility'];
							$totalTravel 			+= (int)$arrPayroll[0]['payroll_earning_travel'];
							$totalHealth 			+= (int)$arrPayroll[0]['payroll_earning_health'];
							$totalFuel 				+= (int)$arrPayroll[0]['payroll_earning_fuel'];
							$totalMobile 			+= (int)$arrPayroll[0]['payroll_earning_mobile'];
							$totalMedical 			+= (int)$arrPayroll[0]['payroll_earning_medical_relief'];
							$totalBonus 			+= (int)$arrPayroll[0]['payroll_earning_bonus'];
							$totalAnnualLeaveEnc 	+= (int)$arrPayroll[0]['payroll_earning_annual_leave_encashment'];
							$totalGratuity			+= (int)$arrPayroll[0]['payroll_earning_gratuity'];
							$totalSettlement 		+= (int)$arrPayroll[0]['payroll_earning_settlement'];
							$totalEarningMisc 		+= (int)$arrPayroll[0]['payroll_earning_misc'];
							$totalFoodAllowance 	+= (int)$arrPayroll[0]['payroll_earning_food_allowance'];
							
							$totalTax 				+= (int)$arrPayroll[0]['payroll_deduction_tax'];
							$totalPF 				+= (int)$arrPayroll[0]['payroll_deduction_pf'];
							$totalLoan 				+= (int)$arrPayroll[0]['payroll_deduction_loan'];
							$totalEOBI 				+= (int)$arrPayroll[0]['payroll_deduction_eobi'];
							$totalTelephone 		+= (int)$arrPayroll[0]['payroll_deduction_telephone'];
							$MobileUse 				+= (int)$arrPayroll[0]['payroll_deduction_mobile'];
							$JeansTshirt 			+= (int)$arrPayroll[0]['payroll_deduction_jeans'];
							$Smoking 				+= (int)$arrPayroll[0]['payroll_deduction_smoking'];
							$WrongTime 				+= (int)$arrPayroll[0]['payroll_deduction_wrong_time'];
							$CashierChecking 		+= (int)$arrPayroll[0]['payroll_deduction_cashier_checking'];
							//$CashierChecking += 0;
							$GuardAttention 		+= (int)$arrPayroll[0]['payroll_deduction_guard_attention'];
							//$GuardAttention += 0;
							$DVROff 				+= (int)$arrPayroll[0]['payroll_deduction_dvr_off'];
							$LaptopInBranch 		+= (int)$arrPayroll[0]['payroll_deduction_laptop_in_branch'];
							$NoResponse 			+= (int)$arrPayroll[0]['payroll_deduction_no_response'];
							$BranchIncharge 		+= (int)$arrPayroll[0]['payroll_deduction_branch_incharge'];
							$CCFLA 					+= (int)$arrPayroll[0]['payroll_deduction_CCFLA'];
							$DealingWithout 		+= (int)$arrPayroll[0]['payroll_deduction_dealing_without'];
							$Misbehave 				+= (int)$arrPayroll[0]['payroll_deduction_misbehave'];
							$OpenClose 				+= (int)$arrPayroll[0]['payroll_deduction_open_close'];
							$PendingWork 			+= (int)$arrPayroll[0]['payroll_deduction_pending_work'];
							$lateComing 			+= (int)$arrPayroll[0]['payroll_deduction_late_coming'];
							$totalDeductionMisc 	+= (int)$arrPayroll[0]['payroll_deduction_misc'];
							$totalDeductionsMMBLoan += (int)$arrPayroll[0]['payroll_deductsion_MMB_loan'];
						}
					}
					
					$arrHeaderColumns = array(
						'ID',
						'Employee',
						'Department',
						'PayMode'
					);
					
					if($totalBasic) $arrHeaderColumns[] 			= 'Basic';
					if($totalHousing) $arrHeaderColumns[] 			= 'Housing';
					if($totalTransport) $arrHeaderColumns[] 		= 'Transport';
					if($totalClaims) $arrHeaderColumns[] 			= 'Claims';
					if($totalAnnualTicket) $arrHeaderColumns[] 		= 'Annual Ticket Allowance';
					if($totalCommission) $arrHeaderColumns[] 		= 'Commission';
					if($totalSurveyExpense) $arrHeaderColumns[] 	= 'Travel/Survey Expense';
					if($totalUtility) $arrHeaderColumns[] 			= 'Utility';
					
					if($totalTravel) $arrHeaderColumns[] 			= 'Travel';
					if($totalHealth) $arrHeaderColumns[] 			= 'Health';
					if($totalFuel) $arrHeaderColumns[] 				= 'Fuel';
					if($totalMobile) $arrHeaderColumns[] 			= 'Mobile';
					if($totalMedical) $arrHeaderColumns[] 			= 'Medical';
					if($totalBonus) $arrHeaderColumns[] 			= 'Bonus';
					if($totalAnnualLeaveEnc) $arrHeaderColumns[] 	= 'Annual Leave Encashment';
					if($totalGratuity) $arrHeaderColumns[] 			= 'Gratuity';
					if($totalSettlement) $arrHeaderColumns[] 		= 'Settlement';
					if($totalEarningMisc) $arrHeaderColumns[] 		= 'Other Earning';
					if($totalFoodAllowance) $arrHeaderColumns[] 	= 'Food Allowance';
					
					$arrHeaderColumns[] 							= 'Total Earning';
					
					if($totalTax) $arrHeaderColumns[] 				= 'Tax';
					if($totalPF) $arrHeaderColumns[] 				= 'Provident Fund';
					if($totalLoan) $arrHeaderColumns[] 				= 'Loan';
					if($totalEOBI) $arrHeaderColumns[] 				= 'EOBI';
					if($totalTelephone) $arrHeaderColumns[] 		= 'Telephone Expense';
					if($totalDeductionMisc) $arrHeaderColumns[] 	= 'Other Deduction';
					if($MobileUse) $arrHeaderColumns[] 				= 'Mobile Use';
					if($JeansTshirt) $arrHeaderColumns[] 			= 'Jeans/T-Shirt';
					if($Smoking) $arrHeaderColumns[] 				= 'Smoking';
					if($WrongTime) $arrHeaderColumns[] 				= 'Wrong Time';
					if($CashierChecking) $arrHeaderColumns[] 		= 'Cashier Checking';
					if($GuardAttention) $arrHeaderColumns[] 		= 'Guard Attention';
					if($DVROff) $arrHeaderColumns[] 				= 'DVR Off';
					if($LaptopInBranch) $arrHeaderColumns[] 		= 'Laptop In Branch';
					if($NoResponse) $arrHeaderColumns[] 			= 'No Response';
					if($BranchIncharge) $arrHeaderColumns[] 		= 'Branch Incharge';
					if($CCFLA) $arrHeaderColumns[] 					= 'CCFLA';
					if($DealingWithout) $arrHeaderColumns[] 		= 'Dealing Without';
					if($Misbehave) $arrHeaderColumns[] 				= 'Misbehave';
					if($OpenClose) $arrHeaderColumns[] 				= 'Open Close';
					if($PendingWork) $arrHeaderColumns[] 			= 'Pending Work';
					if($totalDeductionsMMBLoan) $arrHeaderColumns[] = 'MMB Loan';
					
					$arrHeaderColumns[] 							= 'Total Deduction';
					$arrHeaderColumns[] 							= 'Net Salary';
					
					$strCSV .= implode(',', $arrHeaderColumns) . "\n";
					
					for($jnd = 0; $jnd < count($arrEmployees); $jnd++) {
						$arrPayroll = $this->payroll->getPayrolls(
							array(
								'ep.emp_id' => $arrEmployees[$jnd]['emp_id'],
								'ep.payroll_month' => $this->input->post("payrollMonth"),
								'ep.payroll_year' => $this->input->post("payrollYear")
							)
						);
						if(count($arrPayroll)) {
							$totalEarning = (int)$arrPayroll[0]['payroll_earning_basic'] + 
											(int)$arrPayroll[0]['payroll_earning_housing'] +
											(int)$arrPayroll[0]['payroll_earning_transport'] +
											(int)$arrPayroll[0]['payroll_earning_utility'] +
											(int)$arrPayroll[0]['payroll_earning_travel'] +
											(int)$arrPayroll[0]['payroll_earning_health'] +
											(int)$arrPayroll[0]['payroll_earning_fuel'] +
											(int)$arrPayroll[0]['payroll_earning_mobile'] +
											(int)$arrPayroll[0]['payroll_earning_medical_relief'] +
											(int)$arrPayroll[0]['payroll_earning_bonus'] +
											(int)$arrPayroll[0]['payroll_earning_annual_leave_encashment'] +
											(int)$arrPayroll[0]['payroll_earning_claims'] +
											(int)$arrPayroll[0]['payroll_earning_commission'] +
											(int)$arrPayroll[0]['payroll_earning_annual_ticket'] +
											(int)$arrPayroll[0]['payroll_earning_gratuity'] +
											(int)$arrPayroll[0]['payroll_earning_survey_expense'] +
											(int)$arrPayroll[0]['payroll_earning_settlement'] +
											(int)$arrPayroll[0]['payroll_earning_misc'] +
											(int)$arrPayroll[0]['payroll_earning_food_allowance'];
											
							$totalDeduction = (int)$arrPayroll[0]['payroll_deduction_tax'] + // 100 * (int)$arrPayroll[0]['payroll_earning_basic'] + 
											  (int)$arrPayroll[0]['payroll_deduction_pf'] + 
											  (int)$arrPayroll[0]['payroll_deduction_loan'] + 
											  (int)$arrPayroll[0]['payroll_deduction_eobi'] + 
											  (int)$arrPayroll[0]['payroll_deduction_telephone'] + 
											  (int)$arrPayroll[0]['payroll_deduction_misc'] +
											  (int)$arrPayroll[0]['payroll_deduction_mobile'] +
											  (int)$arrPayroll[0]['payroll_deduction_jeans'] +
											  (int)$arrPayroll[0]['payroll_deduction_smoking'] +
											  (int)$arrPayroll[0]['payroll_deduction_wrong_time'] +
											  (int)$arrPayroll[0]['payroll_deduction_guard_attention'] +
											  (int)$arrPayroll[0]['payroll_deduction_dvr_off'] +
											  (int)$arrPayroll[0]['payroll_deduction_laptop_in_branch'] +
											  (int)$arrPayroll[0]['payroll_deduction_no_response'] +
											  (int)$arrPayroll[0]['payroll_deduction_branch_incharge'] +
											  (int)$arrPayroll[0]['payroll_deduction_CCFLA'] +
											  (int)$arrPayroll[0]['payroll_deduction_dealing_without'] +
											  (int)$arrPayroll[0]['payroll_deduction_misbehave'] +
											  (int)$arrPayroll[0]['payroll_deduction_pending_work'] +
											  (int)$arrPayroll[0]['payroll_deduction_late_coming'] +
											  (int)$arrPayroll[0]['payroll_deductsion_MMB_loan'];
							

							$totalEarnings += (int)$totalEarning;						
							$totalDeductions += (int)$totalDeduction;						
							$totalNetSalary += (int)($totalEarning - $totalDeduction);
							
							$arrEmpPay = array(
												$arrEmployees[$jnd]['emp_code'],
												$arrEmployees[$jnd]['emp_full_name'],
												str_replace(',', ' ', $arrEmployees[$jnd]['job_category_name']),
												$arrEmployees[$jnd]['emp_pay_mode']
											);
							
							if(in_array('Basic', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_basic'];
							if(in_array('Housing', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_housing'];
							if(in_array('Transport', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_transport'];
							if(in_array('Claims', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_claims'];
							if(in_array('Annual Ticket Allowance', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_annual_ticket'];
							if(in_array('Commission', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_commission'];
							if(in_array('Travel/Survey Expense', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_survey_expense'];
							if(in_array('Utility', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_utility'];
							if(in_array('Travel', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_travel'];
							if(in_array('Health', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_health'];
							if(in_array('Fuel', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_fuel'];
							if(in_array('Mobile', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_mobile'];
							if(in_array('Medical', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_medical_relief'];
							if(in_array('Bonus', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_bonus'];
							if(in_array('Annual Leave Encashment', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_annual_leave_encashment'];
							if(in_array('Gratuity', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_gratuity'];
							if(in_array('Settlement', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_settlement'];
							if(in_array('Other Earning', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_misc'];
							if(in_array('Food Allowance', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_earning_food_allowance'];
							
							$arrEmpPay[] = $totalEarning;
							
							if(in_array('Tax', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_tax'];
							if(in_array('Provident Fund', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_pf'];
							if(in_array('Loan', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_loan'];
							if(in_array('EOBI', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_eobi'];
							if(in_array('Telephone Expense', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_telephone'];
							if(in_array('Other Deduction', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_misc'];
							//NEW FIELDS
							if(in_array('Mobile Use', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_mobile'];
							if(in_array('Jeans/T-Shirt', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_jeans'];
							if(in_array('Smoking', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_smoking'];
							if(in_array('Wrong Time', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_wrong_time'];
							if(in_array('Guard Attention', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_guard_attention'];
							if(in_array('DVR Off', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_dvr_off'];
							if(in_array('Laptop In Branch', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_laptop_in_branch'];
							if(in_array('No Response', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_no_response'];
							if(in_array('Branch Incharge', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_branch_incharge'];
							if(in_array('CCFLA', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_CCFLA'];
							if(in_array('Dealing Without', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_dealing_without'];
							if(in_array('Misbehave', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_misbehave'];
							if(in_array('Open Close', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_open_close'];
							if(in_array('Late Coming', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deduction_late_coming'];
							if(in_array('MMB Loan', $arrHeaderColumns)) $arrEmpPay[] = $arrPayroll[0]['payroll_deductsion_MMB_loan'];
							
							$arrEmpPay[] = $totalDeduction;
							$arrEmpPay[] = (int)($totalEarning - $totalDeduction);
							
							$strCSV .=  implode(',', $arrEmpPay) . "\n";
						}
					}
					
					$arrGrandTotal = array(
						'Grand Total',
						'',
						'',
						''
					);
					
					if(in_array('Basic', $arrHeaderColumns)) $arrGrandTotal[] 					= $totalBasic;
					if(in_array('Housing', $arrHeaderColumns)) $arrGrandTotal[] 				= $totalHousing;
					if(in_array('Transport', $arrHeaderColumns)) $arrGrandTotal[] 				= $totalTransport;
					if(in_array('Claims', $arrHeaderColumns)) $arrGrandTotal[] 					= $totalClaims;
					if(in_array('Annual Ticket Allowance', $arrHeaderColumns)) $arrGrandTotal[] = $totalAnnualTicket;
					if(in_array('Commission', $arrHeaderColumns)) $arrGrandTotal[] 				= $totalCommission;
					if(in_array('Travel/Survey Expense', $arrHeaderColumns)) $arrGrandTotal[] 	= $totalSurveyExpense;
					if(in_array('Utility', $arrHeaderColumns)) $arrGrandTotal[] 				= $totalUtility;
					if(in_array('Travel', $arrHeaderColumns)) $arrGrandTotal[] 					= $totalTravel;
					if(in_array('Health', $arrHeaderColumns)) $arrGrandTotal[] 					= $totalHealth;
					if(in_array('Fuel', $arrHeaderColumns)) $arrGrandTotal[] 					= $totalFuel;
					if(in_array('Mobile', $arrHeaderColumns)) $arrGrandTotal[] 					= $totalMobile;
					if(in_array('Medical', $arrHeaderColumns)) $arrGrandTotal[] 				= $totalMedical;
					if(in_array('Bonus', $arrHeaderColumns)) $arrGrandTotal[] 					= $totalBonus;
					if(in_array('Annual Leave Encashment', $arrHeaderColumns)) $arrGrandTotal[] = $totalAnnualLeaveEnc;
					if(in_array('Gratuity', $arrHeaderColumns)) $arrGrandTotal[] 				= $totalGratuity;
					if(in_array('Settlement', $arrHeaderColumns)) $arrGrandTotal[] 				= $totalSettlement;
					if(in_array('Other Earning', $arrHeaderColumns)) $arrGrandTotal[] 			= $totalEarningMisc;
					if(in_array('Food Allowance', $arrHeaderColumns)) $arrGrandTotal[] 			= $totalFoodAllowance;
					
					$arrGrandTotal[] 															= $totalEarnings;
					
					if(in_array('Tax', $arrHeaderColumns)) $arrGrandTotal[] 					= $totalTax;
					if(in_array('Provident Fund', $arrHeaderColumns)) $arrGrandTotal[] 			= $totalPF;
					if(in_array('Loan', $arrHeaderColumns)) $arrGrandTotal[] 					= $totalLoan;
					if(in_array('EOBI', $arrHeaderColumns)) $arrGrandTotal[] 					= $totalEOBI;
					if(in_array('Telephone Expense', $arrHeaderColumns)) $arrGrandTotal[] 		= $totalTelephone;
					if(in_array('Other Deduction', $arrHeaderColumns)) $arrGrandTotal[] 		= $totalDeductionMisc;
					//NEW FIELDS
					if(in_array('Mobile Use', $arrHeaderColumns)) $arrGrandTotal[] 				= $MobileUse;
					if(in_array('Jeans/T-Shirt', $arrHeaderColumns)) $arrGrandTotal[] 			= $JeansTshirt;
					if(in_array('Smoking', $arrHeaderColumns)) $arrGrandTotal[] 				= $Smoking;
					if(in_array('Wrong Time', $arrHeaderColumns)) $arrGrandTotal[] 				= $WrongTime;
					if(in_array('Cashier Checking', $arrHeaderColumns)) $arrGrandTotal[] 		= $CashierChecking;
					if(in_array('Guard Attention', $arrHeaderColumns)) $arrGrandTotal[] 		= $GuardAttention;
					if(in_array('DVR Off', $arrHeaderColumns)) $arrGrandTotal[] 				= $DVROff;
					if(in_array('Laptop In Branch', $arrHeaderColumns)) $arrGrandTotal[] 		= $LaptopInBranch;
					if(in_array('No Response', $arrHeaderColumns)) $arrGrandTotal[] 			= $NoResponse;
					if(in_array('Branch Incharge', $arrHeaderColumns)) $arrGrandTotal[] 		= $BranchIncharge;
					if(in_array('CCFLA', $arrHeaderColumns)) $arrGrandTotal[] 					= $CCFLA;
					if(in_array('Dealing Without', $arrHeaderColumns)) $arrGrandTotal[] 		= $DealingWithout;
					if(in_array('Misbehave', $arrHeaderColumns)) $arrGrandTotal[] 				= $Misbehave;
					if(in_array('Open Close', $arrHeaderColumns)) $arrGrandTotal[] 				= $OpenClose;
					if(in_array('Pending Work', $arrHeaderColumns)) $arrGrandTotal[] 			= $PendingWork;
					if(in_array('Late Coming', $arrHeaderColumns)) $arrGrandTotal[] 			= $lateComing;
					if(in_array('MMB Loan', $arrHeaderColumns)) $arrGrandTotal[] 				= $totalDeductionsMMBLoan;
					
					$arrGrandTotal[] = $totalDeductions;
					$arrGrandTotal[] = (int)($totalEarnings - $totalDeductions);
					
					$strCSV .=  implode(',', $arrGrandTotal) . "\n";
					$strCSV .= "\n\n";
				}
				
				header('Content-Type: text/csv; charset=utf-8');
				header('Content-Disposition: attachment; filename=salary_register_' . strtolower(date('F', mktime(0, 0, 0, $this->input->post("payrollMonth"), 10))) . '_' . $this->input->post("payrollYear") . '.csv');
				$opFile = fopen('php://output', 'w');
				fwrite($opFile, $strCSV);
				fclose($opFile);
				exit;
				
			} else if($this->input->post("frmType") == 2) {
				
				$strDate = $this->input->post("payrollYear") . '-' . str_pad($this->input->post("payrollMonth"), 2, '0', STR_PAD_LEFT) . '-01';
				$this->configuration->saveValues(TABLE_CONFIGURATION, array('config_value' => date("Y-m-t", strtotime($strDate))), array('config_key' => 'PAYROLL_CLOSE_DATE'));
				$this->session->set_flashdata('success_message', 'Payroll Closed Successfully');
				redirect($this->baseURL . '/' . $this->currentController . '/manage_payroll');
				
			}
			
		} else {	
			$this->arrData['validation_error_message'] = validation_errors();
		}
		
		# CODE FOR PAGE CONTENT
		if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
			$this->arrData['arrCompanies'] = $this->configuration->getCompanies(array('company_id' => $this->userCompanyID));
			
		} else {
			$this->arrData['arrCompanies'] = $this->configuration->getCompanies();
		}
		
		$this->arrData['arrMonths'] = $this->config->item('months');
		
		# TEMPLATE LOADING
		$this->template->write_view('content', 'payroll_management/manage_payroll', $this->arrData);
		$this->template->render();
	}
	
	public function employee_detail($empID = 0) {
		$this->arrData['arrRecord'] = $this->employee->getEmployeeDetail(array('e.emp_id' => (int)$empID), false);
		
		$this->template->write_view('content', 'payroll_management/employee_detail', $this->arrData);
		$this->template->render();
	}

	public function get_employee_salary($empID = 0) {
		
		$this->arrData['arrRecord'] = $this->employee->getEmployeeDetail(array('e.emp_id' => (int)$empID), false);
		print_r($this->arrData);exit;
		return $this->arrData['arrRecord'];
		// $this->template->write_view('content', 'payroll_management/employee_detail', $this->arrData);
		// $this->template->render();
	}
	
	public function payslip($payrollID = 0, $empID = 0) {
		$this->load->model('model_attendance_management');

		$arrWhere['ep.payroll_id'] = (int)$payrollID;
		$arrWhere['ep.emp_id'] = (int)$empID;
		
		$this->arrData['record'] = $this->payroll->getPayrolls($arrWhere);
		$this->arrData['record'] = $this->arrData['record'][0];
		
		$leave_month = $this->arrData['record']['payroll_month'] < 10 ? '0'.$this->arrData['record']['payroll_month'] : $this->arrData['record']['payroll_month'];
		
		$present 		= $this->model_attendance_management->totalPresent($empID,$leave_month,$this->arrData['record']['payroll_year']);
		$Absent 		= $this->model_attendance_management->totalAbsent($empID,$leave_month,$this->arrData['record']['payroll_year']);
		$Leave 			= $this->model_attendance_management->totalLeaves($empID,$leave_month,$this->arrData['record']['payroll_year']);

		$this->arrData['arrEmployee'] = $this->employee->getEmployeeDetail(array('e.emp_id' => (int)$empID), false);
		$arrCompany = $this->configuration->getCompanies(array('company_id' => $this->arrData['record']['payroll_company_id']));
		$arrJobCategory = $this->configuration->getJobCategories(array('job_category_id' => $this->arrData['arrEmployee']['emp_job_category_id']));
		$arrLocation = $this->configuration->getLocations(array('location_id' => $arrCompany[0]['company_country_id']));
		$arrBank = $this->configuration->getBanks(array('bank_id' => $this->arrData['arrEmployee']['emp_salary_bank_id']));
		$basic = (($this->arrData['record']['payroll_earning_basic'] / 165) * 100);
		
		$totalEarning = (int)$basic + 
						(int)$this->arrData['record']['payroll_earning_housing'] +
						(int)$this->arrData['record']['payroll_earning_transport'] +
						(int)$this->arrData['record']['payroll_earning_utility'] +
						(int)$this->arrData['record']['payroll_earning_travel'] +
						(int)$this->arrData['record']['payroll_earning_health'] +
						(int)$this->arrData['record']['payroll_earning_fuel'] +
						(int)$this->arrData['record']['payroll_earning_mobile'] +
						(int)$this->arrData['record']['payroll_earning_medical_relief'] +
						(int)$this->arrData['record']['payroll_earning_bonus'] +
						(int)$this->arrData['record']['payroll_earning_annual_leave_encashment'] +
						(int)$this->arrData['record']['payroll_earning_claims'] +
						(int)$this->arrData['record']['payroll_earning_commission'] +
						(int)$this->arrData['record']['payroll_earning_annual_ticket'] +
						(int)$this->arrData['record']['payroll_earning_gratuity'] +
						(int)$this->arrData['record']['payroll_earning_survey_expense'] +
						(int)$this->arrData['record']['payroll_earning_settlement'] +
						(int)$this->arrData['record']['payroll_earning_misc'] +
						(int)$this->arrData['record']['payroll_earning_food_allowance'];	
		$totalDeduction = 
						  (int)$this->arrData['record']['payroll_deduction_tax'] + 
						  (int)$this->arrData['record']['payroll_deduction_pf'] + 
						  (int)$this->arrData['record']['payroll_deduction_loan'] + 
						  (int)$this->arrData['record']['payroll_deduction_eobi'] + 
						  (int)$this->arrData['record']['payroll_deduction_telephone'] + 
						  (int)$this->arrData['record']['payroll_deduction_misc']+
						  //NEW FIELDS
						  (int)$this->arrData['record']['payroll_deduction_mobile'] +
						  (int)$this->arrData['record']['payroll_deduction_jeans'] +
						  (int)$this->arrData['record']['payroll_deduction_smoking'] +
						  (int)$this->arrData['record']['payroll_deduction_wrong_time'] +
						  (int)$this->arrData['record']['payroll_deduction_cashier_checking'] +
						  (int)$this->arrData['record']['payroll_deduction_guard_attention'] +
						  (int)$this->arrData['record']['payroll_deduction_dvr_off'] +
						  (int)$this->arrData['record']['payroll_deduction_laptop_in_branch'] +
						  (int)$this->arrData['record']['payroll_deduction_no_response'] +
						  (int)$this->arrData['record']['payroll_deduction_branch_incharge'] +
						  (int)$this->arrData['record']['payroll_deduction_CCFLA'] +
						  (int)$this->arrData['record']['payroll_deduction_dealing_without'] +
						  (int)$this->arrData['record']['payroll_deduction_misbehave'] +
						  (int)$this->arrData['record']['payroll_deduction_open_close'] +
						  (int)$this->arrData['record']['payroll_deduction_pending_work'] +
						  (int)$this->arrData['record']['payroll_deduction_late_coming'] +
						  (int)$this->arrData['record']['cctc_approval_deduction'] +
						  (int)$this->arrData['record']['compliance_approval_deduction'] +
						  (int)$this->arrData['record']['payroll_deductsion_MMB_loan'];
		
		$totalComplianceDeduction = (int)$this->arrData['record']['compliance_approval_deduction'];
		$lateComingDeduction = (int)$this->arrData['record']['payroll_deduction_late_coming'];
		$totalcctvDeduction = (int)$this->arrData['record']['cctc_approval_deduction'];
		
		$HRDeduction = 
						  (int)$this->arrData['record']['payroll_deduction_tax'] + 
						  (int)$this->arrData['record']['payroll_deduction_pf'] + 
						  (int)$this->arrData['record']['payroll_deduction_loan'] + 
						  (int)$this->arrData['record']['payroll_deduction_eobi'] + 
						  (int)$this->arrData['record']['payroll_deduction_telephone'] + 
						  (int)$this->arrData['record']['payroll_deduction_mobile']+
						  (int)$this->arrData['record']['payroll_deduction_misc'];
						
		$strEarningsHTML = '';
		
		if((int)$this->arrData['record']['net_salary']) {
			$basic =  (($this->arrData['record']['payroll_earning_basic'] / 165) * 100);

			$strEarningsHTML .= '<tr>
			<td>Basic</td>
			<td>' . number_format($basic, 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_housing']) {
			$strEarningsHTML .= '<tr>
			<td>Housing Allowance</td>
			<td>' . number_format($this->arrData['record']['payroll_earning_housing'], 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_transport']) {
			$strEarningsHTML .= '<tr>
			<td>Transport Allowance</td>
			<td>' . number_format($this->arrData['record']['payroll_earning_transport'], 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_utility']) {
			$strEarningsHTML .= '<tr>
			<td>Utility Allowance</td>
			<td>' . number_format($this->arrData['record']['payroll_earning_utility'], 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_travel']) {
			$strEarningsHTML .= '<tr>
			<td>Travel Expenses</td>
			<td>' . number_format($this->arrData['record']['payroll_earning_travel'], 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_survey_expense']) {
			$strEarningsHTML .= '<tr>
			<td>Travel/Survey Expense</td>
			<td>' . number_format($this->arrData['record']['payroll_earning_survey_expense'], 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_commission']) {
			$strEarningsHTML .= '<tr>
			<td>Commission</td>
			<td>' . number_format($this->arrData['record']['payroll_earning_commission'], 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_health']) {
			$strEarningsHTML .= '<tr>
			<td>Health Allowance</td>
			<td>' . number_format($this->arrData['record']['payroll_earning_health'], 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_fuel']) {
			$strEarningsHTML .= '<tr>
			<td>Fuel</td>
			<td>' . number_format($this->arrData['record']['payroll_earning_fuel'], 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_mobile']) {
			$strEarningsHTML .= '<tr>
			<td>Mobile/Telephone</td>
			<td>' . number_format($this->arrData['record']['payroll_earning_mobile'], 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_medical_relief']) {
			$strEarningsHTML .= '<tr>
			<td>Medical Relief</td>
			<td>' . number_format($this->arrData['record']['payroll_earning_medical_relief'], 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_bonus']) {
			$strEarningsHTML .= '<tr>
			<td>Bonus</td>
			<td>' . number_format($this->arrData['record']['payroll_earning_bonus'], 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_annual_ticket']) {
			$strEarningsHTML .= '<tr>
			<td>Annual Ticket</td>
			<td>' . number_format($this->arrData['record']['payroll_earning_annual_ticket'], 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_claims']) {
			$strEarningsHTML .= '<tr>
			<td>Claims</td>
			<td>' . number_format($this->arrData['record']['payroll_earning_claims'], 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_annual_leave_encashment']) {
			$strEarningsHTML .= '<tr>
			<td>Annual Leave Encashment</td>
			<td>' . number_format($this->arrData['record']['payroll_earning_annual_leave_encashment'], 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_gratuity']) {
			$strEarningsHTML .= '<tr>
			<td>Gratuity</td>
			<td>' . number_format($this->arrData['record']['payroll_earning_gratuity'], 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_settlement']) {
			$strEarningsHTML .= '<tr>
			<td>Final Settlement</td>
			<td>' . number_format($this->arrData['record']['payroll_earning_settlement'], 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_misc']) {
			$strEarningsHTML .= '<tr>
			<td>Earning Misc</td>
			<td>' . number_format($this->arrData['record']['payroll_earning_misc'], 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_food_allowance']) {
			$strEarningsHTML .= '<tr>
			<td>Earning Food Allowance</td>
			<td>' . number_format($this->arrData['record']['payroll_earning_food_allowance'], 2) . '</td>	
		</tr>';
		}
		
		$strDeductionsHTML = '';
		
		if((int)$this->arrData['record']['payroll_deduction_tax']) {
			$strDeductionsHTML .= '<tr>
			<td> Income Tax</td>
			<td>' . number_format($this->arrData['record']['payroll_deduction_tax'], 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_deduction_pf']) {
			$strDeductionsHTML .= '<tr>
			<td> PF Deduction</td>
			<td>' . number_format($this->arrData['record']['payroll_deduction_pf'], 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_deduction_loan']) {
			$strDeductionsHTML .= '<tr>
			<td> Salary Advance</td>
			<td>' . number_format($this->arrData['record']['payroll_deduction_loan'], 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_deduction_eobi']) {
			$strDeductionsHTML .= '<tr>
			<td> EOBI</td>
			<td>' . number_format($this->arrData['record']['payroll_deduction_eobi'], 2) . '</td>	
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_deduction_telephone']) {
			$strDeductionsHTML .= '<tr>
			<td> HR Deductions</td>
			<td>' . number_format($this->arrData['record']['payroll_deduction_telephone'], 2) . '</td>	
		</tr>';
		}

		if((int)$this->arrData['record']['payroll_deduction_mobile']) {
			$strDeductionsHTML .= '<tr>
			<td> Mobile Use</td>
			<td>' . number_format($this->arrData['record']['payroll_deduction_mobile'], 2) . '</td>	
		</tr>';
		}

		if((int)$this->arrData['record']['payroll_deductsion_MMB_loan']) {
			$strDeductionsHTML .= '<tr>
			<td>Loan Deductions</td>
			<td>' . number_format($this->arrData['record']['payroll_deductsion_MMB_loan'], 2) . '</td>	
		</tr>';
		}

		if((int)$this->arrData['record']['payroll_deduction_late_coming']) {
			$strDeductionsHTML .= '<tr>
			<td> Late coming Deductions ('.$this->arrData['record']['total_late_hours'] .' mins x 5) </td>
			<td>' . number_format($this->arrData['record']['payroll_deduction_late_coming'], 2) . '</td>
		</tr>';
		}

		$strDeductionsHTML .= '<tr>
			<td>Others</td>
			<td>' . $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['cctc_approval_deduction'], 2) . '</td>
		</tr>';
		$total = $totalEarning - $totalDeduction;
		
		$pdfbasic 	= (($this->arrData['record']['payroll_earning_basic'] / 165) * 100);
		$arrValues 	= array(
							'[LOGO_URL]' 				=> EMAIL_HEADER_LOGO,
							'[OFFICE_ADDRESS]' 			=> $this->arrData['arrEmployee']['company_name'] . '<br />&nbsp;&nbsp;' . $this->arrData['arrEmployee']['company_address'],
							'[CREATED_DATE_TIME]' 		=> date(SHOW_DATE_TIME_FORMAT . ' h:i:s A', strtotime('now')),
							'[MONTH]' 					=> date('F', mktime(0, 0, 0, (int)$this->arrData['record']['payroll_month'], 10)),
							'[YEAR]' 					=> $this->arrData['record']['payroll_year'],
							'[EMPLOYEE_CODE]' 			=> $this->arrData['arrEmployee']['emp_code'],
							'[EMPLOYEE_DESIGNATION]' 	=> $this->arrData['arrEmployee']['emp_designation'],
							'[EMPLOYEE_NAME]' 			=> $this->arrData['arrEmployee']['emp_full_name'],
							'[EMPLOYEE_JOINING_DATE]' 	=> date(SHOW_DATE_TIME_FORMAT, strtotime($this->arrData['arrEmployee']['emp_joining_date'])),
							'[EMPLOYEE_DEPARTMENT]' 	=> $arrJobCategory[0]['job_category_name'],
							'[EMPLOYEE_CURRENCY]' 		=> $arrLocation[0]['location_currency_code'],
							'[EMPLOYEE_BANK]' 			=> $arrBank[0]['bank_name'],
							'[EMPLOYEE_BRANCH]' 		=> $this->arrData['arrEmployee']['emp_salary_bank_branch'],
							'[EMPLOYEE_ACCOUNT_NUMBER]' => $this->arrData['arrEmployee']['emp_salary_bank_account_number'],
							'[ANNUAL_LEAVE]' 			=> $this->arrData['arrEmployee']['emp_annual_leaves'],
							'[SICK_LEAVE]' 				=> $this->arrData['arrEmployee']['emp_sick_leaves'],
							'[BASIC_SALARY]' 			=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($pdfbasic, 2),
							'[HOUSING]' 				=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_housing'], 2),
							'[TRANSPORT]' 				=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($this->arrData['record']['payroll_earning_transport'], 2),
							'[GROSS_EARNING]' 			=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($totalEarning, 2),
							'[EARNINGS]' 				=> $strEarningsHTML,
							'[DEDUCTIONS]' 				=> $strDeductionsHTML,
							'[EARNINGS]' 				=> $strEarningsHTML,
							'[PRESENT]' 				=> (int)$this->arrData['record']['total_workDays'],
							'[HOLIDAY]' 				=> (int)$this->arrData['record']['total_holidays'],
							'[ABSENT]' 					=> (int)$this->arrData['record']['total_absent'],
							'[LEAVE]' 					=> (int)$this->arrData['record']['total_leave'],
							'[TRANSFER]' 				=> (int)$this->arrData['record']['total_tranfer'],
							'[TOTAL_WORKING_DAYS]'		=> (int)$this->arrData['record']['total_workDays'] + (int)$this->arrData['record']['total_absent'] + (int)$this->arrData['record']['total_tranfer'] + (int)$this->arrData['record']['total_leave'],
							'[TOTAL_EARNINGS]' 			=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($totalEarning, 2),
							'[LOAN]' 					=> (int)$this->arrData['record']['payroll_deduction_loan'],
							'[TAXDEDUCTIONS]' 			=> (int)$this->arrData['record']['payroll_deduction_tax'],
							'[FINEDEDUCTIONS]' 			=> (int)$this->arrData['record']['payroll_deduction_misc'],
							'[TOTAL_DEDUCTIONS]' 		=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($totalDeduction, 2),
							'[NET_SALARY]' 				=> $arrLocation[0]['location_currency_code'] . ' ' . number_format((int)($totalEarning - $totalDeduction), 2),
							//   '[AMOUNT_IN_WORDS]' 		=> strtoupper($objFormatter->format((int)($totalEarning - $totalDeduction)))
							'[AMOUNT_IN_WORDS]' 		=> strtoupper($this->convertNumberToWord($totalEarning - $totalDeduction)),
						);

		  $strHTML = getHTML($arrValues, 'payslip.html');		  
		  require_once(APPPATH . 'libraries/tcpdf/tcpdf.php');
		  $pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		  // set document information
		  $pdf->SetCreator(PDF_CREATOR);
		  $pdf->SetAuthor(PDF_AUTHOR);
		  $pdf->setCellHeightRatio(2);
		  $pdf->setPrintHeader(false);
		  $pdf->setPrintFooter(false);
		  $pdf->SetFontSize(8);	
		  $pdf->SetCellPadding(3);			  
		  // set margins
		  $pdf->SetMargins(PDF_MARGIN_LEFT, 18, PDF_MARGIN_RIGHT);	
		// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

		//   $pdf->SetMargins(PDF_MARGIN_TOP, 18, PDF_MARGIN_BOTTOM);				  
		//   set auto page breaks
		  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);				  
		  // set image scale factor
		  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);				  
		  // set some language-dependent strings (optional)
		  if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
				require_once(dirname(__FILE__).'/lang/eng.php');
				$pdf->setLanguageArray($l);
		  }
		  
		  // add a page
		  $pdf->AddPage();			  
		  // output the HTML content
		  $pdf->writeHTML(utf8_encode($strHTML), true, 0, true, 0);
		  $pdf->lastPage();
		  $pdfFileName = 'Salary_Slip_' . str_replace(' ', '_', $this->arrData['arrEmployee']['emp_full_name']) . '_' . date('F', mktime(0, 0, 0, (int)$this->arrData['record']['payroll_month'], 10)) . '_' . $this->arrData['record']['payroll_year'] . '.pdf';
		  $pdf->Output($pdfFileName, 'D');
		  
		  header('Content-Type: text/doc');
		  header('Content-Disposition: attachment;filename="'.$pdfFileName.'"');
		  header('Cache-Control: max-age=0');
		  readfile('./' . PDF_FILES_FOLDER . $pdfFileName);
		  
		  exit;
	}

	public function payroll_deduction($month = 0, $year = 0, $empID = 0, $payrollID = 0) {
		$this->load->model('model_deduction_management');
		$this->load->model('model_compliance_management');

		$arrWhere['ep.payroll_id'] = (int)$payrollID;
		$arrWhere['ep.emp_id'] = (int)$empID;
		
		$this->arrData['record'] = $this->payroll->getPayrolls($arrWhere);
		$this->arrData['record'] = $this->arrData['record'][0];
		
		$this->arrData['arrEmployee'] = $this->employee->getEmployeeDetail(array('e.emp_id' => (int)$empID), false);
		$arrCompany = $this->configuration->getCompanies(array('company_id' => $this->arrData['record']['payroll_company_id']));
		$arrJobCategory = $this->configuration->getJobCategories(array('job_category_id' => $this->arrData['arrEmployee']['emp_job_category_id']));
		$arrLocation = $this->configuration->getLocations(array('location_id' => $arrCompany[0]['company_country_id']));
		$arrBank = $this->configuration->getBanks(array('bank_id' => $this->arrData['arrEmployee']['emp_salary_bank_id']));

		$basic = (($this->arrData['record']['payroll_earning_basic'] / 165) * 100);
		// print_r($basic);exit;
		$totalEarning = (int)$basic + 
						(int)$this->arrData['record']['payroll_earning_housing'] +
						(int)$this->arrData['record']['payroll_earning_transport'] +
						(int)$this->arrData['record']['payroll_earning_utility'] +
						(int)$this->arrData['record']['payroll_earning_travel'] +
						(int)$this->arrData['record']['payroll_earning_health'] +
						(int)$this->arrData['record']['payroll_earning_fuel'] +
						(int)$this->arrData['record']['payroll_earning_mobile'] +
						(int)$this->arrData['record']['payroll_earning_medical_relief'] +
						(int)$this->arrData['record']['payroll_earning_bonus'] +
						(int)$this->arrData['record']['payroll_earning_annual_leave_encashment'] +
						(int)$this->arrData['record']['payroll_earning_claims'] +
						(int)$this->arrData['record']['payroll_earning_commission'] +
						(int)$this->arrData['record']['payroll_earning_annual_ticket'] +
						(int)$this->arrData['record']['payroll_earning_gratuity'] +
						(int)$this->arrData['record']['payroll_earning_survey_expense'] +
						(int)$this->arrData['record']['payroll_earning_settlement'] +
						(int)$this->arrData['record']['payroll_earning_misc'] +
						(int)$this->arrData['record']['payroll_earning_food_allowance'];	
		
		$totalDeduction = 
						  (int)$this->arrData['record']['payroll_deduction_tax'] + 
						  (int)$this->arrData['record']['payroll_deduction_pf'] + 
						  (int)$this->arrData['record']['payroll_deduction_loan'] + 
						  (int)$this->arrData['record']['payroll_deduction_eobi'] + 
						  (int)$this->arrData['record']['payroll_deduction_telephone'] + 
						  (int)$this->arrData['record']['payroll_deduction_misc']+
						  //NEW FIELDS
						  (int)$this->arrData['record']['payroll_deduction_mobile'] +
						  (int)$this->arrData['record']['payroll_deduction_jeans'] +
						  (int)$this->arrData['record']['payroll_deduction_smoking'] +
						  (int)$this->arrData['record']['payroll_deduction_wrong_time'] +
						  (int)$this->arrData['record']['payroll_deduction_cashier_checking'] +
						  (int)$this->arrData['record']['payroll_deduction_guard_attention'] +
						  (int)$this->arrData['record']['payroll_deduction_dvr_off'] +
						  (int)$this->arrData['record']['payroll_deduction_laptop_in_branch'] +
						  (int)$this->arrData['record']['payroll_deduction_no_response'] +
						  (int)$this->arrData['record']['payroll_deduction_branch_incharge'] +
						  (int)$this->arrData['record']['payroll_deduction_CCFLA'] +
						  (int)$this->arrData['record']['payroll_deduction_dealing_without'] +
						  (int)$this->arrData['record']['payroll_deduction_misbehave'] +
						  (int)$this->arrData['record']['payroll_deduction_open_close'] +
						  (int)$this->arrData['record']['payroll_deduction_pending_work'] +
						  (int)$this->arrData['record']['payroll_deduction_late_coming'] +
						  (int)$this->arrData['record']['cctc_approval_deduction'] +
						  (int)$this->arrData['record']['compliance_approval_deduction'] +
						  (int)$this->arrData['record']['payroll_deductsion_MMB_loan'];
		
		$totalComplianceDeduction = (int)$this->arrData['record']['compliance_approval_deduction'];
		$lateComingDeduction = (int)$this->arrData['record']['payroll_deduction_late_coming'];
		$totalcctvDeduction = (int)$this->arrData['record']['cctc_approval_deduction'];
		
		$HRDeduction = 
						  (int)$this->arrData['record']['payroll_deduction_tax'] + 
						  (int)$this->arrData['record']['payroll_deduction_pf'] + 
						  (int)$this->arrData['record']['payroll_deduction_loan'] + 
						  (int)$this->arrData['record']['payroll_deduction_eobi'] + 
						  (int)$this->arrData['record']['payroll_deduction_telephone'] + 
						  (int)$this->arrData['record']['payroll_deduction_mobile']+
						  (int)$this->arrData['record']['payroll_deduction_misc'];
						
		$strEarningsHTML = '';
		
		if((int)$this->arrData['record']['net_salary']) {
			$basic =  (($this->arrData['record']['payroll_earning_basic'] / 165) * 100);

			$strEarningsHTML .= '<tr>
			<td style="padding: 11px;">Basic</td>
			<td style="padding: 11px;">' . number_format($basic, 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_housing']) {
			$strEarningsHTML .= '<tr>
			<td style="padding: 11px;">Housing Allowance</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_earning_housing'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_transport']) {
			$strEarningsHTML .= '<tr>
			<td style="padding: 11px;">Transport Allowance</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_earning_transport'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_utility']) {
			$strEarningsHTML .= '<tr>
			<td style="padding: 11px;">Utility Allowance</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_earning_utility'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_travel']) {
			$strEarningsHTML .= '<tr>
			<td style="padding: 11px;">Travel Expenses</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_earning_travel'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_survey_expense']) {
			$strEarningsHTML .= '<tr>
			<td style="padding: 11px;">Travel/Survey Expense</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_earning_survey_expense'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_commission']) {
			$strEarningsHTML .= '<tr>
			<td style="padding: 11px;">Commission</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_earning_commission'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_health']) {
			$strEarningsHTML .= '<tr>
			<td style="padding: 11px;">Health Allowance</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_earning_health'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_fuel']) {
			$strEarningsHTML .= '<tr>
			<td style="padding: 11px;">Fuel</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_earning_fuel'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_mobile']) {
			$strEarningsHTML .= '<tr>
			<td style="padding: 11px;">Mobile/Telephone</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_earning_mobile'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_medical_relief']) {
			$strEarningsHTML .= '<tr>
			<td style="padding: 11px;">Medical Relief</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_earning_medical_relief'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_bonus']) {
			$strEarningsHTML .= '<tr>
			<td style="padding: 11px;">Bonus</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_earning_bonus'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_annual_ticket']) {
			$strEarningsHTML .= '<tr>
			<td style="padding: 11px;">Annual Ticket</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_earning_annual_ticket'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_claims']) {
			$strEarningsHTML .= '<tr>
			<td style="padding: 11px;">Claims</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_earning_claims'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_annual_leave_encashment']) {
			$strEarningsHTML .= '<tr>
			<td style="padding: 11px;">Annual Leave Encashment</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_earning_annual_leave_encashment'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_gratuity']) {
			$strEarningsHTML .= '<tr>
			<td style="padding: 11px;">Gratuity</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_earning_gratuity'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_settlement']) {
			$strEarningsHTML .= '<tr>
			<td style="padding: 11px;">Final Settlement</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_earning_settlement'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_misc']) {
			$strEarningsHTML .= '<tr>
			<td style="padding: 11px;">Earning Misc</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_earning_misc'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_earning_food_allowance']) {
			$strEarningsHTML .= '<tr>
			<td style="padding: 11px;">Earning Food Allowance</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_earning_food_allowance'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		$strDeductionsHTML = '';
		
		if((int)$this->arrData['record']['payroll_deduction_tax']) {
			$strDeductionsHTML .= '<tr>
			<td style="padding: 11px;"> Income Tax</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_deduction_tax'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_deduction_pf']) {
			$strDeductionsHTML .= '<tr>
			<td style="padding: 11px;"> PF Deduction</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_deduction_pf'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_deduction_loan']) {
			$strDeductionsHTML .= '<tr>
			<td style="padding: 11px;"> Salary Advance/Loan Recovery</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_deduction_loan'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_deduction_eobi']) {
			$strDeductionsHTML .= '<tr>
			<td style="padding: 11px;"> EOBI</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_deduction_eobi'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}
		
		if((int)$this->arrData['record']['payroll_deduction_telephone']) {
			$strDeductionsHTML .= '<tr>
			<td style="padding: 11px;"> HR Deductions</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_deduction_telephone'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}

		if((int)$this->arrData['record']['payroll_deduction_mobile']) {
			$strDeductionsHTML .= '<tr>
			<td style="padding: 11px;"> Mobile Use</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_deduction_mobile'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}

		if((int)$this->arrData['record']['payroll_deductsion_MMB_loan']) {
			$strDeductionsHTML .= '<tr>
			<td style="padding: 11px;"> MMB Loan Deductions</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_deductsion_MMB_loan'], 2) . '</td>
			
			<td style="padding: 11px;"> System Generated </td>
		</tr>';
		}

		$monnth_search = $month >= 10 ? $month : '0'.$month;
		$Late_Coming_DeductionsHTML = "";
		if((int)$this->arrData['record']['payroll_deduction_late_coming']) {
			$Late_Coming_DeductionsHTML .= '<tr>
			<td style="padding: 11px;"> Late coming Deductions</td>
			<td style="padding: 11px;">' . number_format($this->arrData['record']['payroll_deduction_late_coming'], 2) . '</td>
			<td style="padding: 11px;"> System Generated </td>
			<td style="padding: 11px;">
				<form name="frmAttDetail" id="frmAttDetail" method="post" action="'.$this->baseURL.'/attendance_management/att_detail" target="_blank">
					<input type="hidden" name="empID" class="dropDown" value="'.$empID.'">
					<input type="hidden" name="selMonth" class="dropDown" value="'.$monnth_search.'">
					<input type="hidden" name="selYear" class="dropDown" value="'.$year.'">
					<input type="hidden" name="txtExport" id="txtExport" value="0" />
					<input class="searchButton" name="btnSearch" id="btnSearch" type="submit" value="View Details">
				</form>
			</td>
		</tr>';
		}

		$monthNum  = (int)$this->arrData['record']['payroll_month'];
		$dateObj   = DateTime::createFromFormat('!m', $monthNum);
		$monthName = $dateObj->format('F'); // March
		$arrEmpDetail	 		= $this->model_deduction_management->getEmployeeSummary($empID, $month, $year);
		$arrTransferDetail 		= $this->model_deduction_management->getPayrollSummary($empID, $month, $year);
		$arrComplinaceDetail 	= $this->model_compliance_management->getPayrollSummary($empID, $month, $year);
		$transferarrValues['transfer_details'] = array(
			'LOGO_URL' 					=> "https://amanahmall.com.pk/wp-content/uploads/2018/12/Pakistan-Currency-Exchange.png",
			'CREATED_DATE_TIME' 		=> date(SHOW_DATE_TIME_FORMAT . 'h:i:s A', strtotime('now')),						  
			'CREATED_MONTH' 			=> $monthName,							  
			'EMPLOYEE_CODE' 			=> $arrEmpDetail[0]['emp_code'],
			'EMPLOYEE_NAME' 			=> $arrEmpDetail[0]['emp_full_name'],
			'JOINING_DATE'				=> $arrEmpDetail[0]['emp_joining_date'],
			'EMPLOYEE_DESIGNATION' 		=> $arrEmpDetail[0]['emp_designation'],
			'TOTALHRDEDUCTION'			=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($HRDeduction, 2),
			'TOTALCCTVDEDUCTION'		=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($totalcctvDeduction, 2),
			'TOTALCOMPLIANCEDEDUCTION'	=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($totalComplianceDeduction, 2),
			'TOTALLATECOMINGDEDUCTION'	=> $arrLocation[0]['location_currency_code'] . ' ' . number_format($lateComingDeduction, 2),
			'CCTVDEDUCTION'				=> $arrTransferDetail,
			'COMPLIANCEDEDUCTION'		=> $arrComplinaceDetail,
			'SYSTEMDEDUCTION'			=> $strDeductionsHTML,
			'SYSTEMBENEFITS'			=> $strEarningsHTML,
			'LATECOMINGDEDUCTION'		=> $Late_Coming_DeductionsHTML,
			'TOTAL_EARNINGS'			=> number_format($totalEarning, 2),
			'TOTAL_DEDUCTIONS'			=> number_format($totalDeduction, 2),
			'NET_SALARY'				=> number_format((int)($totalEarning - $totalDeduction), 2),

		);
		$this->template->write_view('content', 'payroll_management/payroll_deduction', $transferarrValues);
		$this->template->render();
	}
	
	function convertNumberToWord($num = false)
	{
		$num = str_replace(array(',', ' '), '' , trim($num));
		if(! $num) {
			return false;
		}
		$num = (int) $num;
		$words = array();
		$list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
			'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
		);
		$list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
		$list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
			'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
			'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
		);
		$num_length = strlen($num);
		$levels = (int) (($num_length + 2) / 3);
		$max_length = $levels * 3;
		$num = substr('00' . $num, -$max_length);
		$num_levels = str_split($num, 3);
		for ($i = 0; $i < count($num_levels); $i++) {
			$levels--;
			$hundreds = (int) ($num_levels[$i] / 100);
			$hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
			$tens = (int) ($num_levels[$i] % 100);
			$singles = '';
			if ( $tens < 20 ) {
				$tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
			} else {
				$tens = (int)($tens / 10);
				$tens = ' ' . $list2[$tens] . ' ';
				$singles = (int) ($num_levels[$i] % 10);
				$singles = ' ' . $list1[$singles] . ' ';
			}
			$words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
		} //end for loop
		$commas = count($words);
		if ($commas > 1) {
			$commas = $commas - 1;
		}
		return implode(' ', $words);
	}

	function list_department(){
		$this->load->model('model_attendance_management');
		$TW_date 	    = date('d-m-Y');
		$TW_month     	= readableDate($TW_date, 'm');
		$TW_year 	    = readableDate($TW_date, 'Y');
		$daysInMonth  	= cal_days_in_month(CAL_GREGORIAN, $TW_month, $TW_year);
		$myTime 		= strtotime($TW_date);
		$workDays     	= 0;
		while($daysInMonth >= 0){
			$day = date("D", $myTime);
			if($day != "Sun"){
				$workDays++;
			}else{}
			$daysInMonth--;
		}
		$workHours = $workDays * 9;	
		// print_r($workHours);exit;
		$this->arrData['fetch_timing'] 		= $this->payroll->get_department_emp();
		// print_r(json_encode($this->arrData['job_categories']));exit;
		// $this->arrData['fetch_timing'] 		= $this->model_attendance_management->fetch_overtime();
		$this->arrData['arrSupervisors'] 	= $this->model_attendance_management->getSupervisors($this->userEmpNum);
		// print_r($this->arrData['arrSupervisors']);exit;

		if(!isAdmin($this->userRoleID)) {
		
			$arrWhereSupervisors = array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE);
			$arrWhereEmployees = array('es.supervisor_emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE);
			
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$arrWhereSupervisors['e.emp_company_id'] = $this->userCompanyID;
				$arrWhereEmployees['e.emp_company_id'] = $this->userCompanyID;
			}
			
			$this->arrData['arrSupervisors'] = $this->employee->getEmployees($arrWhereSupervisors);
			$this->arrData['arrEmployees'] = $this->employee->getEmployees($arrWhereEmployees);
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE));
		} else {
			$this->arrData['arrSupervisors'] = $this->employee->getSupervisors();
			
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$this->arrData['arrEmployees'] = $this->payroll->getEmployeesPayroll(array('p.payroll_company_id' => $this->userCompanyID));
			} else {
				$this->arrData['arrEmployees'] = $this->employee->getEmployees(array());
			}
			
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		}		
		
		$finalResult = array();		
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrEmployees'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);
			if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
				unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
			}
		}
		$this->arrData["arrEmployees"] = $finalResult;
		$this->arrData['arrMonths'] = $this->config->item('months');

		$this->template->write_view('content', 'payroll_management/list_department', $this->arrData);
		$this->template->render();
	}

	function over_time($dep_id = '1', $month = '1', $year = '2021', $action = '', $emp = '', $days_num = ''){
		if ($action == 1) {
			// print_r($day);exit;
			$total_salary = $this->payroll->fetch_overtime_by_emp($dep_id, $month, $year, $emp);
			$total_salary = $total_salary[0];
			$basic = (($total_salary['total_salary'] / 165) * 100);
			$TW_date 		= readableDate('1/'.$month.'/'.$year, 'd/m/Y');
			$daysInMonth 	= cal_days_in_month(CAL_GREGORIAN, $month, $year);
			$workDays 		= 0;			
			for ($i=1; $i <=$daysInMonth ; $i++) { 
				$day = date("D", strtotime($month.'/'.$i.'/'. $year));
				if($day != "Sun"){
					$workDays++;
				}
			}
			$perHourSalary = ($basic / $workDays) / 9;
			$totalOvertimeHours = $total_salary['hours'];
			$overTimeTotal = $perHourSalary * $totalOvertimeHours;
			$arrValues = array(
				'emp_id' 		=> $emp,
				'ot_amount' 	=> round($overTimeTotal),
				'ot_statuss' 	=> "1",
				'day' 			=> $days_num,
				'month' 		=> $month,
				'year' 			=> $year,
				'proced_by' 	=> $this->userEmpNum,
				'proced_date' 	=> date($this->arrData["dateTimeFormat"])
			);
			$this->overtime->saveValues(TABLE_EMPLOYEE_OVERTIME, $arrValues);
			$this->session->set_flashdata('success_message', 'Overtime Details Update Successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/list_department');

		}else if($action == 2){
			$total_salary = $this->payroll->fetch_overtime_by_emp($dep_id, $month, $year, $emp);
			$total_salary = $total_salary[0];
			$basic = (($total_salary['total_salary'] / 165) * 100);
			$TW_date 		= readableDate('1/'.$month.'/'.$year, 'd/m/Y');
			$daysInMonth 	= cal_days_in_month(CAL_GREGORIAN, $month, $year);
			$workDays 		= 0;			
			for ($i=1; $i <=$daysInMonth ; $i++) { 
				$day = date("D", strtotime($month.'/'.$i.'/'. $year));
				if($day != "Sun"){
					$workDays++;
				}
			}
			$perHourSalary = ($basic / $workDays) / 9;
			$totalOvertimeHours = $total_salary['hours'];
			$overTimeTotal = $perHourSalary * $totalOvertimeHours;
			$arrValues = array(
				'emp_id' 		=> $emp,
				'ot_amount' 	=> round($overTimeTotal),
				'ot_statuss' 	=> "2",
				'day' 			=> $days_num,
				'month' 		=> $month,
				'year' 			=> $year,
				'proced_by' 	=> $this->userEmpNum,
				'proced_date' 	=> date($this->arrData["dateTimeFormat"])
			);
			$this->overtime->saveValues(TABLE_EMPLOYEE_OVERTIME, $arrValues);
			$this->session->set_flashdata('success_message', 'Overtime Details Update Successfully');
			redirect($this->baseURL . '/' . $this->currentController . '/list_department');
		}
		$this->load->model('model_attendance_management');
		$this->arrData['fetch_timing'] 		= $this->payroll->fetch_overtime($dep_id, $month, $year);
		$this->arrData['arrSupervisors'] 	= $this->model_attendance_management->getSupervisors($this->userEmpNum);

		if(!isAdmin($this->userRoleID)) {
		
			$arrWhereSupervisors = array('e.emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE);
			$arrWhereEmployees = array('es.supervisor_emp_id in ' => '(' . $this->arrData['strHierarchy'] . ')', 'emp_status' => STATUS_ACTIVE);
			
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$arrWhereSupervisors['e.emp_company_id'] = $this->userCompanyID;
				$arrWhereEmployees['e.emp_company_id'] = $this->userCompanyID;
			}
			
			$this->arrData['arrSupervisors'] = $this->employee->getEmployees($arrWhereSupervisors);
			$this->arrData['arrEmployees'] = $this->employee->getEmployees($arrWhereEmployees);
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE));
		} else {
			$this->arrData['arrSupervisors'] = $this->employee->getSupervisors();
			
			if($this->userRoleID == ACCOUNT_EMPLOYEE_ROLE_ID) {
				$this->arrData['arrEmployees'] = $this->payroll->getEmployeesPayroll(array('p.payroll_company_id' => $this->userCompanyID));
			} else {
				$this->arrData['arrEmployees'] = $this->employee->getEmployees(array());
			}
			
			$arrJobCategories = $this->employee->getValues(TABLE_JOB_CATEGORY, 'job_category_id, job_category_name,', array('job_category_status' => STATUS_ACTIVE, 'order_by' => 'job_category_name'));
		}
		
		$finalResult = array();	
		for($i=0; $i < count($arrJobCategories); $i++)
		{
			if($arrJobCategories[$i]['job_category_id'] == $dep_id){
				$finalResult[$arrJobCategories[$i]['job_category_name']] = $this->search($this->arrData['arrEmployees'], 'emp_job_category_id', $arrJobCategories[$i]['job_category_id']);	
				if(count($finalResult[$arrJobCategories[$i]['job_category_name']]) <= 0) {
					unset($finalResult[$arrJobCategories[$i]['job_category_name']]);
				}
			}
		}
		$this->arrData["arrEmployees"] = $finalResult;
		$this->arrData['arrMonths'] = $this->config->item('months');

		$this->template->write_view('content', 'payroll_management/over_time', $this->arrData);
		$this->template->render();
	}
}

/* End of file payroll_management.php */
/* Location: ./application/controllers/payroll_management.php